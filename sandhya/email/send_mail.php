<?php
function getEmailConstants(){
	
	$params = array();
	
	/****	SMTP SETTINGS  ****/
	/*$params['host'] = '';
	$params['username'] = ''; 
	$params['password'] = '';  */
	$params['fromName'] ='Sandhya Cine House'; 
	//$params['replyTo'] = '/';
	//$params['replyToName'] = '';
	//$params['port'] = 25;
	/****	SMTP SETTINGS  ****/
	
	return $params;
}

function sendMail($user_params){
	require_once($_SERVER['DOCUMENT_ROOT']. '/sandhya/email/PHPMailerAutoload.php');
	$params = getEmailConstants();
	
	$mail = new PHPMailer;
	$mail->SMTPDebug = 3;
	$mail->isSMTP();
	$mail->Host = 'localhost';  
	$mail->SMTPAuth = false;                               
	/*$mail->Username = $params['username'];                 
	$mail->Password = $params['password'];     */                    
	$mail->SMTPSecure = 'ssl';                           
	$mail->Port = 465 ;                                   
	
	//$mail->setFrom($params['fromName']);
	$mail->addAddress($user_params['c_email'], $user_params['c_fullname']);
	/*if($params['replyTo']!='')
		$mail->addReplyTo($params['replyTo'], $params['replyToName']);*/
		
	$mail->isHTML(true);
	
	$mail->Subject = $user_params['subject'];
	$mail->Body    = $user_params['message'];
	$mail->AltBody =  $user_params['message'];
	
	if(!$mail->send()) {
		return 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
		return 'Message has been sent';
	}
}
?>