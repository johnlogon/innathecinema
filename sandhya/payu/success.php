<?php
	require_once("../email.php");
	
	$status      = $_POST["status"];
	$firstname   = $_POST["firstname"];
	$amount      = $_POST["amount"];
	$txnid       = $_POST["txnid"];
	$key         = $_POST["key"];
	$productinfo = $_POST["productinfo"];
	$email       = $_POST["email"];
	$phone		 = $_POST['phone'];
	
	if($status=='success'){
		$bkid = $productinfo;
		
		$sqls = array("UPDATE thcbs_bookings SET status='confirmed', txn_id=".$txnid." WHERE id=".$bkid, "UPDATE thcbs_bookings_payments SET status='paid', amount=$amount WHERE booking_id=".$bkid, "UPDATE thcbs_plugin_invoice SET STATUS='paid' WHERE order_id=(SELECT UUID FROM thcbs_bookings WHERE id=$bkid)");
		foreach($sqls as $sql){
			mysqli_query($db, $sql);
		}
		echo "<h3>Thank You. Your order status is " . $status . ".</h3>";
		echo "<h4>Your Transaction ID for this transaction is " . $txnid . ".</h4>";
		echo "<h4>We have received a payment of Rs. " . $amount . "</h4>";
		
		$invoice_params = array();
		$invoice_params['c_name'] = $firstname;
		$invoice_params['c_email'] = $email;
		$invoice_params['c_phone'] = $phone;
		
		/**** MOVIE TITLE ***/
		$sql = "SELECT content FROM thcbs_multi_lang WHERE foreign_id=(SELECT event_id FROM thcbs_bookings WHERE id=$bkid) AND model='pjEvent' AND FIELD='title'";
		$row =  mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$invoice_params['movie'] = $row['content'];
		
		/***** FETCHING SHOW TIME ****/
		$sql = "SELECT date_time FROM thcbs_shows WHERE id=(SELECT show_id FROM thcbs_bookings_shows WHERE booking_id=$txnid LIMIT 1 OFFSET 0);";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC); 
		$showTime = $row['date_time'];
		$invoice_params['show_time'] = $showTime;
		
		/***** FETCHING SHOW SEATS ****/
		
		$sql = "SELECT name FROM thcbs_seats WHERE id IN(SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=$txnid);";
		
		$result = mysqli_query($db, $sql);
		$seats = "";
		while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$seats = $seats. $row['name'].", ";
		}
		$seats= rtrim($seats, ", ");
		$invoice_params['seats'] = $seats;
		
		/***** FETCHING CUSTOMER NUMBER ****/
		$sql = "SELECT * FROM thcbs_bookings WHERE id=$txnid";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$number = $row['c_phone'];
		$invoice_params['booking_id'] = $row['uuid'];
		$mail = new SnogoEmail();
		$mail->sendMail($invoice_params);
		
		
	}
	
	else{
		echo "Transaction Failed";
		$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
		$bkid = $productinfo;
		$sqls = array("UPDATE thcbs_bookings SET status='cancelled' WHERE id=$bkid", "DELETE FROM thcbs_bookings_payments WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_shows WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_tickets WHERE booking_id=$bkid", "UPDATE thcbs_plugin_invoice SET STATUS='cancelled' WHERE order_id=(SELECT UUID FROM thcbs_bookings WHERE id=$bkid)" );
		
		foreach($sqls as $sql){
			mysqli_query($db, $sql);
		}
		
	} 
?>	