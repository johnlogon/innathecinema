<?php
	require_once("../app/config/config.inc.php");
	
	$status    = $_POST["status"];
	$firstname = $_POST["firstname"];
	$amount    = $_POST["amount"];
	$txnid     = $_POST["txnid"];
	
	$posted_hash = $_POST["hash"];
	$key         = $_POST["key"];
	$productinfo = $_POST["productinfo"];
	$email       = $_POST["email"];
	$salt        = "GQs7yium";
	
	if (isset($_POST["additionalCharges"])) {
		$additionalCharges = $_POST["additionalCharges"];
		$retHashSeq        = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
		
	} else {
		
		$retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
		
	}
	$hash = hash("sha512", $retHashSeq);
	
	$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
		
	
	$sqls = array("UPDATE thcbs_bookings SET status='cancelled' WHERE id=$txnid", "DELETE FROM thcbs_bookings_payments WHERE booking_id=$txnid", "DELETE FROM thcbs_bookings_shows WHERE booking_id=$txnid", "DELETE FROM thcbs_bookings_tickets WHERE booking_id=$txnid", "UPDATE thcbs_plugin_invoice SET STATUS='cancelled' WHERE order_id=(SELECT UUID FROM thcbs_bookings WHERE id=$txnid)" );
	
	foreach($sqls as $sql){
		mysqli_query($db, $sql);
	}
	
	
	if ($hash != $posted_hash) {
		echo "Invalid Transaction. Please try again";
	} else {
		
		echo "<h3>Your order status is " . $status . ".</h3>";
		echo "<h4>Your transaction id for this transaction is " . $txnid . ". You may retry making the payment.</h4>";
		
	}
?>
