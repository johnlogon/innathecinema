<?php

require_once dirname( __FILE__ ) . '/payu.php';
require_once("../loader.php");
require_once("email.php");

if(isset($_GET['bkid'])){
	/*CHECKING IF ID CONTAINS ONLY DIGITS*/
	if(!ctype_digit($_GET['bkid']))
		die();
	/*************************************/
	$m = new SnogoEmail();
	$db = $m->connectPj();
	
	/*CHECKING IF BOOKING IS ALREADY CONFIRMED*/
	$sql = "SELECT status FROM thcbs_bookings WHERE id=".$_GET['bkid'];
	$result = mysqli_query($db, $sql);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	if($row['status']=='confirmed'){
		die();
	}
	/*****************************************/
	/***** FETCHING SHOW SEATS ****/
		
		$sql = "SELECT name FROM thcbs_seats WHERE id IN(SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=".$_GET['bkid'].");";
		
		$result = mysqli_query($db, $sql);
		$state = mysqli_num_rows($result);
		$seats = "";
		while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$seats = $seats. $row['name'].", ";
		}
		$seats= rtrim($seats, ", ");
	/***** *****/
	
	/***** FETCHING SHOW TIME AND VENUE ****/
		$sql = "SELECT date_time, venue_id FROM thcbs_shows WHERE id=(SELECT show_id FROM thcbs_bookings_shows WHERE booking_id=".$_GET['bkid']." LIMIT 1 OFFSET 0);";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC); 
		$showTime = $row['date_time'];
		$showTime = date('d-M-Y h:i A', strtotime($showTime));
		
		$sql = "SELECT * FROM thcbs_multi_lang WHERE foreign_id=".$row['venue_id']." AND model='pjVenue' AND FIELD='name'";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$venue = $row['content'];
		
		$city = $showTime. "  ". $venue;
	
	$sql = "SELECT * FROM thcbs_bookings WHERE id=".$_GET['bkid'];
	$result = mysqli_query($db, $sql);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	
	if($row['event_id']==22){
		pay_page( array (	'key' => 'WQHHHt', 'txnid' => $row['uuid'], 'amount' => $row['sub_total'],
				'firstname' => $row['c_name'], 'email' => $row['c_email'], 'phone' => $row['c_phone'],
				'productinfo' => $_GET['bkid'], 'surl' => 'payment_success', 'furl' => 'payment_failure' , 'address1' => $seats, 'city' =>$city, 'state' =>$state), 'B9J4gfCJ' );
	}
	else{
		pay_page( array (	'key' => 'WQHHHt', 'txnid' => $row['uuid'], 'amount' => $row['total'],
					'firstname' => $row['c_name'], 'email' => $row['c_email'], 'phone' => $row['c_phone'],
					'productinfo' => $_GET['bkid'], 'surl' => 'payment_success', 'furl' => 'payment_failure' , 'address1' => $seats, 'city' =>$city, 'state' =>$state), 'B9J4gfCJ' );
	}
}

function payment_success() {
	//echo print_r( $_POST, true );
	$status      = $_POST["status"];
	$firstname   = $_POST["firstname"];
	$amount      = $_POST["amount"];
	$txnid       = $_POST["txnid"];
	$key         = $_POST["key"];
	$productinfo = $_POST["productinfo"];
	$email       = $_POST["email"];
	$phone		 = $_POST['phone'];
	$bank_re 	 = $_POST['bank_ref_num'];
	
	$bkid = $productinfo;
	//echo $bkid;
		
		$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
		/*
			CHECK FOR DUPLICATION OF SEATS
		*/
		$sql = "SELECT COUNT(*) AS s FROM thcbs_bookings_shows WHERE show_id=(SELECT DISTINCT show_id FROM thcbs_bookings_shows WHERE booking_id=".$bkid.") 
				AND seat_id IN (SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=".$bkid.")";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$count_total = $row['s'];
		
		$sql = "SELECT COUNT(*) AS s FROM thcbs_bookings_shows WHERE booking_id=".$bkid;
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$count_this_booking_seats = $row['s'];
		
		if($count_total>$count_this_booking_seats){ //duplication occured
			//fetch duplicated seats.. only for reference
			$sql = "SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=".$bkid;
			$result = mysqli_query($db, $sql);
			$dupSeats = "";
			while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
				$dupSeats.=$row['seat_id']." ";
			}
			$dupFile = fopen('dup_log.txt', 'a');
			date_default_timezone_set('Asia/Kolkata');
			fwrite($dupFile, 'Duplicated Seats found>>Booking id:'.$bkid.', seats: '.$dupSeats.',  time:'.date('d-m-Y h:i:s A')."\n");
			echo "<div style='margin:10%; border:solid 1px;text-align:center;color:red;'>";
			echo "<h3>Your transaction has been failed. For refund, please contact: 9562583247, 0495 4044435 (9 AM to 5.30 PM, MON-FRI)</h3>";
			echo "<h4>Your Transaction ID is " . $txnid . ".</h4>";
			echo "<a href='http://innathecinema.com/ticket' >Book Again</a><br><br>";
			echo "</div>";
			$sqls = array("UPDATE thcbs_bookings SET status='cancelled' WHERE id=$bkid", "DELETE FROM thcbs_bookings_payments WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_shows WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_tickets WHERE booking_id=$bkid", "UPDATE thcbs_plugin_invoice SET STATUS='cancelled' WHERE order_id=(SELECT UUID FROM thcbs_bookings WHERE id=$bkid)" );
	
			foreach($sqls as $sql){
				mysqli_query($db, $sql);
			}
			
			/*mail and message*/
			$invoice_params['booking_id'] = $txnid;
			$invoice_params['c_email'] = $email;
			$invoice_params['c_phone'] = $phone;
			$mail->sendErrorSMS($invoice_params);
			$mail->sendErrorMail($invoice_params);
			die();
		}
		
		/*********end of duplication checking***********/
		$sqls = array("UPDATE thcbs_bookings SET status='confirmed', txn_id='".$bank_re."' WHERE id=".$bkid, "UPDATE thcbs_bookings_payments SET status='paid', amount=$amount WHERE booking_id=".$bkid, "UPDATE thcbs_plugin_invoice SET STATUS='paid' WHERE order_id=(SELECT UUID FROM thcbs_bookings WHERE id=$bkid)");
		foreach($sqls as $sql){
			if(!mysqli_query($db, $sql)){
				//echo $sql;
			}
				
		}
		
		$invoice_params = array();
		$invoice_params['c_name'] = $firstname;
		$invoice_params['c_email'] = $email;
		$invoice_params['c_phone'] = $phone;
		
		/**** MOVIE TITLE ***/
		$sql = "SELECT content FROM thcbs_multi_lang WHERE foreign_id=(SELECT event_id FROM thcbs_bookings WHERE id=$bkid) AND model='pjEvent' AND FIELD='title'";
		$row =  mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$invoice_params['movie'] = $row['content'];
		
		/***** FETCHING SHOW TIME AND VENUE ****/
		$sql = "SELECT date_time, venue_id FROM thcbs_shows WHERE id=(SELECT show_id FROM thcbs_bookings_shows WHERE booking_id=$bkid LIMIT 1 OFFSET 0);";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC); 
		$showTime = $row['date_time'];
		$showTime = date('d-M-Y h:i A', strtotime($showTime));
		$invoice_params['show_time'] = $showTime;
		
		$sql = "SELECT * FROM thcbs_multi_lang WHERE foreign_id=".$row['venue_id']." AND model='pjVenue' AND FIELD='name'";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$invoice_params['venue'] = $row['content'];
		
		
		/***** FETCHING SHOW SEATS ****/
		
		$sql = "SELECT name FROM thcbs_seats WHERE id IN(SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=$bkid);";
		
		$result = mysqli_query($db, $sql);
		$sendFlag=true;
		if(!mysqli_num_rows($result)>0){
			$sendFlag=false;
		}
		$seats = "";
		while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$seats = $seats. $row['name'].",";
		}
		$seats= rtrim($seats, ",");
		$invoice_params['seats'] = $seats;
		
		/***** FETCHING CUSTOMER NUMBER ****/
		$number = $_POST['phone'];
		$invoice_params['booking_id'] = $_POST["txnid"];
		$mail = new SnogoEmail();
		if($sendFlag){
			echo "<div style='margin:10%; border:solid 1px;text-align:center;color:green;font-family:arial;'>";
			echo "<h3>Your booking is successful.</h3>";
			echo "<h4>Transaction ID is " . $txnid . ".</h4>";
			echo "<h4>Name: ".$invoice_params['c_name'].".</h4>";
			echo "<h4>We have received a payment of Rs. " . $amount . "</h4>";
			echo "<span style='color: blue;font-size: 18px;'><h4>Movie:".$invoice_params['movie'].", Seats: ".$seats.", Show: ".$invoice_params['show_time'].",".$invoice_params['venue']."</h4></span>";
			echo "<h4>Your ticket will be recieved in SMS / mail</h4>";
			echo "<h4>Show the mail / screenshot / SMS at the counter.</h4>";
			echo "<h4 style='color:black;'>For any queries: +91 9562583247, 0495 4044435 (9 AM to 5.30 PM, MONDAY - FRIDAY)</h4>";
			echo "<h3>Enjoy the film. Thanks!!</h3>";
			echo "<a href='http://innathecinema.com/ticket' >Book Another Ticket</a><br><br>";
			echo "</div>";			
			$mail->sendMail($invoice_params);
			$send = $mail->sendSms($invoice_params);
		}else{
			echo "<div style='margin:10%; border:solid 1px;text-align:center;color:red;'>";
			echo "<h3>Your transaction has been failed. For refund, please contact: 9562583247, 0495 4044435 (9 AM to 5.30 PM, MONDAY - FRIDAY)</h3>";
			echo "<h4>Your Transaction ID is " . $txnid . ".</h4>";
						echo "<a href='http://innathecinema.com/ticket' >Book Again</a><br><br>";
						echo "</div>";
			$mail->sendErrorSMS($invoice_params);
			$mail->sendErrorMail($invoice_params);
		}
		
		/*$sql = "SELECT 
		  COUNT(seats.`booking_id`) AS c,
		  shows.`booking_limit`,
		  seats.`show_id`  
		FROM
		  thcbs_bookings_shows AS seats 
		  INNER JOIN thcbs_shows AS shows 
			ON shows.id = seats.`show_id` 
		WHERE seats.show_id = 
		  (SELECT DISTINCT
			seatsA.show_id 
		  FROM
			thcbs_bookings_shows AS seatsA 
		  WHERE seatsA.`booking_id`= ".$bkid.") ;
		
		";
		$file = fopen("disable.txt", "a");
		
		
		$result = mysqli_query($db, $sql);
		if($result){
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			if($row['c']!=0){
				if($row['c']>=$row['booking_limit']){
					fwrite($file, $sql);
					$sql = "UPDATE thcbs_shows SET booking_disabled=1 WHERE id=".$row['show_id'];
					date_default_timezone_set('Asia/Kolkata');
					fwrite($file, $sql." >>time:".date('d-M-Y h:i:s A', time())." >>Disabled by:software"."\n\n\n");
					$result = mysqli_query($db, $sql);
				}
			}
		}
		fclose($file);*/
		//unholding booked seats
			$sql = "UPDATE thcbs_shows_seats SET holding = 0 WHERE show_id = (SELECT show_id FROM thcbs_bookings_shows WHERE booking_id = ".$bkid." LIMIT 1 OFFSET 0) AND seat_id IN (SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id = ".$bkid.")";
			mysqli_query($db, $sql);
		/*
			CHECKING IF BOOKING LIMIT REACHED
		*/
		$sql="SELECT * FROM thcbs_shows WHERE id=(SELECT DISTINCT show_id FROM thcbs_bookings_shows WHERE booking_id=".$bkid.");";
		$file = fopen("disable.txt", "a");
		$result = mysqli_query($db, $sql);
		$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
		$booking_limit = $row['booking_limit'];
		$show_id = $row['id'];
		$event_id = $row['event_id'];
		$date_time = $row['date_time'];
		
		$sql = "SELECT COUNT(*) AS c FROM thcbs_bookings_shows WHERE booking_id IN
				(SELECT id FROM thcbs_bookings WHERE event_id=".$event_id." AND date_time='".$date_time."' and status='confirmed')";
		$result = mysqli_query($db, $sql);
		if($result){
			$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
			if($row['c']!=0){
				if($row['c']>=$booking_limit){
					fwrite($file, $sql);
					$sql = "UPDATE thcbs_shows SET booking_disabled=1 WHERE id=".$show_id;
					date_default_timezone_set('Asia/Kolkata');
					fwrite($file, $sql." >>time:".date('d-M-Y h:i:s A', time())." >>Disabled by:software"."\n\n\n");
					$result = mysqli_query($db, $sql);
				}
			}
		}
		fclose($file);
}

function payment_failure() {
	echo "<div style='margin:10%; border:solid 1px;text-align:center;color:red;'>";
			echo "<h3>Your transaction has been failed.</h3>";
						echo "<a href='http://innathecinema.com/ticket' >Book Again</a><br><br>";
						echo "</div>";
		$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
		$status      = $_POST["status"];
		$firstname   = $_POST["firstname"];
		$amount      = $_POST["amount"];
		$txnid       = $_POST["txnid"];
		$key         = $_POST["key"];
		$productinfo = $_POST["productinfo"];
		$email       = $_POST["email"];
		$phone		 = $_POST['phone'];
		$bank_re 	 = $_POST['bank_ref_num'];
		$bkid = $productinfo;
		$sqls = array("UPDATE thcbs_bookings SET status='cancelled' WHERE id=$bkid", "DELETE FROM thcbs_bookings_payments WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_shows WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_tickets WHERE booking_id=$bkid", "UPDATE thcbs_plugin_invoice SET STATUS='cancelled' WHERE order_id=(SELECT UUID FROM thcbs_bookings WHERE id=$bkid)" );
		
		foreach($sqls as $sql){
			mysqli_query($db, $sql);
		}
		
		//unholding seats
			$sql = "UPDATE thcbs_shows_seats SET holding = 0 WHERE show_id = (SELECT show_id FROM thcbs_bookings_shows WHERE booking_id = ".$bkid." LIMIT 1 OFFSET 0) AND seat_id IN (SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id = ".$bkid.")";
			mysqli_query($db, $sql);
		?>
        	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
			<script>
            	$(document).ready(function(e) {
                   setInterval(function(){
						goHome();
					}, 10000);
					function goHome(){
						location.href='http://innathecinema.com/ticket';
					}
                });
            </script>
		<?php
}

