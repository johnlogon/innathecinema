<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require_once('classes/refund.php');
	if($_SERVER['REQUEST_METHOD']!='POST')
		die();
		
	/*if(!isset($_POST['transaction_date']) || empty($_POST['transaction_date'])){
		echo "<div style='color:red'>TRANSACTION DATE EMPTY </div>";
		die();
	}*/
	if(!isset($_FILES['file']['name']) || empty($_FILES['file']['name'])){
		echo "<div style='color:red'>TRANSACTION FILE NOT FOUND </div>";
		die();
	}
	if (($_FILES["file"]["error"] > 0)){
		echo "<div style='color:red'>INVALID FILE";
		echo "<br /> Return Code: " . $_FILES["file"]["error"] . "<br /> </div>";
		die();
	}
	//$transaction_date = $_POST['transaction_date'];
	$allowedExts = array("CSV", "csv", "xlsx", "XLSX", "xls");
	$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	
	$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');
	
	if(!in_array($_FILES['file']['type'],$mimes)){
		echo "<div style='color:red'>INVALID FILE </div>";
		die();
	}
	if(!in_array($extension, $allowedExts)){
		echo "<div style='color:red'>INVALID FILE </div>";
		die();
	}
	$temp = explode(".", $_FILES["file"]["name"]);
	$newfilename = round(microtime(true)) . '.' . end($temp);
	$newfilename = $_SERVER['DOCUMENT_ROOT']."/sandhya/charge-back/files/".$newfilename;
	$uploaded=move_uploaded_file($_FILES["file"]["tmp_name"],$newfilename);
	if(!$uploaded){
		echo "<div style='color:red'>FILE UPLOAD FAILED </div>";
		die();
	}
	
	date_default_timezone_set('Asia/Kolkata');
	$refund = new Refund;
	$uuids = $refund->readUuidsFromFile($newfilename);
	$ids = $refund->getBookingDetails($uuids);
	$data = $refund->readBookingTickets($ids);
	exit;
	$refund->exportToFile($data);
?>