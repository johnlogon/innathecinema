<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/loader.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/charge-back/PHPExcel.php");
	class Refund {
		public function logger($log){
			$logFile = fopen($_SERVER['DOCUMENT_ROOT'].'/sandhya/charge-back/log.txt', 'a');
			date_default_timezone_set('Asia/Kolkata');
			fwrite($logFile, $log."  time:".date("d-m-Y h:i:s A", time())."\n");
			fclose($logFile);
		}
		private function connect(){
			$mysqli = new mysqli(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
			if ($mysqli->connect_error) {
				$this->logger("Connection failed: " . $mysqli->connect_error);
				return false;
			}
			return $mysqli;
		}
		public function readUuidsFromFile($fileName){
			$inputFileType = PHPExcel_IOFactory::identify($fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($fileName);
			$uuids = array();
			for($i=2;$i>0;$i++){
				$cellValue = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue();
				if($cellValue=='' || $cellValue==NULL)
					break;
				$uuids[] = $cellValue;
				//echo $cellValue."<br>";
			}
			return $uuids;
		}
		public function getBookingDetails($uuids){
			$id_array = "";
			for($i=0;!empty($uuids[$i]);$i++){
				$id_array = $id_array. "'".$uuids[$i]."',";
			}
			$id_array = rtrim($id_array, ",");
			$booking_Details = $this->getDetails($id_array);
			return $booking_Details;
		}
		public function getDetails($id_array){
			$mysqli = $this->connect();
			if($mysqli==false){
				$this->logger('Database connection error');
				return false;
			}
			$sql = "SELECT t1.id, t1.total, t1.uuid, t1.c_name, t1.c_phone, t1.created FROM thcbs_bookings t1 where t1.uuid IN (".$id_array.")";
			if($stmt = $mysqli->prepare($sql)){
				$r = $stmt->execute();
				$stmt->bind_result($id, $amount, $uuid, $name, $phone, $created);
				$data[] = array();
				$i =0;
				while($stmt->fetch()){
					$data[$i]['id'] = $id;
					$data[$i]['amount'] = $amount;
					$data[$i]['uuid'] = $uuid;
					$data[$i]['name'] = $name;
					$data[$i]['phone'] = $phone;
					$data[$i]['created'] = $created;
					$i++;
				}
				$stmt->close();
				return $data;
			}
			else
				return $mysqli->error;
		}
		public function readBookingTickets($booking_details){
			$result = array();
			$j=0;
			echo "<table style='border-collapse: separate;border-spacing: 15px;'><tr><td>Transaction ID</td><td>Name</td><td>Phone</td><td>Transaction date</td></tr>";
			for($i=0;!empty($booking_details[$i]['id']);$i++){
				if(!$this->readTickets($booking_details[$i]['id'])){
					$result[$j]['uuid'] = /*'INNCINEMALIV'. */$booking_details[$i]['uuid'];
					$result[$j]['amount'] = $booking_details[$i]['amount'];
					$j++;
					echo "<tr><td>".$booking_details[$i]['uuid']."</td><td>".$booking_details[$i]['name']."</td><td>".$booking_details[$i]['phone']."</td><td>".date('d-M-y', strtotime($booking_details[$i]['created']))."</td></tr>"; 
				}
			}
			return $result;
		}
		private function readTickets($id){
			
			$mysqli = $this->connect();
			if($mysqli==false){
				$this->logger('Database connection error');
				return false;
			}
			$sql = "SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=?";
			if($stmt = $mysqli->prepare($sql)){
				$stmt->bind_param('i', $id);
				$r = $stmt->execute();
				$stmt->bind_result($seatid);
				$stmt->store_result();
				if($stmt->num_rows>0){
					return true;
				}
				return false;
			}
			else{
				echo $mysqli->error;
				die();
			}
		}
		public function exportToFile($data){
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$rowCount = 1;
			$objPHPExcel->getActiveSheet()->getStyle('C2:C250')->getNumberFormat()->setFormatCode('d-mmm-yy');
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, 'Easy Payments User ID');
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'Unique Transaction ID');
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, 'Date of Transaction');
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Transaction Amount');
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, 'Reason for the refund');
			$styleArray = array(
			  'font' => array(
				'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE,
				'bold' => true
			  )
			);
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$rowCount)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount)->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getStyle('E'.$rowCount)->applyFromArray($styleArray);
			unset($styleArray);
			$rowCount++;
			//$trans_date = date('d-m-y', strtotime($transaction_date));
			for($i=0;!empty($data[$i]);$i++){
				$obj = $data[$i];
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, 'INNCINEMALIV');
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $obj['uuid']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, '');
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $obj['amount']);
				$rowCount++;
			}
			$rowCount--;
			
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$fileName = $_SERVER['DOCUMENT_ROOT']."/sandhya/charge-back/outputs/REFUND_LIST_".$trans_date.".xlsx";
			$objWriter->save($fileName);
			ob_end_clean();
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=REFUND_LIST_'.date('d_m_y', time()).'.xlsx');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($fileName));
			readfile($fileName);
			exit;
		}
	}
?>