<?php
	require_once("../app/config/config.inc.php");
	
	$status      = $_POST["status"];
	$firstname   = $_POST["firstname"];
	$amount      = $_POST["amount"];
	$txnid       = $_POST["txnid"];
	$posted_hash = $_POST["hash"];
	$key         = $_POST["key"];
	$productinfo = $_POST["productinfo"];
	$email       = $_POST["email"];
	$salt        = "vrvaiBBSHJ";
	
	echo $productinfo;
	
	if (isset($_POST["additionalCharges"])) {
		$additionalCharges = $_POST["additionalCharges"];
		$retHashSeq        = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
		
	} else {
		
		$retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
		
	}
	$hash = hash("sha512", $retHashSeq);
	
	if ($hash != $posted_hash) {
		echo "Invalid Transaction. Please try again";
		$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
		$bkid = $productinfo;
		$sqls = array("UPDATE thcbs_bookings SET status='cancelled' WHERE id=$bkid", "DELETE FROM thcbs_bookings_payments WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_shows WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_tickets WHERE booking_id=$bkid", "UPDATE thcbs_plugin_invoice SET STATUS='cancelled' WHERE order_id=(SELECT UUID FROM thcbs_bookings WHERE id=$bkid)" );
		
		foreach($sqls as $sql){
			mysqli_query($db, $sql);
		}
		
	} else {
		$sqls = array("UPDATE thcbs_bookings SET status='confirmed' WHERE id=".$bkid, "UPDATE thcbs_bookings_payments SET status='paid', amount=$amount WHERE booking_id=".$bkid, "UPDATE thcbs_plugin_invoice SET STATUS='paid' WHERE order_id=(SELECT UUID FROM thcbs_bookings WHERE id=$bkid)");
		foreach($sqls as $sql){
			mysqli_query($db, $sql);
		}
		echo "<h3>Thank You. Your order status is " . $status . ".</h3>";
		echo "<h4>Your Transaction ID for this transaction is " . $txnid . ".</h4>";
		echo "<h4>We have received a payment of Rs. " . $amount . "</h4>";
		
		/***** FETCHING SHOW TIME ****/
		$sql = "SELECT date_time FROM thcbs_shows WHERE id=(SELECT show_id FROM thcbs_bookings_shows WHERE booking_id=$txnid LIMIT 1 OFFSET 0);";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC); 
		$showTime = $row['date_time'];
		
		/***** FETCHING SHOW SEATS ****/
		
		$sql = "SELECT name FROM thcbs_seats WHERE id IN(SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=$txnid);";
		
		$result = mysqli_query($db, $sql);
		$seats = "";
		while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$seats = $seats. $row['name'].", ";
		}
		$seats= rtrim($seats, ", ");
		
		/***** FETCHING CUSTOMER NUMBER ****/
		$sql = "SELECT * FROM thcbs_bookings WHERE id=$txnid";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$number = $row['c_phone'];
		
		$message = "Order no:".$row['uuid']." Show time:".$showTime." Seats:".$seats;
		
		$url = "http://sms.hspsms.com/sendSMS?username=hspdemo&message=".$message."&sendername=HSPSMS&smstype=TRANS&numbers=".$number."&apikey=2d24be91-e1d2-4dc5-b8d0-433baa791182";
		
		 echo sendSms($url);
		
		
		
	}
	
	function sendSms($url)
	{
		$ch = curl_init();  
	 
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	//  curl_setopt($ch,CURLOPT_HEADER, false); 
	 
		$output=curl_exec($ch);
	 
		curl_close($ch);
		return $output;
	}
 
	
?>	