<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/pdf/tcpdf.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/loader.php");
	
	class PDF{
		public function logger($log){
			$logFile = fopen('pdf.txt', 'a');
			date_default_timezone_set('Asia/Kolkata');
			fwrite($logFile, $log."  time:".date("d-m-Y h:i:s A", time())."\n");
			fclose($logFile);
		}
		private function connect(){
			$mysqli = new mysqli(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
			date_default_timezone_set('Asia/Kolkata');
			if ($mysqli->connect_error) {
				$this->logger("Connection failed: " . $mysqli->connect_error);
				return false;
			}
			return $mysqli;
		}
		public function generateReport($params){
			try{
				$pdf = $this->init();
				$img = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/sandhya/pdf/image/logo.jpg');
				$pdf->Image('@' . $img);
				$pdf->writeHTML($params['modal'], true, 0, true, 0);
				$pdf->lastPage();
				$fileName = "default.pdf";
				if(!empty($params['fileName']))
					$fileName = $params['fileName'].".pdf";
				$pdf->Output($_SERVER['DOCUMENT_ROOT'].'/sandhya/temp/'.$fileName, 'F');
				return true;
			}
			catch(Exception $e){
				return false;
			}
		}
		private function init(){
			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
			
			if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
				require_once(dirname(__FILE__).'/lang/eng.php');
				$pdf->setLanguageArray($l);
			}
			$pdf->SetFont('helvetica', '', 9);
			$pdf->AddPage();
			return $pdf;
		}
		private function getShowWiseModal($params){
			$show_info = $params['show_info'];
			$settings = $params['settings'];
			$modal = '<html>
						<head>
							<style>
								td,th{
									font-size:14px;
									text-align:center;
								}
							</style>
						</head>
						<body>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<div>
								<h2 align="center" style="font-size:18px"><b><u>Summary Report</u></b></h2>
								<h1>Report Number: '.$params['report_num'].'</h1>
								<h1>Date: '.$params['date'].'</h1>
								<h1>Movie: '.$params['movie'].'</h1>
								<h1>Venue: '.$params['venue'].' </h1>
								<table border="1">
									<tr>
										
										<th><h3>Show Time</h3></th>
										<th><h3>Number Of Seats</h3></th>
										<th><h3>Ticket Price</h3></th>
										<th><h3>Total</h3></th>
									</tr>
									<tr>
										<td>'.$params['show_time'].'</td>
										<td>'.$show_info['total_seats'].'</td>
										<td>'.$settings['ticket_price'].'</td>
										<td>'.$show_info['total_seats']*$settings['ticket_price'].'</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td colspan="3" style="text-align:right;">Total &nbsp;&nbsp;</td>
										<td>'.$show_info['total_seats']*$settings['ticket_price'].'</td>
									</tr>
								</table>
								<br>
								<br>
								<br>
								<br>
								<br>
								<i align="center">***** This is a system generated file *****</i>
							</div>
						</body>
					  </html>';
			$data['modal'] = $modal;
			$data['sms'] = strtoupper($params['venue']).' - '.date('h:iA', strtotime($params['show_time'])).' Online Booking Closed ('.date('d-m-Y', strtotime($params['date'])).') ('.$show_info['total_seats'].' seats - Rs.'.$show_info['total_seats']*$settings['ticket_price'].'/-) - '.strtoupper($params['movie']);
			return $data;
		}
		private function getScreenWiseModal($params){
			$setttings = $params['settings'];
			date_default_timezone_set('Asia/Kolkata');
			$modal = '<html>
						<head>
							<style>
								td,th{
									font-size:14px;
									text-align:center;
								}
							</style>
						</head>
						<body>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<div>
								<h2 align="center" style="font-size:18px"><b><u>Summary Report</u></b></h2>
								<h1>Report Number: '.$params['report_num'].'</h1>
								<h1>Date: '.$params['date'].'</h1>
								<br>
								<br>';
								$smss = array();
								foreach($params['venues'] as $venue){
									$venue_data = $venue['venue_data'];
						$modal.='<h1>Venue: '.$venue['venue_name'].' </h1>
								<table border="1">
									<tr>
										<th><h3>Movie</h3></th>
										<th><h3>Show Time</h3></th>
										<th><h3>Number Of Seats</h3></th>
										<th><h3>Ticket Price</h3></th>
										<th><h3>Total</h3></th>
									</tr>';
									$sms=strtoupper($venue['venue_name'])."- Today's Collection(".date('d-m-Y', time())."): Rs.%s%/-";
									$total_p = 0;
									foreach($venue_data as $show_info){
										$total = $show_info['total_seats']*$setttings['ticket_price'];
										$total_p+=$total;
							$modal.='<tr>
										<td>'.$show_info['movie'].'</td>
										<td>'.$show_info['show_time'].'</td>
										<td>'.$show_info['total_seats'].'</td>
										<td>'.$setttings['ticket_price'].'</td>
										<td>'.$total.'</td>
									</tr>';
									$show_time = date('h:iA', strtotime($show_info['show_time']));
									$sms.="(".$show_time."-".$show_info['total_seats']."-Rs.".$total."),";
									}
						   $modal.='<tr>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td colspan="4" style="text-align:right;">Total &nbsp;&nbsp;</td>
										<td>'.$total_p.'</td>
									</tr>
								</table>';
								$sms=rtrim($sms, ",");
								$sms = str_replace("%s%", $total_p, $sms);
								$smss[] = $sms;
								}
					    
						$modal.='<br>
								<br>
								<br>
								<br>
								<br>
								<i align="center">***** This is a system generated file *****</i>
							</div>
						</body>
					  </html>';
			$data['modal'] = $modal;
			$data['sms'] = $smss;
			return $data;
		
		}
		public function sendShowReport($params){
			$settings = $params['settings'];
			$show = $params['show'];
			if($params['which']=='single'){
				$dt = $this->getShowWiseModal($params);
				$data['modal'] = $dt['modal'];
				$data['fileName'] = $params['report_num'];
				if($this->generateReport($data)){
					if($settings['email_each_show']==1){
						$mail = $this->sentReportMail($settings['email'],$data['fileName']);
					}
					if($settings['sms_each_show']==1){
						$sms = $dt['sms'];
						$numbers = $settings['sms'];
						$numbers= explode(",", $numbers);
						foreach($numbers as $number){
							$number = trim($number, " ");
							$this->sentReportSms($sms, $number);
						}
					}
					
				}
			}
			else{
				$dt = $this->getScreenWiseModal($params);
				$data['modal'] = $dt['modal'];
				$data['fileName'] = $params['report_num'];
				if($this->generateReport($data)){
					if($settings['email_end_day']==1){
						$mail = $this->sentReportMail($settings['email'],$data['fileName']);
					}
					if($settings['sms_end_day']==1){
						$smss = $dt['sms'];
						$numbers = $settings['sms'];
						$numbers= explode(",", $numbers);
						foreach($numbers as $number){
							$number = trim($number, " ");
							foreach($smss as $sms)
								$this->sentReportSms($sms, $number);
						}
					}
				}
			}
			if(!$this->setReportSentFlag($show['id'])){
				sleep(30);
				$this->setReportSentFlag($show['id']);
			}
				
		}
		private function setReportSentFlag($show_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "UPDATE thcbs_shows SET report_sent=1 WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $show_id);
			if($stmt->execute())
				return true;
			return false;
		}
		private function sentReportMail($emails, $fileName){
			$email = new PHPMailer();
			 
			/*******************************/
				//For local machine
				/*$email->SMTPOptions = array(
					'ssl' => array(
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					)
				);
				$email->isSMTP();
				$email->Host = 'mail.snogol.net';
				$email->SMTPAuth = true;
				$email->Username = '';
				$email->Password = '';
				$email->Port = 587;  */
			/*******************************/
			
			$email->From      = 'ereport@innathecinema.com';
			$email->FromName  = 'Innathe Cinema';
			$email->Subject   = 'Summary Report';
			$email->Body      = 'Please see the attachment';
			$mails = explode("," ,$emails);
			foreach($mails as $mail){
				$mail = trim($mail," ");
				$email->AddAddress($mail);
			}
			$file_to_attach = $_SERVER['DOCUMENT_ROOT'].'/sandhya/temp/'.$fileName.'.pdf';
			
			$email->AddAttachment( $file_to_attach , 'summary.pdf' );
			
			return $email->Send();
			
		}
		private function sentReportSms($sms, $number){
			$http = new pjHttp();
			$url = "http://alerts.ebensms.com/api/v3/";
			$params = http_build_query(array(
				'method' =>'sms',
				'message' => $sms,
				'sender' => 'INCNMA',
				'to' =>$number,
				'api_key' => 'A35e447822b5a27a172224ae820e51572' 
			));
			$http->request($url ."?". $params)->getResponse();
		}
	}
?>