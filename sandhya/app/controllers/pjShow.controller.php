<?php
	class pjShow extends pjFront{
		private function connect(){
			$mysqli = new mysqli(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
			if ($mysqli->connect_error) {
				$this->logger("Connection failed: " . $mysqli->connect_error);
				return false;
			}
			return $mysqli;
		}
		public function getActiveEvents(){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			/*$sql = "SELECT 
					  thcbs_events.*,
					  thcbs_multi_lang.`content` AS movie_name 
					FROM
					  thcbs_events 
					  INNER JOIN thcbs_multi_lang 
						ON thcbs_events.`id` = thcbs_multi_lang.`foreign_id` 
					WHERE thcbs_events.`status` = 'T' 
					  AND thcbs_multi_lang.`model` = 'pjEvent' 
					  AND thcbs_multi_lang.`locale` = 1 
					  AND thcbs_multi_lang.`field` = 'title'
					ORDER BY thcbs_events.index";	*/
			date_default_timezone_set('Asia/Kolkata');
			$date = date('Y-m-d 00:00:00', time());
			if(isset($_GET['date']) && !empty($_GET['date']))
				$date = date('Y-m-d 00:00:00', strtotime($_GET['date']));
			$sql = "SELECT 
					  thcbs_events.*,
					  thcbs_multi_lang.`content` AS movie_name 
					FROM
					  thcbs_events 
					  INNER JOIN thcbs_multi_lang 
						ON thcbs_events.`id` = thcbs_multi_lang.`foreign_id`
					  INNER JOIN `thcbs_shows` ON  `thcbs_shows`.`event_id`= thcbs_events.`id`
					WHERE thcbs_events.`status` = 'T' 
					  AND thcbs_multi_lang.`model` = 'pjEvent' 
					  AND thcbs_multi_lang.`locale` = 1 
					  AND thcbs_multi_lang.`field` = 'title' 
					  AND `thcbs_shows`.`date_time`<='".$date."'
					GROUP BY `thcbs_events`.`index`";
			$stmt = $mysqli->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($id, $duration, $release_date, $event_img,$created, $status, $event_img_site, $yoututbe_url, $index, $movie_name);
			$data = array();
			$i=0;
			while($stmt->fetch()){
				$data[$i]['id'] = $id;
				$data[$i]['duration'] = $duration;
				$data[$i]['release_date'] = $release_date;
				$data[$i]['event_img'] = $event_img;
				$data[$i]['created'] = $created;
				$data[$i]['status'] = $status;
				$data[$i]['event_img_site'] = $event_img_site;
				$data[$i]['yoututbe_url'] = $yoututbe_url;
				$data[$i++]['movie_name'] = $movie_name;
			}
			return $data;
		}
		public function getShowTimes($event_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT DISTINCT 
					  CAST(date_time AS TIME) AS show_time 
					FROM
					  thcbs_shows 
					WHERE event_id = ?
					  AND CAST(date_time AS DATE) =
					  (SELECT 
						CAST(date_time AS DATE) 
					  FROM
						thcbs_shows 
					  WHERE event_id = ?
					  ORDER BY date_time DESC 
					  LIMIT 1)";		
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('ii', $event_id, $event_id);
			if(!$stmt->execute()){
				//echo $mysqli->error;
				//exit;
			}
			$stmt->bind_result($show_time);
			$data = array();
			$i=0;
			while($stmt->fetch()){
				$show_time_converted=date('g:i A',strtotime($show_time));
				$data[$i++]['time'] = $show_time_converted;
			}
			return $data;
		}
		public function getMovieDescription($event_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT 
					  thcbs_multi_lang.`content` 
					FROM
					  thcbs_multi_lang 
					WHERE thcbs_multi_lang.`foreign_id` = ? 
					  AND thcbs_multi_lang.`model` = 'pjEvent' 
					  AND thcbs_multi_lang.`field` = 'description' ";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $event_id);
			$stmt->execute();
			$stmt->bind_result($description);
			$stmt->fetch();
			return $description;
		}
		public function getVenueInfo($event_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT 
					  content AS screen,
					  foreign_id AS venue_id 
					FROM
					  thcbs_multi_lang 
					WHERE foreign_id = 
					  (SELECT 
						venue_id 
					  FROM
						thcbs_shows 
					  WHERE event_id = ? ORDER BY date_time DESC LIMIT 1) 
					  AND model = 'pjVenue' 
					  AND locale = 1 
					  AND FIELD= 'name'";
			/*$sql = "SELECT 
					  content AS screen,
					  foreign_id AS venue_id 
					FROM
					  thcbs_multi_lang 
					WHERE foreign_id = 
					  (SELECT DISTINCT 
						venue_id 
					  FROM
						thcbs_shows 
					  WHERE event_id = ? 
					  LIMIT 1) 
					  AND model = 'pjVenue' 
					  AND FIELD= 'name' ;
					
					";*/
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $event_id);
			$stmt->execute();
			$stmt->bind_result($screen, $id);
			$stmt->fetch();
			$data['screen'] = $screen;
			$data['id'] = $id;
			return $data;
		}
	}	
?>