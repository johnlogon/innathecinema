<style>
	.tbSelectSeatGuide.success a{
		display:none;
	}
</style>
<?php
require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/app/config/config.inc.php");
$date_time='';
if(isset($_GET['date_time'])){
	$date_time = $_GET['date_time'];
}
$map = $tpl['venue_arr']['map_path'];
if (is_file($map))
{
	$size = getimagesize($map);
	?>
	<div style="display: block" class="b10">
		<div class="tbSelectSeatGuide"></div>
		<div class="tbLabelSeats"><?php __('lblSelectedSeats');?>:</div>
		<div class="tbAskToSelectSeats" style="display: block"><?php __('lblSelectAvailableSeats');?></div>
		<div id="tbSelectedSeats">
			<?php
			$class = 'tbAssignedSeats';
			if(isset($tpl['seat_id_arr']) && count($tpl['seat_id_arr']) > 0)
			{
				$seat_label_arr = $tpl['seat_id_arr'];
				foreach($seat_label_arr as $price_id => $seat_arr)
				{
					foreach($seat_arr as $seat_id => $cnt)
					{
						for($i = 1; $i <= $cnt; $i++)
						{
							?><span class="<?php echo $class;?> tbAssignedSeats_<?php echo $price_id;?>" data_seat_id="<?php echo $seat_id;?>" data_price_id="<?php echo $price_id;?>"><?php echo $tpl['ticket_name_arr'][$price_id]; ?> #<?php echo $tpl['seat_name_arr'][$seat_id];?></span><?php
						}	
					}
				}
			} 
			?>
		</div>
		<div class="tbTipToRemoveSeats" style="display: none"><?php __('lblRemoveSeats');?><br/></div>
	</div>
	<div class="tb-seats-legend b10">
		<label><span class="tb-available-seats"></span><?php __('lblAvailableSeats');?></label>
		<label><span class="tb-selected-seats"></span><?php __('lblSelectedSeats');?></label>
        <label><span style="background-color:#f5ca8c;" class="tb-booked-seats"></span>Holding</label>
		<label><span class="tb-booked-seats"></span><?php __('lblBookedSeats');?></label>
	</div>
	<div class="clear_both"></div>
	<div id="boxMap">
		<div id="tbMapHolder" class="tbMapHolder" style="position: relative; overflow: hidden; width: <?php echo $size[0]; ?>px; height: <?php echo $size[1]; ?>px; margin: 0 auto;">
			<img id="map" src="<?php echo $map; ?>" alt="" style="margin: 0; border: none; position: absolute; top: 0; left: 0; z-index: 500" />
			<?php
			$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
			
			/*** UNHOLDING SEATS HELD 25 MINUTES BEFORE***/
			$time = time()-1500;
			
			$sql = "UPDATE thcbs_shows_seats SET holding=0, held_time='' WHERE held_time<=$time";
			mysqli_query($db, $sql);
			
			/*** FETCHING SEATS ON HOLD***/
			
			$sql = "SELECT * FROM thcbs_shows_seats WHERE show_id=(SELECT id FROM thcbs_shows WHERE date_time='$date_time') AND holding=1";
			
			$result = mysqli_query($db, $sql);
			
			$holding_seats = array();
			
			while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				$holding_seats[] = $row['seat_id'];
			}
			
			$selected_seats = array();
			foreach ($tpl['seat_arr'] as $seat){
				foreach($_arr as $pid){
					if(isset($tpl['seat_id_arr'][$pid][$seat['id']])){
						$selected_seats[] = $seat['id'];
					}
				}
			}
			
			$holding_seats = array_diff($holding_seats, $selected_seats);
			foreach ($tpl['seat_arr'] as $seat)
			{
				$is_selected = false;
				$is_available = true;
				$_arr = explode("~:~", $seat['price_id']);
				$tooltip = array();
				$seat_id = $seat['id'];
				
				foreach($_arr as $pid)
				{
					if(isset($tpl['seat_id_arr'][$pid][$seat['id']]))
					{
						$is_selected = true;
						if($seat['seats'] == $tpl['seat_id_arr'][$pid][$seat['id']])
						{
							$is_available = false;
						}
					}
				}
				$style="";
				if(in_array($seat_id, $holding_seats)){
					$avail_seats = 0;
					$style = "background: #f5ca8c; !important";
				}
				else
					$avail_seats = $seat['seats'] - $seat['cnt_booked'];
				?><span class="tbSeatRect<?php echo $avail_seats <= 0 ? ' tbSeatBlocked' : ($is_available == true ? ' tbSeatAvailable' : null); ?><?php echo $is_selected == true ? ' tbSeatSelected' : null;?>" data-id="<?php echo $seat['id']; ?>" data-price-id="<?php echo $seat['price_id']; ?>" data-name="<?php echo $seat['name']; ?>" data-count="<?php echo $seat['seats']; ?>" style="<?php echo $style; ?> width: <?php echo $seat['width']; ?>px; height: <?php echo $seat['height']; ?>px; left: <?php echo $seat['left']; ?>px; top: <?php echo $seat['top']; ?>px; line-height: <?php echo $seat['height']; ?>px"><?php echo stripslashes($seat['name']); ?></span><?php
			}
			?>
		</div>
	</div>
	<?php
} 
?>