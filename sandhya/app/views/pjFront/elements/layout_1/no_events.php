<div class="pjCbEventList">
<?php 
	foreach($events as $event){
		$show_times = $show->getShowTimes($event['id']);
		if(empty($show_times))
			continue;
		$venue = $show->getVenueInfo($event['id']);
?>
   <div class="pjCbEventRow">
      <div class="pjCbMovieIntroImage">
         <a href="#" class="tbMovieLink pjCbMovie" data-id="<?php echo $event['id'] ?>">
         <img src="http://<?php echo $_SERVER['SERVER_NAME'] ?>/sandhya/<?php echo $event['event_img'] ?>" class="img-responsive" alt=""></a>
      </div>
      <div class="pjCbMovieIntroContent">
         <div class="pjCbMovieTitle">
         	<a href="#" class="tbMovieLink pjCbMovie"><?php echo $event['movie_name'] ?></a></div>
       <?php 
	   		if($venue['screen']=='Screen 1'){
	   ?>
         <div class="pjCbMovieDuration" style="display:block;color: #337ab7;">
            <span style="font-weight:bold">Screen 1 (4K, DOLBY Atmos)</span><br><span> Total Seating Capacity:505</span><br>
            <img src="http://sandhyacinehouse.com/images/4k.jpg" style="width: 49px;">
            <img src="http://sandhyacinehouse.com/images/atmos.jpg" style="width: 51px;">                                
         </div>
       <?php 
			}
			else{
	   ?>
         <div class="pjCbMovieDuration" style="display:block;color: #337ab7;">
			<span style="font-weight:bold">Screen 2 (2K, 3D)</span><br><span> Total Seating Capacity:156</span><br>
            <img src="http://sandhyacinehouse.com/images/3d.jpg" style="width: 49px;">
         </div>
       <?php 
			}
	   ?>
         <div class="pjCbMovieDuration"><?php echo $event['duration'] ?>  minutes</div>
         <div class="pjCbMovieDesc"><?php echo $show->getMovieDescription($event['id']) ?></div>
         <div class="pjCbMovieTime">
            <label>Select time:</label>
            <div class="pjCbMovieTimesWrapper">
            	<?php foreach($show_times as $time){ ?>
            	<a href="#" data-disabled="" class="tbSelectorSeats pjCbTimePassed" >
                	<?php echo $time['time'] ?>
                	<span style="color:red"> 
                    (<?php $status =  $venue['screen']=='Screen 1' ? "Available at counter: 505 Seats" :  "Available at counter: 156 Seats";
					 		echo $status;
					 ?>) </span>
                </a>
                <?php } ?>
            </div>
         </div>
      </div>
   </div>
<?php 
	}
?>
</div>