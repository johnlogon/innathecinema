<?php
require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/app/config/config.inc.php");
$STORE = @$_SESSION[$controller->defaultStore];
$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
if($tpl['status'] == 'OK')
{
	?>
	<br/>
	<div class="container-fluid">
		<div class="panel panel-default pjCbMain">
			<div class="panel-heading clearfix pjCbHeading">
				<?php include_once PJ_VIEWS_PATH . 'pjFront/elements/layout_1/locale.php';?>

				<a href="#" class="btn btn-link text-muted pjCbBtnBack<?php echo $STORE['back_to'] == 'details' ? ' tbBackToDetails' : ' tbBackToEvents';?>" data-id="<?php echo $tpl['arr']['id']; ?>" data-date="<?php echo date($tpl['option_arr']['o_date_format'], strtotime($tpl['hash_date'])); ?>">
					<span class="text-muted">
						<i class="fa fa-arrow-left"></i>
						<?php __('front_back');?>
					</span>
				</a>
			</div><!-- /.panel-heading clearfix pjCbHeading -->
			
			<div class="panel-body pjCbBody pjCbSeats">
            	<!--<div id="timeout-wrapper" style="width:200px;height:50px;background:antiquewhite;margin-bottom:5px;font-size:20px;padding:15px;">
                	<label style="">Time left:</label><span style="margin-left:5px;" id="time_left"></span>
                </div>-->
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="well">
							<h1 class="text-center pjCbSeatsTitle"><?php echo pjSanitize::html($tpl['arr']['title']);?></h1>
							
							<div class="row">
								<p class="text-muted pjCbSeatsInfo text-right col-lg-6 col-md-6 col-sm-6 col-xs-12"><?php __('front_date')?>:</p>
																
								<p class="lead pjCbSeatsInfo col-lg-6 col-md-6 col-sm-6 col-xs-12"><strong><?php echo date($tpl['option_arr']['o_date_format'], strtotime($tpl['selected_date'])); ?></strong></p><!-- /.col-md-12 col-sm-12 col-xs-12 -->
							</div><!-- /.row -->

							<div class="row">								
								<p class="text-muted pjCbSeatsInfo text-right col-lg-6 col-md-6 col-sm-6 col-xs-12"><?php __('front_time')?>:</p>
																
								<p class="lead pjCbSeatsInfo selected_time col-lg-6 col-md-6 col-sm-6 col-xs-12"><strong><?php echo date($tpl['option_arr']['o_time_format'], strtotime($tpl['selected_date'] . ' ' . $STORE['selected_time'])); ?></strong></p><!-- /.col-md-12 col-sm-12 col-xs-12 -->
							</div><!-- /.row -->

							<div class="row">
								<p class="text-muted pjCbSeatsInfo text-right col-lg-6 col-md-6 col-sm-6 col-xs-12"><?php __('front_running_time')?>:</p>
																
								<p class="lead pjCbSeatsInfo col-lg-6 col-md-6 col-sm-6 col-xs-12"><strong><?php echo $tpl['arr']['duration']?> <?php __('front_minutes')?></strong></p><!-- /.col-md-12 col-sm-12 col-xs-12 -->
							</div><!-- /.row -->
							<?php
							if(count($tpl['hall_arr']) > 1)
							{ 
								?>
								<div class="row">
									<p class="text-muted pjCbSeatsInfo text-right col-lg-6 col-md-6 col-sm-6 col-xs-12"><?php __('front_hall');?>:</p>
																	
									<p class="lead pjCbSeatsInfo col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<select id="venue_id_<?php echo $_GET['index'];?>" name="venue_id" class="form-control pjCbSeatVenue">
											<?php
											foreach($tpl['hall_arr'] as $hall)
											{
												?><option value="<?php echo $hall['venue_id'];?>"<?php echo isset($STORE['venue_id']) ? ($STORE['venue_id'] == $hall['venue_id'] ? ' selected="selected"' : NULL) : NULL;?>><?php echo pjSanitize::html($hall['venue_name']);?></option><?php
											} 
											?>
										</select>
									</p><!-- /.col-md-12 col-sm-12 col-xs-12 -->
								</div><!-- /.row -->
								<?php
							} 
							?>
						</div><!-- /.well -->
					</div><!-- /.col-lg-6 col-md-6 col-sm-6 col-xs-12 -->

					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="well">
							<div class="row">
								<?php
								$class = 'tbAssignedNoMap';
								if(isset($tpl['venue_arr']))
								{
									if (is_file($tpl['venue_arr']['map_path']))
									{
										$class = 'tbAssignedSeats';
									}
								} 
								$ticket_name_arr = array();
								$ticket_tooltip_arr = array();
								if(isset($tpl['ticket_arr']) && count($tpl['ticket_arr']) > 0)
								{
									foreach($tpl['ticket_arr'] as $v)
									{
										$ticket_name_arr[$v['price_id']] = pjSanitize::html($v['ticket']);
										$ticket_tooltip_arr[$v['price_id']] = pjSanitize::html($v['ticket']) . ', ' .  pjUtil::formatCurrencySign($v['price'], $tpl['option_arr']['o_currency']);
									}
								}
								?>
								<div class="col-xs-12">
									<div class="tbAskToSelectTickets alert alert-info" role="alert" style="display: <?php echo isset($STORE['tickets']) ? 'none': 'block';?>"><?php $tpl['seats_available'] == true ? __('front_select_ticket_types_above') : __('front_no_seats_available');?></div>
									<div style="display: <?php echo isset($STORE['tickets']) ? 'block': 'none';?>">
										<div class="tbSelectSeatGuide alert alert-info" role="alert"></div>
										<label for="" class="tbSelectedSeatsLabel"><?php __('front_selected_seats');?>:</label>
										<?php
										if($class == 'tbAssignedSeats')
										{ 
											?>
											<div class="tbAskToSelectSeats pjCbSeatsMessage" style="display: <?php echo isset($STORE['seat_id']) ? 'none': 'block';?>"><?php __('front_select_available_seats');?></div>
											<?php
										} 
										?>
										<div id="tbSelectedSeats_<?php echo $_GET['index'];?>">
											<?php
											if(isset($STORE['seat_id']))
											{
												$seat_label_arr = $STORE['seat_id'];
												foreach($seat_label_arr as $price_id => $seat_arr)
												{
													foreach($seat_arr as $seat_id => $cnt)
													{
														for($i = 1; $i <= $cnt; $i++)
														{
															?><span class="<?php echo $class;?> tbAssignedSeats_<?php echo $price_id;?>" data_seat_id="<?php echo $seat_id;?>" data_price_id="<?php echo $price_id;?>"><?php echo $ticket_name_arr[$price_id]; ?> #<?php echo $tpl['seat_name_arr'][$seat_id];?></span><?php
														}	
													}
												}
											} 
											?>
										</div>
										<?php
										if($class == 'tbAssignedSeats')
										{ 
											?>
											<div class="tbTipToRemoveSeats pjCbSeatsMessage" style="display: <?php echo isset($STORE['seat_id']) ? 'block': 'none';?>"><?php __('front_how_to_remove_seats');?><br/></div>
											<?php
										} 
										?>
									</div>
								</div><!-- /.col-lg-8 col-md-7 col-sm-6 col-xs-12 -->
							</div><!-- /.row -->
							
							<div class="row">
								<?php
								if(isset($tpl['ticket_arr']) && count($tpl['ticket_arr']) > 0)
								{
									foreach($tpl['ticket_arr'] as $v)
									{
										
										if($v['cnt_tickets'] > 0 && $tpl['seats_available'] == true)
										{
											//calculate value of selectable seats
											//total seats selectable = booking limit - (count of booked seats + cound of holding seats)
											$sql="SELECT COUNT(id) AS tickets FROM thcbs_bookings_tickets WHERE booking_id IN(SELECT id FROM 
			thcbs_bookings WHERE event_id=".$tpl['arr']['id']." AND date_time='".$tpl['selected_date']." ".$STORE['selected_time']."' AND status='confirmed')";
														
											$result=mysqli_query($db, $sql);
											
											$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
											$booked_num = $row['tickets'];
											
											/*
												NUMBER OF HOLDING SEATS
											*/
											
											$sql = "SELECT COUNT(*) AS holding FROM thcbs_shows_seats WHERE holding=1 AND  show_id=(SELECT id FROM thcbs_shows WHERE event_id=".$tpl['arr']['id']." AND date_time='".$tpl['selected_date']." ".$STORE['selected_time']."')";
											$result=mysqli_query($db, $sql);
											fwrite($myfile, $sql."            \n");
											$seats_on_hold = 0;
											if(!$result){
												
												$txt = mysqli_error($db);
												fwrite($myfile, $txt);
												fclose($myfile);
											}
											else{
												$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
												$seats_on_hold = $row['holding'];
											}
											
											
											$sql = "SELECT booking_limit FROM thcbs_shows WHERE event_id=".$tpl['arr']['id']." AND date_time='".$tpl['selected_date']." ".$STORE['selected_time']."'";
											$result=mysqli_query($db, $sql);
											
											$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
											$booking_limit = $row['booking_limit'];
											
											$aval_sea = $booking_limit - ($booked_num+$seats_on_hold);
											if($aval_sea<0)
												$aval_sea=0;
											
											?>
											<div class="col-md-12 col-sm-12 col-xs-12">
                                            	 <?php if($aval_sea>0){ ?>
													<p class="text-muted"><?php echo pjSanitize::html($v['ticket']);?>:</p>
                                                 <?php } ?>
												<div class="form-horizontal pjCbFormSeats">
													<div class="form-group">
                                                    	<?php if($aval_sea>0) {?>
														<div class="col-sm-6">
															<select id="tbTicket_<?php echo $v['price_id'];?>" name="tickets[<?php echo $v['id'];?>][<?php echo $v['price_id'];?>]" class="form-control tbTicketSelector" data-id="<?php echo $v['price_id'];?>" data-ticket="<?php echo pjSanitize::html($v['ticket']);?>" data-price="<?php echo pjUtil::formatCurrencySign($v['price'], $tpl['option_arr']['o_currency']);?>">
																<?php
																if($aval_sea<10)
																	$count = $aval_sea;
																else
																	$count = 10;
																	
																for($i = 0; $i <= $count; $i++)
																{
																	?><option value="<?php echo $i;?>"<?php echo isset($STORE['tickets'][$v['id']][$v['price_id']]) ? ($STORE['tickets'][$v['id']][$v['price_id']] == $i ? ' selected="selected"' : null) : null;?>><?php echo $i?></option><?php
																} 
																?>
															</select>
														</div><!-- /.col-sm-6 -->
                                                         <?php }
																else{
																	echo "<span class='col-xs-12' style='color:green'>Seats may be avalibale after some time.</span>";
																}
														?>
														<?php if($aval_sea>0){ ?>
															<label for="" class="control-label">x<?php echo pjUtil::formatCurrencySign($v['price'], $tpl['option_arr']['o_currency']);?></label>					<?php } ?>
													</div><!-- /.form-group -->
												</div><!-- /.form-horizontal pjCbFormSeats -->
											</div>
											<?php
										}else{
											?>
											<div class="col-md-12 col-sm-12 col-xs-12">
												<p class="text-muted"><?php echo pjSanitize::html($v['ticket']);?>:</p>
												<p class="lead"><strong><?php __('front_na');?></strong></p>
											</div>
											<?php
										}
									}
								} 
								?>
							</div><!-- /.row -->
						</div><!-- /.well -->
					</div><!-- /.col-lg-6 col-md-6 col-sm-6 col-xs-12 -->
				</div><!-- /.row -->
				
				<div class="row">
					<div class="col-xs-12 tbGuideMessage" data-type=""></div>
				</div><!-- /.row -->
				<?php
				if(isset($tpl['venue_arr']))
				{
					$map = PJ_INSTALL_PATH . $tpl['venue_arr']['map_path'];
					if (is_file($map))
					{ 
						$size = getimagesize($map);
						?>
						<div class="row">
							<div class="col-xs-12">
								<div id="tbMapHolder_<?php echo $_GET['index'];?>" class="tbMapHolder pjCbCinema" style="height: <?php echo $size[1];?>px;">
									<div style="height: <?php echo $size[1];?>px;width:<?php echo $size[0];?>px;margin-left: 0px;margin:0 auto;position: relative;">
										<img id="tbMap_<?php echo $_GET['index'];?>" src="<?php echo PJ_INSTALL_URL . $tpl['venue_arr']['map_path']; ?>" alt="" style="margin: 0; border: none; position: absolute; top: 0; left: 0; z-index: 500;" />
										<?php
										//$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
										$date_time = $tpl['selected_date'] . ' ' . $STORE['selected_time'];
										
										/*** REMOVING SEATS WITH PENDING PAYMENT ***/
										$myfile = fopen("log.txt", "w");
										
			$sql = "SELECT id, created FROM thcbs_bookings WHERE status='pending'";
										$result = mysqli_query($db, $sql);
										if(!$result){
											fwrite($myfile,  mysqli_error($db));
										}
										while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
											$created = $row['created'];
											date_default_timezone_set('Asia/Kolkata');
											$created_timestamp = strtotime($created);
											$timestamp = time()-900;
											if($created_timestamp<$timestamp){
												fwrite($myfile, "inside if loop");
												$sqls = array("DELETE FROM thcbs_bookings_payments WHERE booking_id=".$row['id'], "DELETE FROM thcbs_bookings_shows WHERE booking_id=".$row['id'], "DELETE FROM thcbs_bookings_tickets WHERE booking_id=".$row['id'] );
												foreach($sqls as $sq){
													fwrite($myfile,  $sq);
													if(!mysqli_query($db, $sq))
														fwrite($myfile,  mysqli_error($db));
												}
												$sql = "UPDATE thcbs_bookings SET status='cancelled' WHERE id=".$row['id'];
												mysqli_query($db, $sql);
											}
										}
										
										/*** UNHOLDING SEATS HELD 25 MINUTES BEFORE***/
										$time = time()-1500;
										
										$sql = "UPDATE thcbs_shows_seats SET holding=0, held_time='' WHERE held_time<=$time";
										mysqli_query($db, $sql);
										
										$sql = "SELECT * FROM thcbs_shows_seats WHERE show_id=(SELECT id FROM thcbs_shows WHERE date_time='$date_time' AND event_id=".$tpl['arr']['id'].") AND holding=1";
										
										$result = mysqli_query($db, $sql);
										if(!$result){
											
											$myfile = fopen("th.txt", "a");
											$txt = mysqli_error($db);
											fwrite($myfile, $txt);
										}
										$holding_seats = array();
										/*echo "<script>console.log('".$date_time."')</script>";*/
										
										while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
											$holding_seats[] = $row['seat_id'];
										}
										$hldng_sts="";
										$selected_seats = array();
										foreach ($tpl['seat_arr'] as $seat)
										{
											$is_selected = false;
											$is_available = true;
											$_arr = explode("~:~", $seat['price_id']);
											/*echo "<script>console.log('".$seat['id']."')</script>";*/
											$seat_id = $seat['id'];
											$tooltip = array();
											
											foreach($_arr as $pid){
												if(isset($STORE['seat_id'][$pid][$seat['id']])){
													$selected_seats[] = $seat['id'];
												}
											}
											$holding_seats = array_diff($holding_seats, $selected_seats);
											foreach($_arr as $pid)
											{
												
												if(isset($STORE['seat_id'][$pid][$seat['id']]))
												{
													$hldng_sts = $hldng_sts.$seat['id'].",";
													$is_selected = true;
													if($seat['seats'] == $STORE['seat_id'][$pid][$seat['id']])
													{
														$is_available = false;
													}
												}
												$tooltip[] = $ticket_tooltip_arr[$pid];
											}
											$style="";
											if(in_array($seat_id, $holding_seats)){
												$avail_seats = 0;
												$style = "background: #f5ca8c;";
											}
											else
												$avail_seats = $seat['seats'] - $seat['cnt_booked'];
											
											?><span class="tbSeatRect<?php echo $avail_seats <= 0 ? ' tbSeatBlocked' : ($is_available == true ? ' tbSeatAvailable' : null); ?><?php echo $is_selected == true ? ' tbSeatSelected' : null;?>" <?php echo $is_selected == true ? 'data_price_id='.$seat['price_id'].' data_seat_id='.$seat['id'] : null;?> data-id="<?php echo $seat['id']; ?>" data-price-id="<?php echo $seat['price_id']; ?>" data-name="<?php echo $seat['name']; ?>" data-count="<?php echo $avail_seats; ?>" style="<?php echo $style; ?> width: <?php echo $seat['width']; ?>px; height: <?php echo $seat['height']; ?>px; left: <?php echo $seat['left']; ?>px; top: <?php echo $seat['top']; ?>px; line-height: <?php echo $seat['height']; ?>px" data-toggle="tooltip" data-placement="top" data-html="true" title='<?php echo join('<br/>', $tooltip);?>'><?php echo stripslashes($seat['name']); ?></span><?php
										}
										?>
									</div>
								</div>
							</div>
						</div><!-- /.row -->
						<?php
					}else{
						?>
						<div id="tbMapHolder_<?php echo $_GET['index'];?>">
							<?php
							foreach ($tpl['seat_arr'] as $seat)
							{
								$is_selected = false;
								$is_available = true;
								$_arr = explode("~:~", $seat['price_id']);
								foreach($_arr as $pid)
								{
									if(isset($STORE['seat_id'][$pid][$seat['id']]))
									{
										$is_selected = true;
										if($seat['seats'] == $STORE['seat_id'][$pid][$seat['id']])
										{
											$is_available = false;
										}
									}
								}
								?><span class="tbSeatRect<?php echo $seat['seats'] - $seat['cnt_booked'] <= 0 ? ' tbSeatBlocked' : ($is_available == true ? ' tbSeatAvailable' : null); ?><?php echo $is_selected == true ? ' tbSeatSelected' : null;?>" data-id="<?php echo $seat['id']; ?>" data-price-id="<?php echo $seat['price_id']; ?>" data-name="<?php echo $seat['name']; ?>" data-count="<?php echo $seat['seats']; ?>" style="display: none;"><?php echo stripslashes($seat['name']); ?></span><?php
							}
							?>
						</div>
						<?php
					}
				} 
				?>
				<div class="row">
					<?php
					if(isset($tpl['venue_arr']))
					{
						if (is_file($tpl['venue_arr']['map_path']))
						{
							?>
							<br/>
							<div class="col-xs-12 text-left">
								<ul class="list-inline pjCbSeatsColors">
									<li>
										<button class="btn btn-success btn-xs">&nbsp;&nbsp;&nbsp;&nbsp;</button>
										<label class="tbLegendLabel" for=""><?php __('front_available');?></label>
									</li>
			
									<li>
										<button class="btn btn-danger btn-xs" disabled="disabled">&nbsp;&nbsp;&nbsp;&nbsp;</button>
										<label class="tbLegendLabel" for="">Booked</label>
									</li>
                                    
                                    <li>
										<button class="btn btn-warning btn-xs" disabled="disabled">&nbsp;&nbsp;&nbsp;&nbsp;</button>
										<label class="tbLegendLabel" for="">Holding</label>
									</li>
			
									<li>
										<button class="btn btn-primary btn-xs" disabled="disabled">&nbsp;&nbsp;&nbsp;&nbsp;</button>
										<label class="tbLegendLabel" for=""><?php __('front_selected');?></label>
									</li>
								</ul><!-- /.list-inline -->
							</div><!-- /.col-lg-4 col-md-5 col-sm-6 col-xs-12 -->
							<?php
						}
					} 
					?>
				</div><!-- /.row -->
				<br />
	
			</div><!-- /.panel-body pjCbBody -->
			<?php
			if($tpl['seats_available'] == true)
			{ 
				?>
				<div class="panel-footer text-center pjCbFoot">
					<form id="tbSeatsForm_<?php echo $_GET['index'];?>" action="#" method="post" class="form-inline" style="display: none;">
						<?php
						if(isset($STORE['seat_id']))
						{
							$seat_label_arr = $STORE['seat_id'];
							foreach($seat_label_arr as $price_id => $seat_arr)
							{
								foreach($seat_arr as $seat_id => $cnt)
								{
									?><input class="tbHiddenSeat_<?php echo $price_id;?>" type="hidden" name="seat_id[<?php echo $price_id;?>][<?php echo $seat_id;?>]" data_seat_id="<?php echo $seat_id;?>" value="<?php echo $cnt;?>"><?php
								}
							}
						} 
						?>
					</form>
					
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<br />
							<div class="col-xs-12 tbErrorMessage pjCbSeatsMessage"></div>
					
							<button class="btn btn-default pull-left tbSelectorButton pjCbBtnBack pjCbBtn pjCbBtnSecondary<?php echo $STORE['back_to'] == 'details' ? ' tbBackToDetails' : ' tbBackToEvents';?>" data-id="<?php echo $tpl['arr']['id']; ?>" data-date="<?php echo date($tpl['option_arr']['o_date_format'], strtotime($tpl['hash_date'])); ?>" data-time="<?php echo $STORE['selected_time']; ?>" data-holding-seats="<?php echo $hldng_sts ?>"><?php __('front_button_cancel')?></button>
							
							<button class="btn btn-default pull-right tbSelectorButton tbContinueButton pjCbBtn pjCbBtnPrimary" data-date="<?php echo date($tpl['option_arr']['o_date_format'], strtotime($tpl['hash_date'])); ?>"><?php __('front_button_continue')?></button>
		
						</div><!-- /.col-md-12 col-sm-12 col-xs-12 -->
					</div>
					
						
				</div><!-- /.panel-footer text-center pjCbFoot -->
				<?php
			} 
			?>
		</div><!-- panel panel-default pjCbMain -->
	</div><!-- /.container-fluid -->
	<?php
}else{
	?>
	<div class="container-fluid">
		<div class="panel panel-default pjCbMain">
			<div class="panel-body pjCbBody">
				<p class="text-warning"><?php __('front_start_over_message');?></p>
			</div>
			<div class="panel-footer text-center pjCbFoot">
				<div class="row">
					<div class="col-md-7 col-sm-12 col-xs-12">&nbsp;</div>
					<div class="col-md-5 col-sm-12 col-xs-12 text-right">
						<br />
						<button class="btn btn-default tbSelectorButton tbStartOverButton pjCbBtn pjCbBtnSecondary"><?php __('front_button_start_over')?></button>
					</div><!-- /.col-md-5 col-sm-12 col-xs-12 -->
				</div>
			</div><!-- /.panel-footer text-center pjCbFoot -->
		</div>
	</div>
	<?php
}
?>