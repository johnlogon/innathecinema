<br/>
<?php
	$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/report/classes/report.php");
?>
<div class="container-fluid">
	<div class="panel panel-default pjCbMain">
		<div class="panel-heading tbPanelHeading pjCbHeading">
			<form action="#" method="post" class="form-horizontal pjCbForm pjCbFormDate">
				<div class="form-group clearfix">
                    <?php
                    include_once PJ_VIEWS_PATH . 'pjFront/elements/layout_1/locale.php';
                    ?>
					<div class="pull-left pjCbWeekPanel">
						<?php
						if(date('Y-m-d', $tpl['from_ts']) > date('Y-m-d'))
						{ 
							?>
							<a href="#" class="pjCbDaysNav pjCbNavArrowLeft" data-from_date="<?php echo date($tpl['option_arr']['o_date_format'], $tpl['from_ts'] - (86400 * 7));?>" data-date="<?php echo date($tpl['option_arr']['o_date_format'], $tpl['from_ts'] - (86400 * 7));?>"><i class="fa fa-angle-double-left"></i></a>
							<?php
						}
						for($i = $tpl['from_ts']; $i < $tpl['end_ts']; $i+=86400)
						{
							$date_format = date($tpl['option_arr']['o_date_format'], $i);
							?><a href="#" class="pjCbDaysNav<?php echo date('Y-m-d', $i) == $tpl['hash_date'] ? ' active' : NULL;?>" data-from_date="<?php echo date($tpl['option_arr']['o_date_format'], $tpl['from_ts']);?>" data-date="<?php echo $date_format;?>"><?php echo $date_format;?></a><?php
						} 
						?>
						<a href="#" class="pjCbDaysNav pjCbNavArrowRight" data-from_date="<?php echo date($tpl['option_arr']['o_date_format'], $tpl['end_ts']);?>" data-date="<?php echo date($tpl['option_arr']['o_date_format'], $tpl['end_ts']);?>"><i class="fa fa-angle-double-right"></i></a>
					</div>
					<?php
					$week_start = isset($tpl['option_arr']['o_week_start']) && in_array((int) $tpl['option_arr']['o_week_start'], range(0,6)) ? (int) $tpl['option_arr']['o_week_start'] : 0;
					$jqDateFormat = pjUtil::jqDateFormat($tpl['option_arr']['o_date_format']);
					$months = __('months', true);
					$short_months = __('short_months', true);
					ksort($months);
					ksort($short_months);
					$days = __('days', true);
					$short_days = __('short_days', true);
					?>
					<div class="pull-left pjCbWeekPanelDatePicker">
						<label for="" class="control-label"><?php __('front_select_date')?>:</label>
						<div class="pjCbFormControls">
							<div class="input-group date">
								<input type="text" name="selected_date" readonly="readonly" value="<?php echo date($tpl['option_arr']['o_date_format'], strtotime($tpl['hash_date'])); ?>" class="form-control tbSelectorDatepick" data-id="<?php echo $tpl['arr']['id'];?>" data-list="1" data-dformat="<?php echo $jqDateFormat; ?>" data-fday="<?php echo $week_start; ?>" data-months="<?php echo join(',', $months);?>" data-shortmonths="<?php echo join(',', $short_months);?>" data-day="<?php echo join(',', $days);?>" data-daymin="<?php echo join(',', $short_days);?>">
								<span class="input-group-addon tbSelectorDatepickIcon">
									<i class="fa fa-calendar"></i>
								</span>
							</div><!-- /.input-group date -->
						</div><!-- /.pjCbFormControls -->
					</div><!-- /.pull-left pjCbWeekPanelDatePicker -->
				</div><!-- /.form-group -->
			</form><!-- /.form-horizontal -->
		</div><!-- /.panel-heading tbPanelHeading pjCbHeading -->
		<div class="panel-body pjCbBody pjCbEvents">
			<?php
			
			if(count($tpl['arr']) > 0)
			{ 
				?>
				<div class="pjCbEventList">
					<?php
					foreach($tpl['arr'] as $v)
					{
						$src = 'http://placehold.it/220x320';
						if(!empty($v['event_img']) && is_file(PJ_INSTALL_PATH . $v['event_img']))
						{
							$src = PJ_INSTALL_URL . $v['event_img'];
						}
						?>
						<div class="pjCbEventRow">
							<div class="pjCbMovieIntroImage">
								<a href="#" class="tbMovieLink pjCbMovie" data-id="<?php echo $v['id'];?>" data-date="<?php echo date($tpl['option_arr']['o_date_format'], strtotime($tpl['hash_date']));?>" data-from_date="<?php echo date($tpl['option_arr']['o_date_format'], $tpl['from_ts']);?>"><img src="<?php echo $src;?>" class="img-responsive" alt="" /></a>
							</div>
							<div class="pjCbMovieIntroContent">
								<div class="pjCbMovieTitle"><a href="#" class="tbMovieLink pjCbMovie" data-id="<?php echo $v['id'];?>" data-date="<?php echo date($tpl['option_arr']['o_date_format'], strtotime($tpl['hash_date']));?>" data-from_date="<?php echo date($tpl['option_arr']['o_date_format'], $tpl['from_ts']);?>"><?php echo pjSanitize::html($v['title']);?></a></div>
                                <div class="pjCbMovieDuration" style="display:block;color: #337ab7;">
									<?php
										$selectedDate = date('Y-m-d', strtotime($tpl['hash_date']));
                                        $sql="SELECT 
											  content,
											  foreign_id 
											FROM
											  thcbs_multi_lang 
											WHERE foreign_id = 
											  (SELECT 
												venue_id 
											  FROM
												thcbs_shows 
											  WHERE event_id = ".$v['id']." AND date_time LIKE '".$selectedDate."%'
											  ORDER BY id DESC LIMIT 1) 
											  AND model = 'pjVenue' 
											  AND locale = 1 
											  AND FIELD= 'name'";
                                        $result=mysqli_query($db, $sql);
											$html="";
                                        while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
											$sql = "SELECT seats_count FROM thcbs_venues WHERE id=".$row['foreign_id'];
											$result1=mysqli_query($db, $sql);
											$row1=mysqli_fetch_array($result1, MYSQLI_ASSOC);
                                            $html.=' <span style="font-weight:bold">'.$row['content'];
											if($row['content']=='Screen 1')
												$html.=' (4K, DOLBY Atmos)</span>';
											else if($row['content']=='Screen 2')
												$html.=' (2K, 3D)</span>';
											$html.="<br><span> Total Seating Capacity:".$row1['seats_count']."</span>";
											if($row['content']=='Screen 1')
												$html.='<br><img src="http://sandhyacinehouse.com/images/4k.jpg" style="width: 49px;">
  <img src="http://sandhyacinehouse.com/images/atmos.jpg" style="width: 51px;">';
  											else if($row['content']=='Screen 2')
												$html.="<br><img src='http://sandhyacinehouse.com/images/3d.jpg' style='width: 49px;'>";
                                        }
										echo rtrim($html);
                                    ?>
                                </div>
								<div class="pjCbMovieDuration"><?php echo $v['duration'];?> <?php __('front_minutes');?></div>
								<div class="pjCbMovieDesc"><?php echo stripslashes(pjUtil::truncateDescription(pjUtil::html2txt($v['description']), 300, ' '));?></div>
								<div class="pjCbMovieTime">
									<label><?php __('front_select_time');?>:</label>
									<div class="pjCbMovieTimesWrapper">
										<?php
										if(isset($tpl['show_arr'][$v['id']]))
										{
											
											foreach($tpl['show_arr'][$v['id']] as $k => $time)
											{
												$date_time_iso = $tpl['hash_date'] . ' ' . $time . ':00';
												$date_time_ts = strtotime($tpl['hash_date'] . ' ' . $time . ':00');
												
												$show_time = date($tpl['option_arr']['o_time_format'], strtotime($date_time_iso));
												$show_date_time = $date_time_iso;
												$event_id = $v['id'];
												$sql = "SELECT booking_disabled FROM thcbs_shows WHERE date_time='".$show_date_time."' 
														AND event_id=".$event_id;
												$result=mysqli_query($db, $sql);
												
												
												$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
												$disabled=$row['booking_disabled'];
												
												//\\
												$t = $tpl['option_arr']['o_booking_earlier'] * 60;
													
												date_default_timezone_set('Asia/Kolkata');
												if($date_time_ts-$t <= time()){
													$disabled=1;
													try{
														/*$report = new Report;
														$data = array();
														$data['event_id']=$event_id;
														$data['date_time']=$show_date_time;
														$data['show_date_time'] = $show_date_time;
														$report->sentShowReport($data);*/
													}
													catch(Exception $e){
													
													}
												}
												$show_available_seats = 0;
												if($date_time_ts > time())
													$show_available_seats=1;
												//\\
												
												if($date_time_ts <= (strtotime(date('Y-m-d H:00')) + $tpl['option_arr']['o_booking_earlier'] * 60 ) ||
												   $disabled=='1')
												{
													if($show_available_seats==1){
														/*	CALCULATING AVAILABLE SEATS AT COUNTER */
															//total seats
															$sql = "SELECT seats_count FROM thcbs_venues WHERE id=(SELECT venue_id FROM thcbs_shows WHERE 
																	event_id=".$event_id." AND date_time='".$show_date_time."')";
															$row=mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
															$total_seats = $row['seats_count'];
															
															//number of booked tickets
															
															$sql="SELECT COUNT(id) AS tickets FROM thcbs_bookings_tickets WHERE booking_id IN(SELECT id FROM 
															thcbs_bookings WHERE event_id=".$event_id." AND date_time='".$show_date_time."' AND 
															status='confirmed')";
															
															$result=mysqli_query($db, $sql);
															if(!$result){
																echo mysqli_error($db);
																die();
															}
															$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
															$booked_num = $row['tickets'];
															$ticket_available = $total_seats-$booked_num;
														/*******************************************/
													}
													?>
													<a href="#" data-disabled="<?php echo $disabled; ?>" class="tbSelectorSeats pjCbTimePassed" style="text-align: center;"><?php echo $show_time;?>&nbsp; <span style="color:red"> (Reservation Closed) </span>
                                                    <?php if($show_available_seats==1){?>
                                                    	<br /><span style="color:green; display:block">Available at counter:<?php echo " ".$ticket_available." Seats"; ?>
                                                        </span>
                                                      <?php } ?>
                                                     </a>
													<?php
												}else{
													?>
													<a href="#" data-disabled="<?php echo $disabled; ?>" class="tbSelectorSeats" data-id="<?php echo $v['id'];?>" data-date="<?php echo date($tpl['option_arr']['o_date_format'], strtotime($tpl['hash_date']));?>" data-time="<?php echo $time; ?>" data-from_date="<?php echo date($tpl['option_arr']['o_date_format'], $tpl['from_ts']);?>"><?php echo $show_time;?></a>
													<?php
												}	
											}
										} 
										?>
									</div><!-- /.pjCbMovieTimesWrapper -->
								</div>
							</div>
						</div>
						<?php
					} 
					?>
				</div>
				<?php
			} else {
				//__('front_no_events_found');
				$show = new pjShow;
				$events = $show->getActiveEvents();
				include_once('no_events.php');
			}
			?>
		</div>
	</div>
</div>