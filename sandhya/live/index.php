<?php
	/*if(!isset($_POST['session_id']) || empty($_POST['session_id'])){
		echo "<h1>Forbidden</h1>
				<p>You don't have permission to access /live
				on this server.</p>
				<p>Additionally, a 404 Not Found
				error was encountered while trying to use an ErrorDocument to handle the request.</p>";
		die();
	}*/
	require_once('live_data.php');
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="http://innathecinema.com/sandhya/app/web/css/admin.css">
<link rel="stylesheet" href="css/live.css" />
<link rel="stylesheet" href="css/ripple.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="js/live.js" type="text/javascript"></script>
<style>
	
</style>
<div id="no-loader">
	<div id="header">
        <div id="logo">
            <a href="http://innathecinema.com/sandhya" rel="nofollow">Sandhya Cine House</a>
            <span>v1.0</span>
        </div>
    </div>
    <div class='container'>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <label class="lbl">Select Movie</label>
                    </div>
                    <div class="col-sm-6">
                        <select name="movie_sel" id="select_movie" class="form-control">&nbsp;&nbsp;
                            <option value="0">--Select Movie--</option>
                            <?php
                                foreach($movies as $movie){
                                    if(!empty($movie))
                                    echo '<option value="'.$movie['id'].'">'.$movie['content'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label  class="lbl">Select Venue</label>
                    </div>
                    <div class="col-sm-6">
                        <select name="venue_sel" id="select_venue" class="form-control">&nbsp;&nbsp;
                            <option value="0">--Select Venue--</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label  class="lbl">Select Date</label>
                    </div>
                    <div class="col-sm-6">
                        <input type="date" id="date_sel" name="select_date" class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label  class="lbl">Select Show Time</label>
                    </div>
                    <div class="col-sm-6">
                        <select name="showtime_sel" id="select_showtime" class="form-control">&nbsp;&nbsp;
                            <option value="0">--Select Time--</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <input type="button" class="go_btn btn btn-default" id="go_btn" value="Go" />
                </div>
            </div>
            <div class="col-sm-6 status" style="display:none">
                <div class="row">
                    <div class="col-sm-6">
                        <label  class="lbl">Total Booked Seats:</label>
                    </div>
                    <div class="col-sm-6" style="margin-left:-100px">
                        <label  class="lbl booked_count" style="float:left"></label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label  class="lbl">Total Seats on Hold:</label>
                    </div>
                    <div class="col-sm-6" style="margin-left:-100px">
                        <label  class="lbl holding_count" style="float:left"></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container map_container" style="display:none">
    	<div class="row">
        	<div class="col-sm-12" style="overflow-y: auto;">
            	 <div style="height: 750px;width:1000px;margin-left: 0px;margin:0 auto;position: relative;" class="map-holder">
                 </div>
            </div>
        </div>
    </div>
</div>
<div class='uil-squares-css' id="loader-ring" style='transform:scale(0.5);'>
	<div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
<script>
	$('.datepicker').datepicker({
  format: 'mm/dd/yyyy',
  startDate: '-3d'
})
</script>