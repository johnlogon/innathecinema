
$(document).ready(function(e) {
    $('#select_movie').on('change', function(){
		resetValues();
		var event_id = $('#select_movie option:selected').val();
		if(event_id!=''){
			$.ajax({
				type: "POST",
				async: true,
				url: "../live/request_handler.php",
				data:{'action': 'movie', 'event_id':event_id},
				cache: false,
				beforeSend: function() {
					
				},
				success: function(data) {
					var obj =$.parseJSON(data);
					html='<option value="0">--Select Venue--</option>';
					for(var i=0;obj[i]!=null;i++){
						if(!(typeof(obj[i]['content'])==='undefined'));
							html+="<option value='"+obj[i]['venue_id']+"'>"+obj[i]['content']+"</option>";
					}
					$('#select_venue').html(html);
				},
				error:function(){
					console.log("Error");
				}
			})
		}
	})
	$('#date_sel').on('change', function(){
		var date = $(this).val();
		var event_id = $('#select_movie option:selected').val();
		var venue_id = $('#select_venue option:selected').val();
		if(date!='' && event_id!='' && venue_id!=''){
			$.ajax({
				type: "POST",
				async: true,
				url: "../live/request_handler.php",
				data:{'action': 'date', 'event_id':event_id, 'venue_id':venue_id, 'date':date},
				cache: false,
				beforeSend: function() {
					
				},
				success: function(data) {
					var obj =$.parseJSON(data);
					html='<option value="0">--Select Time--</option>';
					for(var i=0;obj[i]!=null;i++){
						if(i==0)
							continue;
						if(!(typeof(obj[i])==='undefined'));
							html+="<option value='"+obj[i]+"'>"+obj[i]+"</option>";
					}
					$('#select_showtime').html(html);
				},
				error:function(){
					console.log("Error");
				}
			})
		}
	})
	$('#go_btn').on('click', function(){
		if(validate()){
			$('.status').hide();
			getInfo(true);
		}
	});
	function getInfo(global){
		var date = $('#date_sel').val();
		var time = $('#select_showtime').val();
		var event_id = $('#select_movie option:selected').val();
		var venue_id = $('#select_venue option:selected').val();
		if(date!='' && event_id!='' && venue_id!='' && time!='' && time!=0){
			$.ajax({
				type: "POST",
				async: true,
				url: "../live/request_handler.php",
				data:{'action': 'go', 'event_id':event_id, 'venue_id':venue_id, 'date_time':date+" "+time},
				cache: false,
				global:global,
				beforeSend: function() {
					
				},
				success: function(data) {
					var obj=$.parseJSON(data);
					var booked_seats = obj.booked_seats;
					var holding_seats = obj.holding_seats;
					var booked_count = obj.booked_count;
					var holding_count = obj.holding_count;
					console.log(JSON.stringify(holding_seats));
					
					var html = "<img src='"+obj['venue_img']+"' class='venue_map'>";
					for(var i=0;obj['seats'][i]!=null;i++){
						var seat = obj['seats'][i];
						var cls = "tbSeatAvailable";
						if(jQuery.inArray(seat['id'], booked_seats) !== -1)
							cls="tbSeatBooked";
						else if(jQuery.inArray(seat['id'], holding_seats) !== -1)
							cls="tbSeatHolding";
						html+='<span class="tbSeatRect '+cls+'" data-id="'+seat['id']+'"  data-name="'+seat['name']+'" style=" width: '+seat['width']+'px; height: '+seat['height']+'px; left: '+seat['left']+'px; top: '+seat['top']+'px; line-height: '+seat['height']+'px" data-toggle="tooltip" data-placement="top" data-html="true">'+seat['name']+'</span>';
					}
					$('.map-holder').html(html);
					$('.map_container').show();
					$('.booked_count').text(booked_count);
					$('.holding_count').text(holding_count);
					$('.status').show();
					
				},
				error:function(){
					console.log("Error");
				}
			});
		}
		else
			return false;
	}
	function checkAgain(){
		getInfo(false);
	}
	window.setInterval(function(){
	 	checkAgain(false);
	}, 1000*5);
});

$(document).ready(function(e) {
	$(document).ajaxStart(function() {
		$('#loader-ring').css('display', 'block');
		$('#no-loader').css('pointer-events', 'none')
				 .css('opacity', '0.3');
    });
   	$(document).ajaxComplete(function(event, XMLHttpRequest, ajaxOptions) {
        $('#no-loader').css('pointer-events', 'auto')
				 		.css('opacity', '1');
		$('#loader-ring').css('display', 'none');
    });
	
});

function validate(){
	var event_id = $('#select_movie option:selected').val();
	if(event_id==0 || event_id==null)
		return false;
	var venue_id = $('#select_venue option:selected').val();
	if(venue_id==0 || venue_id==null)
		return false;
	if($('#date_sel').val()=='')
		return false;
	return true;
}
function resetValues(){
	$('#select_venue').html('<option value="0">--Select Venue--</option>');
	$('#select_showtime').html('<option value="0">--Select Time--</option>');
	$('#date_sel').val('');
}