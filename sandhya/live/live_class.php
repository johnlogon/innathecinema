<?php
	/*
		Author  : Suhail;
		Created : 24-11-2016;
	*/
	
	class LiveMonitor{
		
		public function logger($log){
			$logFile = fopen('live.txt', 'a');
			fwrite($logFile, $log."\n");
			fclose($logFile);
		}
		private function connect(){
			error_reporting(E_ALL);
			ini_set('display_errors', 'on');
			$mysqli = new mysqli(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
			if ($mysqli->connect_error) {
				$this->logger("Connection failed: " . $mysqli->connect_error);
				die();
				return false;
			}
			return $mysqli;
		}
		public function getMovies(){
				$this->logger(">>getMovies");
				$mysqli = $this->connect();
				if($mysqli==false){
					$this->logger('Database connection error');
					return false;
				}
				$this->logger('Database connected');
				$sql = "SELECT thcbs_events.`id`, thcbs_multi_lang.`content` FROM thcbs_events INNER JOIN thcbs_multi_lang 
						ON thcbs_multi_lang.`model`='pjEvent' AND thcbs_events.`id`=thcbs_multi_lang.`foreign_id` 
						AND thcbs_multi_lang.`field`='title' AND thcbs_multi_lang.`locale`=1 AND thcbs_events.`status`='T'";
				if($stmt = $mysqli->prepare($sql)){
					$r = $stmt->execute();
					$stmt->bind_result($id, $content);
					$data[] = array();
					$i =0;
					while($stmt->fetch()){
						$data[$i]['id'] = $id;
						$data[$i++]['content'] = $content;
					}
					$stmt->close();
					return $data;
				}
				else{
					$this->logger(mysqli_stmt_error($stmt));
				}
		}
		public function getMovieData($venue_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT DISTINCT 
					  thcbs_shows.`venue_id`,
					  thcbs_multi_lang.`content` 
					FROM
					  thcbs_shows
					  INNER JOIN thcbs_multi_lang 
						ON thcbs_shows.`venue_id` = thcbs_multi_lang.`foreign_id`
						AND thcbs_multi_lang.`model` = 'pjVenue' 
						AND thcbs_multi_lang.`locale` = 1 
						AND thcbs_multi_lang.`field` = 'name' 
						AND thcbs_shows.`event_id`=?";		
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $venue_id);
			$stmt->execute();
			$stmt->bind_result($id, $content);
			
			$data[] = array();
			$i=0;
			while($stmt->fetch()){
				$data[$i]['venue_id'] = $id;
				$data[$i++]['content'] = $content;
			}
			$stmt->close();
			return $data;
		}
		public function getShowTimes($params){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT date_time FROM thcbs_shows WHERE 
					event_id=".$params['event_id']." AND 
					venue_id=".$params['venue_id']." AND 
					date_time>'".$params['date']." 00:00:00' AND 
					date_time<'".$params['date']." 23:59:59' ORDER BY date_time ASC";
			$stmt = $mysqli->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($date_time);
			
			$data[] = array();
			while($stmt->fetch()){
				$date=date_create($date_time);
				$data[] = date_format($date,"h:i A");;
			}
			$stmt->close();
			return $data;
		}
		public function getShowInfo($params){
			$data = array();
			$data['venue_img'] = $this->getSeatMap($params['venue_id']);
			$data['seats'] = $this->getSeats($params['venue_id']);
			$data['booked_seats'] = $this->getBookedSeats($params);
			$data['holding_seats'] = $this->getHoldingSeats($params);
			foreach($data['holding_seats'] as $seat){
				if(in_array($seat, $data['booked_seats'])){
					$data['holding_seats'] = array_diff($data['holding_seats'], array($seat));
				}
			}
			$data['holding_seats'] = array_diff($data['holding_seats'], $data['booked_seats']);
			$data['booked_count']= count($data['booked_seats']);
			$data['holding_count']= count($data['holding_seats']);
			return $data;
		}
		private function getSeatMap($venue_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql='SELECT map_path FROM thcbs_venues WHERE id=?';
			$stmt = $mysqli->prepare($sql);
			if($stmt==false)
				$this->logger("error");
			$stmt->bind_param('i', $venue_id);
			$stmt->execute();
			$stmt->bind_result($map);
			while($stmt->fetch()){
				$event_image= "http://".$_SERVER['SERVER_NAME']."/sandhya/".$map;
			}
			return $event_image;
		}
		private function getSeats($venue_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$seats = array();
			$sql='SELECT * FROM thcbs_seats WHERE venue_id=?';
			$stmt = $mysqli->prepare($sql);
			if($stmt==false)
				$this->logger("error");
			$stmt->bind_param('i', $venue_id);
			$stmt->execute();
			$stmt->bind_result($id,$venue,$width,$height,$top,$left,$name,$s);
			$i=0;
			while($stmt->fetch()){
				$seats[$i]['width'] = $width;
				$seats[$i]['height'] = $height;
				$seats[$i]['top'] = $top;
				$seats[$i]['left'] = $left;
				$seats[$i]['id'] = $id;
				$seats[$i++]['name'] = $name;
				
			}
			return $seats;
		}
		private function getBookedSeats($params){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
				
			$sql = "SELECT seat_id FROM thcbs_bookings_shows WHERE show_id=? AND booking_id IN
					(SELECT id FROM thcbs_bookings WHERE STATUS='confirmed' AND event_id=?
					  AND date_time=?)";
			$stmt = $mysqli->prepare($sql);
			if($stmt==false)
				$this->logger("error");
			$show_id = $this->getShowId($params);
			$stmt->bind_param('iis', $show_id,$params['event_id'],$params['date_time']);
			$stmt->execute();
			$stmt->bind_result($seat_id);
			
			$booked_seats = array();
			while($stmt->fetch()){
				$booked_seats[] = $seat_id;
			}
			return $booked_seats;
		}
		private function getShowId($params){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT id FROM thcbs_shows WHERE event_id=? AND venue_id=? AND date_time=?";
			$stmt = $mysqli->prepare($sql);
			if($stmt==false)
				$this->logger("error");
			$stmt->bind_param('iis', $params['event_id'],$params['venue_id'],$params['date_time']);
			$stmt->execute();
			$stmt->bind_result($id);
			
			$stmt->fetch();
			return $id;
		}
		private function getHoldingSeats($params){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT seat_id FROM thcbs_shows_seats WHERE holding=1 AND show_id=?";
			$stmt = $mysqli->prepare($sql);
			if($stmt==false)
				$this->logger("error");
			$show_id = $this->getShowId($params);
			$stmt->bind_param('i', $show_id);
			$stmt->execute();
			$stmt->bind_result($seat_id);
			
			$holding_seats = array();
			while($stmt->fetch()){
				$holding_seats[] = $seat_id;
			}
			return $holding_seats;			
		}
	}
?>