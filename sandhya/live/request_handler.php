<?php
	require_once('../loader.php');
	require_once('live_class.php');
	if(isset($_POST)){
		$obj = new LiveMonitor;
		if(isset($_POST['action'])){
			//on movie change
			if($_POST['action']=='movie'){
				$event_id = $_POST['event_id'];
				$data = $obj->getMovieData($event_id);
				echo json_encode($data);
			}
			else if($_POST['action']=='date'){
				$params=array();
				$params['event_id']=$_POST['event_id'];
				$params['venue_id']=$_POST['venue_id'];
				$params['date'] = $_POST['date'];
				$data = $obj->getShowTimes($params);
				echo json_encode($data);
			}
			else if($_POST['action']=='go'){
				$params=array();
				$params['event_id']=$_POST['event_id'];
				$params['venue_id']=$_POST['venue_id'];
				$date=date_create($_POST['date_time']);
				$params['date_time'] = date_format($date,"Y-m-d H:i:s");;
				$data = $obj->getShowInfo($params);
				echo json_encode($data);
			}
		}
	}
?>