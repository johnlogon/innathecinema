<?php
	/*
		Author  : Suhail;
		Created : 21-01-2017;
	*/
	
	class CollectionReport extends pjAdmin{
		
		public function logger($log){
			$logFile = fopen($_SERVER['DOCUMENT_ROOT'].'/sandhya/collection-report/log.txt', 'a');
			date_default_timezone_set('Asia/Kolkata');
			fwrite($logFile, $log."  time:".date("d-m-Y h:i:s A", time())."\n");
			fclose($logFile);
		}
		private function connect(){
			$mysqli = new mysqli(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
			if ($mysqli->connect_error) {
				$this->logger("Connection failed: " . $mysqli->connect_error);
				return false;
			}
			return $mysqli;
		}
				
		public function getMovieCollectionByDateRange($params){
			if(empty($params['movie_id']) || $params['movie_id']=="")
				$movie_show_arr = $this->getShows($params, $this->getMovies());
			else{
				$movie_show_arr = $this->getShows($params,$this->getMovieById($params['movie_id']));
			}
			foreach($movie_show_arr as &$arr){
				$shows = &$arr['shows'];
				foreach($shows as &$show){
					$data = $this->getBookingsInfoByShowId($show['id']);
					$show['total_seats'] = $data['total_seats'];
					$show['total_bookings'] = $data['total_bookings'];
				}
			}
			return $movie_show_arr;
		}
		
		public function generatePdf($params){
			$pdfParams['modal'] = $this->getPdfHtml($params);
			$pdfParams['fileName'] = 'Collection_Report';
			$pdf = new PDF;
			$pdf->generateReport($pdfParams);
			return $_SERVER['SERVER_NAME']."/sandhya/temp/Collection_Report.pdf";
		}
		private function getPdfHtml($params){
			$html = '<html>
						<head>
							<style>
								td,th{
									font-size:14px;
									text-align:center;
								}
							</style>
						</head>
						<body>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<br>
							<div>
								<h2 align="center" style="font-size:18px"><b><u>Collection Report</u></b></h2>
								<table border="1">
									<tr>
										<th width="25%"><h4>Movie</h4></th>
										<th width="20%"><h4>Show</h4></th>
										<th width="15%"><h4>Total Bookings</h4></th>
										<th width="15%"><h4>Total seats</h4></th>
										<th width="25%"><h4>Total amount</h4></th>
									</tr>';
					$tbody = $this->getPdfRows($params);
					$html = $html . $tbody;
					$html = $html . '</table>
								<br>
								<br>
								<br>
								<br>
								<br>
								
							</div>
						</body>
					  </html>';
					  return $html;
		}
		private function getPdfRows($params){
			$movies = $this->getMovieCollectionByDateRange($params);
			$html = "";
			$total_price = 0;
			$total_bookings = 0;
			$total_seats = 0;
			foreach($movies as $movie){
				$shows = $movie['shows'];
				if(!empty($shows)){
					foreach($shows as $show){
						$price = $show['total_seats'] * $movie['price'];
						$html.="<tr>
									<td>".$movie['movie']."</td>
									<td>".$show['date_time']."</td>
									<td>".$show['total_bookings']."</td>
									<td>".$show['total_seats']."</td>
									<td>".$price."</td>
								</tr>";
						$total_price +=$price;
						$total_bookings+=$show['total_bookings'];
						$total_seats+=$show['total_seats'];
					}
				}
			}
			$html.="<tfoot>
						<tr>
							<td></td>
							<td>Total</td>
							<td>".$total_bookings."</td>
							<td>".$total_seats."</td>
							<td>".$total_price."</td>
						</tr>
					</tfoot>";
			return $html;
		}
		public function getMovies(){
			$mysqli = $this->connect();
			if($mysqli==false){
				$this->logger('Database connection error');
				return false;
			}
			$sql = "SELECT thcbs_events.`id`, thcbs_multi_lang.`content` FROM thcbs_events INNER JOIN thcbs_multi_lang 
					ON thcbs_multi_lang.`model`='pjEvent' AND thcbs_events.`id`=thcbs_multi_lang.`foreign_id` 
					AND thcbs_multi_lang.`field`='title' AND thcbs_multi_lang.`locale`=1"; // AND thcbs_events.`status`='T'
			if($stmt = $mysqli->prepare($sql)){
				$r = $stmt->execute();
				$stmt->bind_result($id, $content);
				$data[] = array();
				$i =0;
				while($stmt->fetch()){
					$data[$i]['id'] = $id;
					$data[$i++]['content'] = $content;
				}
				$stmt->close();
				return $data;
			}
		}
		public function getMovieById($event_id){
			$mysqli = $this->connect();
			if($mysqli==false){
				$this->logger('Database connection error');
				return false;
			}
			$sql = "SELECT thcbs_events.`id`, thcbs_multi_lang.`content` FROM thcbs_events INNER JOIN thcbs_multi_lang 
					ON thcbs_multi_lang.`model`='pjEvent' AND thcbs_events.`id`=thcbs_multi_lang.`foreign_id` 
					AND thcbs_multi_lang.`field`='title' AND thcbs_multi_lang.`locale`=1 WHERE thcbs_events.`id`=?";
			if($stmt = $mysqli->prepare($sql)){
				$stmt->bind_param('i', $event_id);
				$r = $stmt->execute();
				$stmt->bind_result($id, $content);
				$data[] = array();
				$i =0;
				while($stmt->fetch()){
					$data[$i]['id'] = $id;
					$data[$i++]['content'] = $content;
				}
				$stmt->close();
				return $data;
			}
		}
		private function getShows($params, $events){
			$mysqli = $this->connect();
			$data = array();
			if($mysqli==false){
				$this->logger('Database connection error');
				return false;
			}
			$i=0;
			$price =  $this->getTicketPrice();;
			foreach($events as $event){
				$data[$i]['event_id'] =$event['id'];
				$data[$i]['movie'] = $event['content'];
				$data[$i]['price'] = $price;
				$data[$i++]['shows'] = $this->getShowsByEventIdAndDateTime($params, $event['id']);
			}
			return $data;
		}
		private function getShowsByEventIdAndDateTime($params, $event_id){
			$mysqli = $this->connect();
			if($mysqli==false){
				$this->logger('Database connection error');
				return false;
			}
			$sql = "SELECT 
					  id,
					  event_id,
					  venue_id,
					  date_time 
					FROM
					  thcbs_shows 
					WHERE event_id = ?";
			if(!empty($params['from']) || !empty($params['to'])) 
				$sql.=" AND date_time >= ?
					AND date_time <= ?";
			if($stmt = $mysqli->prepare($sql)){
				if(!empty($params['from']) || !empty($params['to'])) 
					$stmt->bind_param('iss', $event_id, $params['from'], $params['to']);
				else
					$stmt->bind_param('i', $event_id);
				$stmt->execute();
				$stmt->bind_result($id, $event_id,$venue_id,$date_time);
				$data = array();
				$i =0;
				while($stmt->fetch()){
					$data[$i]['id'] = $id;
					$data[$i]['venue_id'] = $venue_id;
					$data[$i++]['date_time'] = date('d-m-Y h:i A', strtotime($date_time));
				}
				$stmt->close();
				return $data;
			}
			else{
				$this->logger($mysqli->error);
				return false;
			}
		}
		private function getBookingsInfoByShowId($show_id){
			$mysqli = $this->connect();
			if($mysqli==false){
				$this->logger('Database connection error');
				return false;
			}
			$sql = "SELECT 
					  COUNT(thcbs_bookings_shows.`booking_id`) AS total_seats,
					  COUNT(DISTINCT thcbs_bookings.id) AS total_bookings
					FROM
					  thcbs_bookings_shows 
					  INNER JOIN thcbs_bookings 
						ON thcbs_bookings_shows.`booking_id` = thcbs_bookings.`id` 
					WHERE thcbs_bookings.`status` = 'confirmed' AND thcbs_bookings.is_admin=0
					  AND thcbs_bookings_shows.`show_id` = ?";
			if($stmt = $mysqli->prepare($sql)){
				$stmt->bind_param('i', $show_id);
				$stmt->execute();
				$stmt->bind_result($seats, $bookings);
				$data = array();
				$i =0;
				$stmt->fetch();
				
				$data['total_seats'] = $seats;
				$data['total_bookings'] = $bookings;
				
				$stmt->close();
				return $data;
			}
			else{
				$this->logger($mysqli->error);
				return false;
			}
		}
		private function getTicketPrice(){
			$mysqli = $this->connect();
			if($mysqli==false){
				$this->logger('Database connection error');
				return false;
			}
			$sql = "SELECT 
					  ticket_price 
					FROM
					  thcbs_report_settings 
					WHERE id = 1 ";
			if($stmt = $mysqli->prepare($sql)){
				$stmt->execute();
				$stmt->bind_result($price);
				$stmt->fetch();
				$stmt->close();
				return $price;
			}
			else{
				$this->logger($mysqli->error);
				return false;
			}
		}
	}
?>