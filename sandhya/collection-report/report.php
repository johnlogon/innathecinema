<?php
	if(!isset($_POST['session_id']) || empty($_POST['session_id'])){
		echo "<h1>Forbidden</h1>
				<p>You don't have permission to access /report
				on this server.</p>
				<p>Additionally, a 404 Not Found
				error was encountered while trying to use an ErrorDocument to handle the request.</p>";
		die();
	}
	session_id($_POST['session_id']);
	session_start();
	require_once('data/index_data.php');
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="http://innathecinema.com/sandhya/app/web/css/admin.css">
<link rel="stylesheet" href="css/live.css" />
<link rel="stylesheet" href="css/ripple.css" />
<link rel="stylesheet" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"  />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script src="js/live.js" type="text/javascript"></script>
<style>
	
</style>
<div id="no-loader">
	<div id="header">
        <div id="logo">
            <a href="http://innathecinema.com/sandhya" rel="nofollow">Sandhya Cine House</a>
            <span>v1.0</span>
        </div>
    </div>
    <div style="width:100%; text-align:center; font-size:30px; padding:10px">
    	<h2 style="font-weight: bold">Collection - Report</h2>
    </div>
    <div class='container'>
        <div class="row">
            <div class="col-md-12">
                <div class="row" style="margin-top:5px">
                    <div class="col-md-7">
                    	<div class="col-md-6">
                        	<div class="col-md-3">
                                <label  class="lbl">From</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group date" data-provide="datepicker">
                                  <input type="text" class="form-control" id="frm_date">
                                  <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                  </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        	<div class="col-md-3">
                                <label  class="lbl">To</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group date" data-provide="datepicker">
                                  <input type="text" class="form-control" id="to_date">
                                  <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="col-md-4">
                            <label class="lbl">Select Movie</label>
                        </div>
                        <div class="col-md-8">
                            <select name="movie_sel" id="select_movie" class="form-control">&nbsp;&nbsp;
                                <option value="0">--Select Movie--</option>
                                <?php
                                    foreach($movies as $movie){
                                        if(!empty($movie))
                                        echo '<option value="'.$movie['id'].'">'.$movie['content'].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="col-md-12" style="text-align:center">
            	<input type="button" id="go_btn" value="Go"  class="btn btn-primary" />
            </div>
        </div>
    </div>
    
    <div class="container map_container" style="display:block">
    	<div class="row">
        	<div class="col-sm-12" style="overflow-y: auto;">
            	<table id="collection_info" class="row-border">
                	<thead>
                    	<tr>
                        	<th class="text-center" width="25%">Movie</th>
                            <th class="text-center" width="18%">Show</th>
                            <th class="text-center" width="18%">Total Bookings</th>
                            <th class="text-center" width="18%">Total Seats</th>
                            <th class="text-center" width="18%">Total Amount</th>
                        </tr>
                    </thead>
                    <tbody id="table_body">
                    	
                    </tbody>
                    <tfoot id="table_foot" class="text-center">
                    	<tr id="table_foot_tr">
                        	
                        </tr>
                    </tfoot>
                </table> 
                <div style="width:100%;text-align:center">
                	<input type="button" id="print_btn" class="btn" value="Print" />  
                </div>  
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="s_i" value="<?php echo $_POST['session_id']; ?>"
<div class='uil-squares-css' id="loader-ring" style='transform:scale(0.5);'>
	<div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
<script>
	$('.datepicker').datepicker({
		format: 'mm/dd/yyyy',
		startDate: '-3d'
	})
</script>
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
<script>
  $(function(){
	$("#collection_info").dataTable();
  })
</script>