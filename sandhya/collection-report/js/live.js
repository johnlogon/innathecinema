$('.datepicker').datepicker({
  format: 'yyyy-mm-dd',
  startDate: '-3d'
})
$(document).ready(function(e) {	
	$('#go_btn').on('click', function(){
		if(validate()){
			getInfo()
		}
	});
	$('#print_btn').on('click', function(){
		if(validate()){
			getReport();
		}
	});
});
$(document).ready(function(e) {
	$(document).ajaxStart(function() {
		$('#loader-ring').css('display', 'block');
		$('#no-loader').css('pointer-events', 'none')
				 .css('opacity', '0.3');
    });
   	$(document).ajaxComplete(function(event, XMLHttpRequest, ajaxOptions) {
        $('#no-loader').css('pointer-events', 'auto')
				 		.css('opacity', '1');
		$('#loader-ring').css('display', 'none');
    });
});

function validate(){
	if($('#frm_date').val()=='' && $('#select_movie option:selected').val()=="0"){
		$('#frm_date').focus();
		return false;
	}
	return true;
}
function getInfo(){
	try{
		$('#table_body').html("");
		var from = $('#frm_date').val();
		var to = $('#to_date').val();
		var movie_id = $("#select_movie option:selected").val();
		if(movie_id=="0")
			movie_id="";
		var sess_id = $('#s_i').val(); 
		$.ajax({
			type: "POST",
			async: true,
			url: "http://innathecinema.com/sandhya/collection-report/xhr/xhr.php",
			data:{'action' : 'get-report', 'from':from, 'to':to, 'movie_id':movie_id, 'sid': sess_id},
			cache: false,
			beforeSend: function() {
				
			},
			success: function(data) {
				setTableData(data);
			},
			error:function(){
				console.log("Error");
			}
		});
	}
	catch(err){
		alert("Error");
	}
}
function setTableData(data){
	try{
		console.log(data);
		var obj = $.parseJSON(data);
		var table = $('#collection_info').DataTable();
		table.clear();
		var total_price = 0;
		var total_bookings = 0;
		var total_seats = 0;
		for(var i=0;obj[i]!=null; i++){
			var movie = obj[i];
			var shows = movie['shows'];
			if(!$.isEmptyObject(shows)){
				//table.row.add( [ "<b>"+movie['movie']+"</b>", " ", " ", " ", " " ] ).draw();
				for(var j=0;shows[j]!=null;j++){
					var show = shows[j];
					var price = parseInt(show['total_seats'])*parseInt(movie['price']);
					table.row.add( [ "<b>"+movie['movie']+"</b>", show['date_time'], show['total_bookings'], show['total_seats'], price ] ).draw();
					total_price += price;
					total_bookings+= parseInt(show['total_bookings']);
					total_seats+= parseInt(show['total_seats']);
				}
			}
		}
		var html = "<td></td><td><b>Total</b><td>"+total_bookings+"</td><td>"+total_seats+"</td><td>"+total_price+"</td>";
		$('#table_foot_tr').html(html);
	}
	catch(err){
		alert('Error');
	}
}

function getReport(){
	console.log("getReport");
	try{
		var from = $('#frm_date').val();
		var to = $('#to_date').val();
		var movie_id = $("#select_movie option:selected").val();
		if(movie_id=="0")
			movie_id="";
		var sess_id = $('#s_i').val(); 
		$.ajax({
			type: "POST",
			async: true,
			url: "http://innathecinema.com/sandhya/collection-report/xhr/xhr.php",
			data:{'action' : 'get-pdf', 'from':from, 'to':to, 'movie_id':movie_id, 'sid': sess_id},
			cache: false,
			beforeSend: function() {
				
			},
			success: function(data) {
				var win = window.open('http://'+data, '_blank');
				if (win) {
					win.focus();
				} else {
					alert('Please allow popups for this website');
				}
			},
			error:function(){
				alert("Error");
			}
		});
	}
	catch(err){
		alert("Error");
	}
}