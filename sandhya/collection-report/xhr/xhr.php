<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	if(!isset($_POST['sid']) || empty($_POST['sid'])){
		exit;
	}
	session_id($_POST['sid']);
	session_start();
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/loader.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/pdf/classes/util.php");
	require_once('../classes/main_class.php');
	if($_SERVER['REQUEST_METHOD']=='POST'){
		
		if(isset($_POST['from']) && !empty($_POST['from'])){
			$params['from'] = date("Y-m-d 00:00:00", strtotime($_POST['from']));
			if(isset($_POST['to']) && !empty($_POST['to'])){
				$params['to'] = date("Y-m-d 23:59:59", strtotime($_POST['to']));
			}
			else{
				$params['to'] = date("Y-m-d 23:59:59", strtotime($_POST['from']));
			}
		}
		$params['movie_id'] = $_POST['movie_id'];
		$action = $_POST['action'];
		$report = new CollectionReport;
		switch($action){
			case "get-report":
				$data = $report->getMovieCollectionByDateRange($params);
				echo json_encode($data);
				exit;
			break;
			case "get-pdf":
				$data = $report->generatePdf($params);
				echo $data;
				exit;
			break;
			default:
				
			break;
		}
		
	}
?>