<?php
	/*
		Author: Suhail
		Date  : 8-12-2016
		Note  : This file should be executed in every 15 minutes
	*/
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/report/classes/report.php");
	/*Getting todays show times*/
	$report = new Report;
	$booking_before = ($report->getBookingExpireTime()-20)*60;
	$todays_shows = $report->getShows();
	$settings = $report->getSettings();
	date_default_timezone_set('Asia/Kolkata');
	$time = time();
	$i=0;
	foreach($todays_shows as $show){
		$show_time = strtotime($show['date_time']);
		if(($show_time-$booking_before<=$time || $show['booking_disabled']==1) && $show['report_sent']==0){
			if(($settings['email_end_day']==1 || $settings['sms_end_day']==1) && $i==0){
				/*SEND SCREEN WISE REPORT*/
				$params['settings'] = $settings;
				$params['show'] = $show;
				$report->sendScreenWiseReport($params);
				break;
			}
			else if($settings['email_each_show']==1 || $settings['sms_each_show']==1){
				/*SEND SHOW WISE REPORT*/
				$shtme = date('Y-m-d H:i:s', strtotime($show['date_time']));
				$shtme = explode(" ", $shtme);
				if(in_array($shtme[1], $settings['db_show_times'])){
					$params['settings'] = $settings;
					$params['show'] = $show;
					$report->sendShowWiseReport($params);
				}
			}
		}
		$i++;
	}
?>