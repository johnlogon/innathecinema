<div id="leftmenu">
   <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   <div class="leftmenu-top"></div>
   <div class="leftmenu-middle">
      <ul class="menu">
         <li><a href="/sandhya/index.php?controller=pjAdmin&amp;action=pjActionIndex" class=""><span class="menu-dashboard">&nbsp;</span>Dashboard</a></li>
         <li><a href="/sandhya/index.php?controller=pjAdminSchedule&amp;action=pjActionIndex" class=""><span class="menu-schedule">&nbsp;</span>Now Showing</a></li>
         <li><a href="/sandhya/index.php?controller=pjAdminBookings&amp;action=pjActionIndex" class=""><span class="menu-bookings">&nbsp;</span>Bookings</a></li>
         <li><a href="/sandhya/index.php?controller=pjAdminEvents&amp;action=pjActionIndex" class=""><span class="menu-events">&nbsp;</span>Movies</a></li>
         <li><a href="/sandhya/index.php?controller=pjAdminVenues&amp;action=pjActionIndex" class=""><span class="menu-venues">&nbsp;</span>Cinema Halls</a></li>
         <li><a href="/sandhya/index.php?controller=pjAdminOptions&amp;action=pjActionIndex" class=""><span class="menu-options">&nbsp;</span>Options</a></li>
         <li><a href="/sandhya/index.php?controller=pjAdminUsers&amp;action=pjActionIndex" class=""><span class="menu-users">&nbsp;</span>Users</a></li>
         
         <li><a href="http://innathecinema.com/sandhya/live" class=""><i class="fa fa-television" aria-hidden="true" style="font-size: 25px; margin-right: 18px;font-weight: bold;margin-left: 11px;color: rgba(0, 0, 0, 0.71);"></i>&nbsp;Live monitor</a></li>
         <li><a href="http://innathecinema.com/sandhya/index.php?controller=pjAdmin&amp;action=pjActionReport" class="menu-focus"><i class="fa fa-cog" aria-hidden="true" style="font-size: 25px; margin-right: 18px;font-weight: bold;margin-left: 11px;color: rgba(0, 0, 0, 0.71);"></i>&nbsp;&nbsp; Report Settings</a></li>
         <li><a href="http://innathecinema.com/sandhya/index.php?controller=pjAdmin&amp;action=pjActionCollection" class="menu-focus"><i class="fa fa-file" aria-hidden="true" style="font-size: 25px; margin-right: 18px;font-weight: bold;margin-left: 11px;color: rgba(0, 0, 0, 0.71);"></i>&nbsp;&nbsp; Collection Report </a></li>
         <li><a href="/sandhya/index.php?controller=pjAdmin&amp;action=pjActionLogout"><span class="menu-logout">&nbsp;</span>Logout</a></li>
      </ul>
   </div>
   <div class="leftmenu-bottom"></div>
</div>