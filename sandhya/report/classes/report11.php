<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/sandhya/loader.php");
	require_once ($_SERVER['DOCUMENT_ROOT'].'/sandhya/report/mailer/class.phpmailer.php');
	require_once ($_SERVER['DOCUMENT_ROOT'].'/sandhya/report/mailer/class.smtp.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/sandhya/pdf/classes/util.php');
	
	class Report extends pjAdmin{
		public function logger($log){
			$logFile = fopen('pdf.txt', 'a');
			$this->setTimzone();
			fwrite($logFile, $log."  time:".date("d-m-Y h:i:s A", time())."\n");
			fclose($logFile);
		}
		private function connect(){
			$mysqli = new mysqli(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
			if ($mysqli->connect_error) {
				$this->logger("Connection failed: " . $mysqli->connect_error);
				return false;
			}
			return $mysqli;
		}
		private function setTimzone(){
			date_default_timezone_set('Asia/Kolkata');
		}
		public function updateSettings($params){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "UPDATE thcbs_report_settings SET email_each_show=?,
												 sms_each_show=?,
												 email_end_day=?,
												 sms_end_day=?,
												 email=?,
												 sms=?,
												 show_times=?,
												 ticket_price=? WHERE id=1";		
			$stmt = $mysqli->prepare($sql);
			echo $mysqli->error;
			$stmt->bind_param("iiiisssi", $params['email_each_show'],
							   $params['sms_each_show'],$params['email_end_day'],
							   $params['sms_end_day'],$params['email'],
							   $params['sms'], $params['show_times'],$params['ticket_price']);
			if($stmt->execute())
				return true;
			return false;
		}
		public function getSettings(){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT * FROM thcbs_report_settings";		
			$stmt = $mysqli->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($id, $email_each_show, $sms_each_show, $email_end_day,$sms_end_day, $email, $sms, $show_times, $ticket_price);
			$stmt->fetch();
			$data['id'] = $id;
			$data['email_each_show'] = $email_each_show;
			$data['sms_each_show'] = $sms_each_show;
			$data['email_end_day'] = $email_end_day;
			$data['sms_end_day'] = $sms_end_day;
			$data['email'] = $email;
			$data['sms'] = $sms;
			$data['ticket_price'] = $ticket_price;
			$data['db_show_times'] = explode("," , $show_times);;
			return $data;
		}
		
		public function getShows(){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$this->setTimzone();
			$date = date('Y-m-d');
			$date1 = $date." 00:00:00";
			$date2 = $date." 23:59:59";
			$sql = "SELECT 
					  * 
					FROM
					  thcbs_shows 
					WHERE date_time > '".$date1."' 
					  AND date_time < '".$date2."'
					ORDER BY date_time DESC";		
			$stmt = $mysqli->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($id,$event_id,$venue_id,$price_id,$price,$date_time,$booking_disabled,$booking_limit,$report_sent);
			$i=0;
			$show_times = array();
			while($stmt->fetch()){
				$shows[$i]['id'] = $id;
				$shows[$i]['event_id'] = $event_id;
				$shows[$i]['venue_id'] = $venue_id;
				$shows[$i]['price_id'] = $price_id;
				$shows[$i]['price'] = $price;
				$date_time = date('d-m-Y h:iA', strtotime($date_time));
				$shows[$i]['date_time']=$date_time;
				$dt = explode(" ", $date_time);
				$shows[$i]['date'] = $dt[0];
				$shows[$i]['time'] = $dt[1];
				$shows[$i]['booking_disabled'] = $booking_disabled;
				$shows[$i]['booking_limit'] = $booking_limit;
				$shows[$i++]['report_sent'] = $report_sent;
			}
			return $shows;
		}
		public function getDistinctShowTimes(){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT DISTINCT CAST(date_time AS TIME) AS show_time FROM thcbs_shows ORDER BY show_time";		
			$stmt = $mysqli->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($show);
			$show_times = array();
			while($stmt->fetch()){
				$show_times[] = $show;
			}
			return $show_times;
		}
		public function getBookingExpireTime(){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT 
					  thcbs_options.`value` AS booking_earlier 
					FROM
					  thcbs_options 
					WHERE thcbs_options.`key` = 'o_booking_earlier' 
					  AND thcbs_options.`foreign_id` = 1 ;";		
			$stmt = $mysqli->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($booking_earlier);
			$stmt->fetch();
			return $booking_earlier;
		}
		public function sendShowWiseReport($params){
			$settings = $params['settings'];
			$show = $params['show'];
			$report_params['show'] = $show;
			$report_params['date'] = $show['date'];
			$report_params['show_time'] = $show['time'];
			$report_params['report_num'] = $show['id'];
			$report_params['show_info'] = $this->getShowParams($show['id']);
			$report_params['movie'] = $this->getMovieInfo($show['id']);
			$report_params['venue'] = $this->getVenueInfo($show['venue_id']);
			$report_params['which'] = 'single';
			$report_params['settings'] = $settings;
			$pdf = new PDF;
			$pdf->sendShowReport($report_params);
		}
		public function sendScreenWiseReport($params){
			$venues = $this->getVenueIds();
			$report_params = array();
			$i=0;
			foreach($venues as $venue){
				$venue_info[$i]['venue_name'] = $this->getVenueInfo($venue);
				$show_params = array();
				$shows = $this->getShowsByVenueId($venue);
				foreach($shows as $show){
					$show_data = $this->getShowParams($show['id']);
					$show_data['movie'] = $this->getMovieInfo($show['id']);
					$time = explode(" ", $show['date_time']);
					$show_data['show_time'] = $time[1];
					$show_params[] = $show_data;
				}
				$venue_info[$i++]['venue_data'] = $show_params;
			}
			$this->setTimzone();
			$show = $params['show'];
			$report_params['show'] = $show;
			$report_params['venues'] = $venue_info;
			$report_params['date'] = date('d-m-Y');
			$report_params['which'] = 'all';
			$report_params['report_num'] = $show['id'];
			$settings = $params['settings'];
			$report_params['settings'] = $settings;
			$pdf = new PDF;
			$pdf->sendShowReport($report_params);
		}
		
		private function getShowParams($show_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT 
					  COUNT(
						thcbs_bookings_shows.`booking_id`
					  ) AS total_seats,
					  thcbs_bookings_shows.`price` AS price 
					FROM
					  thcbs_bookings_shows 
					  LEFT OUTER JOIN thcbs_bookings 
						ON thcbs_bookings.`id` = thcbs_bookings_shows.`booking_id` 
					WHERE show_id = ? 
					  AND thcbs_bookings.`status` = 'confirmed' ;
					
					";		
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $show_id);
			$stmt->execute();
			$stmt->bind_result($total_seats, $price);
			$stmt->fetch();
			$data['total_seats'] = $total_seats;
			$data['price'] = $price;
			return $data;
		}
		private function getMovieInfo($show_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT 
					  content 
					FROM
					  thcbs_multi_lang 
					WHERE foreign_id = 
					  (SELECT 
						event_id 
					  FROM
						thcbs_shows 
					  WHERE id = ?) 
					  AND model = 'pjEvent' 
					  AND locale = 1 
					  AND field= 'title'";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $show_id);
			$stmt->execute();
			$stmt->bind_result($movie_name);
			$stmt->fetch();
			return $movie_name;
		}
		private function getVenueInfo($venue_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$sql = "SELECT 
					  content 
					FROM
					  thcbs_multi_lang 
					WHERE foreign_id = ? 
					  AND model = 'pjVenue' 
					  AND locale = 1 
					  AND FIELD= 'name';";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $venue_id);
			$stmt->execute();
			$stmt->bind_result($venue);
			$stmt->fetch();
			return $venue;
		}
		private function getVenueIds(){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$this->setTimzone();
			$date = date('Y-m-d');
			$date1 = $date." 00:00:00";
			$date2 = $date." 23:59:59";
			$sql = "SELECT 
					  DISTINCT venue_id 
					FROM
					  thcbs_shows 
					WHERE date_time > '".$date1."' 
					  AND date_time < '".$date2."'
					ORDER BY date_time DESC";
			$stmt = $mysqli->prepare($sql);
			$stmt->execute();
			$stmt->bind_result($venue);
			$i=0;
			while($stmt->fetch()){
				$venues[$i++] = $venue;
			}
			return $venues;
		}
		private function getShowsByVenueId($venue_id){
			$mysqli = $this->connect();
			if($mysqli==false)
				return false;
			$this->setTimzone();
			$date = date('Y-m-d');
			$date1 = $date." 00:00:00";
			$date2 = $date." 23:59:59";
			$sql = "SELECT * FROM thcbs_shows WHERE venue_id=? AND date_time>'".$date1."' AND date_time<'".$date2."'";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $venue_id);
			$stmt->execute();
			$stmt->bind_result($id,$event_id,$venue_id,$price_id,$price,$date_time,$booking_disabled,$booking_limit,$report_sent);
			$show = array();
			$i=0;
			while($stmt->fetch()){
				$show[$i]['id'] = $id;
				$show[$i]['event_id'] = $event_id;
				$show[$i]['venue_id'] = $venue_id;
				$show[$i]['price_id'] = $price_id;
				$show[$i]['price'] = $price;
				$show[$i]['date_time'] = $date_time;
				$show[$i]['booking_disabled'] = $booking_disabled;
				$show[$i]['booking_limit'] = $booking_limit;
				$show[$i++]['report_sent'] = $report_sent;
			}
			return $show;
		}
	}
?>