<?php
	if(!isset($_POST['session_id']) || empty($_POST['session_id'])){
		echo "<h1>Forbidden</h1>
				<p>You don't have permission to access /report
				on this server.</p>
				<p>Additionally, a 404 Not Found
				error was encountered while trying to use an ErrorDocument to handle the request.</p>";
		die();
	}
	session_id($_POST['session_id']);
	session_start();
	require_once('classes/report.php');
	require_once('data/settingsData.php');
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/sandhya/app/web/css/reset.css">
<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/sandhya/core/third-party/jquery_ui/1.10.4/css/smoothness/jquery-ui.min.css">
<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/sandhya/core/framework/libs/pj/css/pj-all.css">
<link type="text/css" rel="stylesheet" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/sandhya/app/web/css/admin.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
	.heading{
		width: 40%;
	}
	.trheight{
		height:50px;
	}
	.email-text{
		height: 30px !important;
	}
	.form-control{
		width:60% !important;
	}
	input[type="checkbox"]{
	  width: 22px; /*Desired width*/
	  height: 22px; /*Desired height*/
	  cursor: pointer;
	 
	}
	.checkbox{
		margin-left:15px;
		display:inline-block !important;
	}
</style>
<div id="no-loader">
	<div id="container">
        <div id="header">
            <div id="logo">
                <a href="http://localhost/sandhya" rel="nofollow">Sandhya Cine House</a>
                <span>v1.0</span>
            </div>
        </div>
        <div id="middle">
        	<?php include_once('leftmenu.php'); ?>
            <div id="right">
            	<div class="content-top"></div>
                <div class="content-middle" id="content">
                	<form action="#" method="post" id="report-settings" class="form pj-form">
                        <table class="pj-table" cellpadding="0" cellspacing="0" style="width: 100%; ">
                            <tr>
                              <th>Option</th>
                              <th>Value</th>
                            </tr>
                            <tr class="trheight">
                              <td class="heading">Send Email report after each show</td>
                              <td>
                              	<select name="email_each_show" id="email_each_show" class="form-control">
                                	<?php $value = $data['email_each_show']; ?>
                                	<option value="1" <?php if ($value==1) echo "selected=selected"; ?>>Yes</option>
                                    <option value="0" <?php if ($value==0) echo "selected=selected"; ?>>No</option>
                                </select>
                               </td>
                            </tr>
                            <tr class="trheight">
                              <td class="heading">Send SMS report after each show</td>
                              <td>
                              	<select name="sms_each_show" id="sms_each_show" class="form-control">
                                	<?php $value = $data['sms_each_show']; ?>
                                	<option value="1" <?php if ($value==1) echo "selected=selected"; ?>>Yes</option>
                                    <option value="0" <?php if ($value==0) echo "selected=selected"; ?>>No</option>
                                </select>
                              </td>
                            </tr>
                            <tr class="trheight">
                              <td class="heading">Send Email report at end of the day</td>
                              <td>
                              	<select name="email_end_day" id="email_end_day" class="form-control">
                                	<?php $value = $data['email_end_day']; ?>
                                	<option value="1" <?php if ($value==1) echo "selected=selected"; ?>>Yes</option>
                                    <option value="0" <?php if ($value==0) echo "selected=selected"; ?>>No</option>
                                </select>
                              </td>
                            </tr>
                            <tr class="trheight">
                              <td class="heading">Send SMS report at end of the day</td>
                              <td>
                              	<select name="sms_end_day" id="sms_end_day" class="form-control">
                                	<?php $value = $data['sms_end_day']; ?>
                                	<option value="1" <?php if ($value==1) echo "selected=selected"; ?>>Yes</option>
                                    <option value="0" <?php if ($value==0) echo "selected=selected"; ?>>No</option>
                                </select>
                              </td>
                            </tr>
                            <tr class="trheight">
                              <td class="heading">Email to which report to be sent</td>
                              <td><input type="text" id="email" name="email" value="<?php echo $data['email'] ?>" class="pj-form-field w400 email-text">
                              	<br /><span style="font-size:12px; width:100%; font-weight:bold;">Separate multiple emails with coma (<b>,</b>)</span>
                              </td>
                            </tr>
                            <tr class="trheight">
                              <td class="heading">Phone no to which report sms to be sent</td>
                              <td><input type="text" id="sms" name="sms" value="<?php echo $data['sms'] ?>" class="pj-form-field w400 email-text">
                              	<br /><span style="font-size:12px; width:100%; font-weight:bold;">Separate multiple numbers with coma (<b>,</b>)</span>
                              </td>
                            </tr>
                            <tr class="trheight">
                              <td class="heading">Ticket Price</td>
                              <td>
                              	<input type="text" id="ticket_price" name="ticket_price" value="<?php echo $data['ticket_price'] ?>"
                                	 class="pj-form-field w400 email-text">
                              </td>
                            </tr>
                            <tr class="trheight">
                              <td class="heading">Shows of which report should be sent</td>
                              <td>
                              		<?php
										$i=0;
										foreach($show_times as $show_time){
											$checked="";
											if(in_array($show_time, $data['db_show_times']))
												$checked="checked=checked";
											echo '<div class="checkbox">
													  <label>
													  	<input type="checkbox" '.$checked.' name="'.$show_time.'" value="'.$show_time.'">'.$show_time.'
													  </label>
												  </div>';
										}
									?>
                                    <script>
                                    	$('.checkbox:last').css('pointer-events','none')
														   .css('opacity', '0.4');
                                    </script>
                              </td>
                            </tr>
                        </table>
                        <p><input type="submit" value="Save" class="pj-button" style="float:right"></p>
                     </form>
                </div>
                <div class="content-bottom"></div>
            </div>
        </div>
    </div>
    <script>
    	$(document).on('submit', '#report-settings', function(event){
			event.preventDefault();
			var show_times = "";
			$('.checkbox:last').find('input[type=checkbox]').attr('checked', 'checked');
			$("input[type=checkbox]").each(function(index, element) {
                if($(this).is(":checked"))
					show_times+=$(this).val()+",";
            });
			show_times = show_times.slice(0,-1);
			var session_id = <?php echo "'".$_POST['session_id']."'"; ?>;
			var data = {
							"email_each_show" : $('#email_each_show option:selected').val(),
							"sms_each_show" : $('#sms_each_show option:selected').val(),
							"email_end_day" : $('#email_end_day option:selected').val(),
							"sms_end_day" : $('#sms_end_day option:selected').val(),
							"email" : $('#email').val(),
							"sms" : $('#sms').val(),
							"show_times": show_times,
							"ticket_price": $('#ticket_price').val(),
							"session_id": session_id,
						};
						
			$.ajax({
				url: "http://innathecinema.com/sandhya/report/xhr/xhr.php",
				type: "POST",
				data: data,
				beforeSend: function(){
					$('.content-middle').css('pointer-events', 'none').css('opacity', '0.4');
				},
				success: function(data){
					//alert(data);
					location.reload();
				},
				error: function(xhr, status, error){
					alert(xhr.responseText);
				},
				complete: function(){
					$('.content-middle').css('pointer-events', 'auto').css('opacity', '1');
				}
			});
		});
    </script>
<?php
	
?>