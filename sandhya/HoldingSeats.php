<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	require_once('app/config/config.inc.php');
	if(isset($_POST['action'])){
		$myfile = fopen("logHoldingSeats.txt", "a");
		$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
		
		if(!$db){
			$txt =mysqli_error($db);
			fwrite($myfile, $txt."\n");

		}
		fwrite($myfile, "Request received, action:".$_POST['action']."\n");
		$event_id = $_POST['eventId'];
		switch($_POST['action']){
			case "set":
			
				$selected_seats = json_decode($_POST['dataAdd']);
				
				$removed_seats = json_decode($_POST['dataRemove']);
		
				$selected_date = $_POST['date'];
				$selected_time = $_POST['time'];
				
				$newDate = date("Y-m-d", strtotime($selected_date));
				$newTime = date("H:i", strtotime($selected_time));
				$date_time = $newDate." ".$newTime;
				
				fwrite($myfile, $date_time."\n");
				
				foreach($removed_seats as $seat){
					$sql="UPDATE thcbs_shows_seats SET holding=0, held_time='' WHERE seat_id=$seat AND
						 show_id=(SELECT id FROM thcbs_shows WHERE date_time='".$date_time."' AND event_id=".$event_id.")";
					fwrite($myfile, $sql."        line:33"."\n");	
					if(!mysqli_query($db, $sql)){
						$txt = mysqli_error($db);
						fwrite($myfile, $txt."\n");
					}
				}
				
				$sql="SELECT COUNT(id) AS tickets FROM thcbs_bookings_tickets WHERE booking_id IN(SELECT id FROM 
				thcbs_bookings WHERE event_id=".$event_id." AND date_time='".$date_time."' AND status='confirmed')";
				fwrite($myfile, $sql."            \n");
				$result=mysqli_query($db, $sql);
				
				$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
				$booked_num = $row['tickets'];
				
				/*
					NUMBER OF HOLDING SEATS
				*/
				
				$sql = "SELECT COUNT(*) AS holding FROM thcbs_shows_seats WHERE holding=1 AND  show_id=(SELECT id FROM thcbs_shows WHERE event_id=".$event_id." AND date_time='".$date_time."')";
				$result=mysqli_query($db, $sql);
				fwrite($myfile, $sql."            \n");
				if(!$result){
					
					$txt = mysqli_error($db);
					fwrite($myfile, $txt);
					fclose($myfile);
				}
				$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
				$seats_on_hold = $row['holding'];
				
				
				$sql = "SELECT booking_limit FROM thcbs_shows WHERE event_id=".$event_id." AND date_time='".$date_time."'";
				fwrite($myfile, $sql."            \n");
				
				$result=mysqli_query($db, $sql);
				if(!$result){
					
					$txt = mysqli_error($db);
					fwrite($myfile, $txt);
					fclose($myfile);
				}
				$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
				$booking_limit = $row['booking_limit'];
				
				$aval_sea = $booking_limit - ($booked_num+$seats_on_hold);
				if($aval_sea<=0 || count($selected_seats) > $aval_sea){
					echo "1100";
					die();
				}
				
				foreach($selected_seats as $seat){
					$sql="UPDATE thcbs_shows_seats SET holding=1, held_time='".time()."' WHERE seat_id=$seat AND
						 show_id=(SELECT id FROM thcbs_shows WHERE date_time='".$date_time."' AND event_id=".$event_id.")";
						 fwrite($myfile, $sql."        line:44"."\n");	
					if(!mysqli_query($db, $sql)){
						$txt = mysqli_error($db);
						fwrite($myfile, "sql_error: ".$txt."\n");
					}
				}
			
			break;
			
			case "unset":
				
				$selected_seats = json_decode($_POST['dataAdd']);
		
				$selected_date = $_POST['date'];
				$selected_time = $_POST['time'];
				$newTime = date("H:i", strtotime($selected_time));
				
				$newDate = date("Y-m-d", strtotime($selected_date));
				
				$date_time = $newDate." ".$newTime;
				
				foreach($selected_seats as $seat){
					$sql="UPDATE thcbs_shows_seats SET holding=0, held_time='' WHERE seat_id=$seat AND
						 show_id=(SELECT id FROM thcbs_shows WHERE date_time='".$date_time."'  AND event_id=".$event_id.")";
						 fwrite($myfile, $sql."        line:68"."\n");	
					if(!mysqli_query($db, $sql)){
						$txt = mysqli_error($db);
						fwrite($myfile, $txt."\n");
					}
				}
				
			break;
		}
		fclose($myfile);
	}
?>