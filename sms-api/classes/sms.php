<?php
	class SMSAPI{
		private function connect(){
			$conn = new mysqli(PJ_HOST,PJ_USER, PJ_PASS, PJ_DB);
			if ($conn->connect_errno) {
				$this->logger($conn->connect_error);
				return false;
			}
			return $conn;
		}
		public function logger($log){
			$file = $_SERVER['DOCUMENT_ROOT']."/sms-api/log.txt";
			$logFile = fopen($file, 'a');
			$time = date('d-m-Y h:i:s A', time());
			$time= $this->getTimeStamp();
			$log.="       time:".$time."\n";
			fwrite($logFile, $log);
			fclose($logFile);
		}
		private function setTimeZone(){
			date_default_timezone_set('Asia/Kolkata');
		}
		private function getTimeStamp(){
			$this->setTimeZone();
			$time = time();
			return $time;
		}
		public function getTicketInfo($uuid){
			$mysqli = $this->connect();
			$sql = "SELECT DISTINCT 
					  t1.id,
					  t1.uuid,
					  t1.event_id,
					  t1.date_time,
					  t1.total,
					  t1.status,
					  t1.txn_id,
					  t1.created,
					  t1.c_name,
					  t1.c_phone,
					  t1.c_email,
					  t2.content 
					FROM
					  thcbs_bookings t1 
					  LEFT OUTER JOIN thcbs_multi_lang t2 
						ON t1.`event_id` = t2.`foreign_id` 
					WHERE t2.`model` = 'pjEvent' 
					  AND t2.`field` = 'title' 
					  AND t1.uuid = ?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('s', $uuid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				return false;
			}
			$stmt->bind_result($id, $uuid, $event_id, $date_time, $total, $status, $txn_id, $created, $c_name,  $c_phone, $c_email, $movie_name);
			$stmt->store_result();
			if($stmt->num_rows<=0)
				return false;
			$data = array();
			$stmt->fetch();
			$data['id'] = $id;
			$data['uuid'] = $uuid;
			$data['event_id'] = $event_id;
			$data['date_time'] = date('d-m-Y h:i A', strtotime($date_time));
			$data['total'] = $total;
			$data['status'] = $status;
			$data['txn_id'] = $txn_id;
			$data['created'] = $created;
			$data['c_name'] = $c_name;
			$data['c_phone'] = $c_phone;
			$data['c_email'] = $c_email;
			$data['movie_name'] = $movie_name;
			$show_id = $this->getShowId($id);
			$data['show_id'] = $show_id;
			$data['venue'] = $this->getVenueInfo($show_id);
			$data['seats'] = $this->getSeats($id);
			return $data;
		}
		private function getShowId($bkid){
			$mysqli = $this->connect();
			$sql = "SELECT DISTINCT t.show_id FROM thcbs_bookings_shows t WHERE t.`booking_id`=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				return false;
			}
			$stmt->bind_result($show_id);
			$stmt->fetch();
			return $show_id;
		}
		private function getVenueInfo($show_id){
			$mysqli = $this->connect();
			$sql = "SELECT 
					  t.content 
					FROM
					  thcbs_multi_lang t 
					WHERE t.`foreign_id` = 
					  (SELECT 
						venue_id 
					  FROM
						thcbs_shows 
					  WHERE id = ?) 
					  AND t.`model` = 'pjVenue' 
					  AND t.`field` = 'name'";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $show_id);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				return false;
			}
			$stmt->bind_result($venue);
			$stmt->fetch();
			return $venue;
		}
		private function getSeats($bkid){
			$sql = "SELECT 
					  t.name 
					FROM
					  thcbs_seats t 
					WHERE id IN 
					  (SELECT 
						seat_id 
					  FROM
						thcbs_bookings_shows 
					  WHERE booking_id =?)";
			$mysqli = $this->connect();
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				return false;
			}
			$stmt->bind_result($name);
			$data = array();
			while($stmt->fetch()){
				$data[] = $name;
			}
			return $data;
		}
		public function sendSms($params){
			$http = new pjHttp();
			$url = "http://alerts.ebensms.com/api/v3/";
			$params = http_build_query(array(
				'method' =>'sms',
				'message' => $params['message'],
				'sender' => 'INCNMA',
				'to' =>$params['number'],
				'api_key' => 'A35e447822b5a27a172224ae820e51572' 
			));
			return $http->request($url ."?". $params)->getResponse();
		}
		public function sendMail($params){
			$invoice_params = $this->getTicketInfo($_POST['uuid']);
			if(empty($invoice_params))
				return false;
			$message = "<tr><td><b><h3>Your booking has been received. </b></h3></td></tr>
						<tr><td>Personal details:</td></tr>
						<tr><td>Name:</td><td>".$invoice_params['c_name']."</td></tr>
						<tr><td>E-Mail:</td><td>".$params['email']."</td></tr>
						<tr><td>Phone:</td><td>".$invoice_params['c_phone']."</td></tr>
						<tr><td><b><h3>Booking details:</b></h3></td></tr>
						<tr><td>Movie:</td><td>".$invoice_params['movie_name']."</td></tr>
						<tr><td>Venue:</td><td>".$invoice_params['venue']."</td></tr>
						<tr><td>Showtime:</td><td>".$invoice_params['date_time']."</td></tr>
						<tr><td>Booking ID:</td><td>".$invoice_params['uuid']."</td></tr>
						<tr><td>Booking Seats:</td><td>".implode(',', $invoice_params['seats'])."</td></tr>
						<tr><td>Please reach before 15 minutes. Thank you</td></tr>";
		
		
		
			$to      = $params['email'];
			$subject = 'Ticket Confirmation';
			$headers = 'From: Sandhya Cine House <admin@sandhyacinehouse.com>' . PHP_EOL .
				'Reply-To: Sandhya Cine House <admin@sandhyacinehouse.com>' . PHP_EOL .
				'Content-type:text/html;charset=UTF-8'. PHP_EOL .
				'X-Mailer: PHP/' . phpversion();
		
			$mail = mail($to, $subject, $message, $headers);
			return true;
		}
	}
?>