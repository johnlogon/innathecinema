<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	$password = "snogol_#4321";
	require_once($_SERVER['DOCUMENT_ROOT']."/pay/auto_load.php");
	require_once($_SERVER['DOCUMENT_ROOT']."/sms-api/classes/sms.php");
	
	if($_SERVER['REQUEST_METHOD']=='POST'){
		if(!isset($_POST['password']) || $_POST['password']=='' || $_POST['password']==NULL){
			echo 401;
			die();
		}
		if($_POST['password']!=$password){
			echo 401;
			die();
		}
		if(!isset($_POST['action']) || $_POST['action']=='' || $_POST['action']==NULL){
			echo 403;
			die();
		}
		if($_POST['action']=='ticket-info'){
			if($_POST['uuid']==''){
				echo 402;
				die();
			}
			$sms = new SMSAPI;
			$data = $sms->getTicketInfo($_POST['uuid']);
			if(!is_array($data) && $data==false){
				echo 405;
				die();
			}
			echo json_encode($data);
		}
		else if($_POST['action']=='send-msg'){
			$sms = new SMSAPI;
			$data = $sms->sendSms($_POST);
			if(isset($_POST['email'])){
				$sms->sendMail($_POST);
			}
			echo $data;
		}
	}
?>