<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
	require_once($_SERVER['DOCUMENT_ROOT']."/pay/auto_load.php");
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<style>
	td{
		padding-bottom: 13px;
	}
	#message{
		height: 100px;
    	resize: none;
	}
	.st_message{
		width:100%;
		font-size:15px;
	}
	.error{
		color:red;
	}
	.success{
		color:green;
	}
</style>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<div class="col-md-12" style="text-align:center;padding-left: 50px; padding-right: 50px;">
	<h1>SMS</h1>
    <div class="form-container">
    	<form action="#" id="postForm">
            <table width="100%">
            	<tr style="display:none" id="st_tr">
                	<td colspan="2">
                    	<div class="st_message"></div>
                    </td>
                </tr>
            	<tr>
                    <td><input type="checkbox" checked="checked" id="checkbox" />&nbsp;For innathecinema</td>
                    <td><input type="checkbox" checked="checked" id="checkbox_email" />&nbsp;Send Email</td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="password" class="form-control" required="required" id="password" /></td>
                </tr>
                <tr class="hide_tr">
                    <td>Transaction ID</td>
                    <td><input type="text" class="form-control" id="transaction_id"></td>
                </tr>
                <tr class="hide_tr">
                    <td>Name</td>
                    <td><input type="text" class="form-control" id="name" /></td>
                </tr>
                <tr class="hide_tr">
                    <td>Movie</td>
                    <td><input type="text" class="form-control" id="movie" /></td>
                </tr>
                <tr class="hide_tr">
                    <td>Venue</td>
                    <td><input type="text" class="form-control" id="venue" /></td>
                </tr>
                <tr class="hide_tr">
                    <td>Show time</td>
                    <td><input type="text" class="form-control" id="show_time" /></td>
                </tr>
                <tr class="hide_tr">
                    <td>Seats</td>
                    <td><input type="text" class="form-control" id="seats" /></td>
                </tr>
                <tr>
                    <td>Phone number</td>
                    <td><input type="text" class="form-control" required="required" id="phone_number" pattern="[1-9]{1}[0-9]{9}" maxlength="10" min="10"/></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="email" class="form-control" required="required" id="email"/></td>
                </tr>
                <tr>
                    <td>Message Body</td>
                    <td><textarea class="form-control" id="message" required="required"></textarea></td>
                </tr>
                <tr>
                	<td></td>
                    <td><input type="submit" id="submitBtn" class="btn btn-primary" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script>
	var baseUrl = "http://innathecinema.com/";
	$(document).ready(function(e) {
        $('#transaction_id').on('blur', function(e){
			var password = $('#password').val();
			var t_id = $('#transaction_id').val();
			if(t_id=='' || password=='')
				return false;
			getTicketInfo(t_id, password);
		});
		$('#postForm').on('submit', function(e){
			e.preventDefault();
			var message = $('#message').val();
			sendMessage(message);
		});
		$('#checkbox').change(function() {
			if (!this.checked) {
				$('.hide_tr').hide();
				$('#transaction_id').removeAttr('required');
       		}
			else{
				$('.hide_tr').show();
				$("#transaction_id").prop('required',true);
			}
		});
    });
	function getTicketInfo(uuid, password){
		$.ajax({
			url: baseUrl + "sms-api/xhr.php",
			type: "POST",
			data: {'action':'ticket-info','uuid':uuid, 'password': password},			
			beforeSend: function(){
				$('.form-container').css('opacity', '0.5').css('pointer-events', 'none');
			},
			success: function(data){
				//console.log(data);
				if(data==401 || data=='401'){
					alert("Incorrect Password");
					$('#password').focus();
					return false;
				}
				else if(data==403 || data==402 || data=='403' || data=='402'){
					alert("Required data not found in request");
					return false;				
				}
				else if(data==405 || data=='405'){
					alert("Invalid transaction ID");
					$('td').find('input').not('#password, #transaction_id, #submitBtn').val('');
					$('#message').val('');
					return false;
				}
				var obj = $.parseJSON(data);
				$('#name').val(obj.c_name);
				$('#movie').val(obj.movie_name);
				$('#venue').val(obj.venue);
				$('#show_time').val(obj.date_time);
				$('#seats').val(obj.seats.join(","));
				$('#phone_number').val(obj.c_phone);
				$('#email').val(obj.c_email);
				var msg = "Name: "+obj.c_name+", Movie: "+obj.movie_name+", Venue: "+obj.venue+", Show: "+obj.date_time+", ID: "+obj.uuid+", Seats: "+$('#seats').val()+" . Pls reach before 15 min";
				$('#message').val(msg);
			},
			error: function(xhr, status, error){
				alert(xhr.responseText);
			},
			complete: function(){
				$('.form-container').css('opacity', '1').css('pointer-events', 'auto');
			}
		});
	}
	function sendMessage(message){
		var data = {};
		if($('#checkbox_email').is(':checked')){
			data = {'action':'send-msg','uuid':$('#transaction_id').val(), 'email': $('#email').val(), 'message':message, 'password': $('#password').val(), 'number':$('#phone_number').val()};
		}
		else{
			data = {'action':'send-msg','message':message, 'password': $('#password').val(), 'number':$('#phone_number').val()};
		}
		$.ajax({
			url: baseUrl + "sms-api/xhr.php",
			type: "POST",
			data: data,			
			beforeSend: function(){
				$('.form-container').css('opacity', '0.5').css('pointer-events', 'none');
			},
			success: function(data){
				console.log(data);
				if(data==401 || data=='401'){
					alert("Incorrect Password");
					$('#password').focus();
					return false;
				}
				else if(data==403 || data==402 || data=='403' || data=='402'){
					alert("Required data not found in request");
					return false;				
				}
				alert(data);
			},
			error: function(xhr, status, error){
				alert(xhr.responseText);
			},
			complete: function(){
				$('.form-container').css('opacity', '1').css('pointer-events', 'auto');
			}
		});
	}
</script>