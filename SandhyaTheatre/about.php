<!doctype html>
<html>
<head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>Sandhya</title>
        <meta name="description" content="A Template by Gozha.net">
        <meta name="keywords" content="HTML, CSS, JavaScript">
        <meta name="author" content="Gozha.net">
    
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
    
    <!-- Fonts -->
        <!-- Font awesome - icon font -->
         <link href="css/font-awesome.css" rel="stylesheet">
        <!-- Roboto -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    
    <!-- Stylesheets -->
        <!-- jQuery UI -->
        <link href="../code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet">

        <!-- Mobile menu -->
        <link href="css/gozha-nav.css" rel="stylesheet" />
        <!-- Select -->
        <link href="css/external/jquery.selectbox.css" rel="stylesheet" />
        <!-- Swiper slider -->
        <link href="css/external/idangerous.swiper.css" rel="stylesheet" /> 
    
        <!-- Custom -->
        <link href="css/style3860.css?v=1" rel="stylesheet" />

        <!-- Modernizr --> 
        <script src="js/external/modernizr.custom.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
</head>

<body>
    <div class="wrapper">
        <!-- Banner -->
        <div class="banner-top">
            <img alt='top banner' src="images/banners/bra.jpg">
        </div>

        <!-- Header section -->
        <header class="header-wrapper header-wrapper--home">
            <div class="container">
                <!-- Logo link-->
                <a href='index.php' class="logo">
                    <img alt='logo' src="images/logo.png">
                </a>
                
                <!-- Main website navigation-->
                <nav id="navigation-box">
                    <!-- Toggle for mobile menu mode -->
                    <a href="#" id="navigation-toggle">
                        <span class="menu-icon">
                            <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                              <span class="lines"></span>
                            </span>
                        </span>
                    </a>
                    
                    <!-- Link navigation -->
                    <ul id="navigation">
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="index.php">HOME</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="about.php">ABOUT</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="#">UPCOMING MOVIES</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="movie-page-full.php">NOW RUNNING</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="contact.php">CONTACT US</a>                            
                        </li>                        
                    </ul>
                </nav>
                
                <!-- Additional header buttons / Auth and direct link to booking-->
                <div class="control-panel">
                   
                    <a href="http://www.innathecinema.com/ticket/" class="btn btn-md btn--warning btn--book">Book a ticket</a>
                </div>

            </div>
        </header>
		        <section class="cinema-container">
            <div class="cinema cinema--full">                
                <div class="cinema__gallery">
                    <div class="swiper-container">
                      <div class="swiper-wrapper">
                          <!--First Slide-->
                          <div class="swiper-slide"> 
                                <img alt='' src="images/cinema/cinema-inner1.jpg">
                          </div>
                          
                          <!--Second Slide-->
                          <div class="swiper-slide">
                                <img alt='' src="images/cinema/cinema-inner2.jpg">
                          </div>
                          
                          <!--Third Slide-->
                          <div class="swiper-slide"> 
                                <img alt='' src="images/cinema/cinema-inner3.jpg">
                          </div>

                          <!--Four Slide-->
                          <div class="swiper-slide"> 
                                <img alt='' src="images/cinema/cinema-inner4.jpg">
                          </div>
                          
                          <!--Five Slide-->
                          <div class="swiper-slide">
                                <img alt='' src="images/cinema/cinema-inner5.jpg">
                          </div>
                          
                          <!--Six Slide-->
                          <div class="swiper-slide"> 
                                <img alt='' src="images/cinema/cinema-inner6.jpg">
                          </div>

                          <!--Seven Slide-->
                          <div class="swiper-slide"> 
                                <img alt='' src="images/cinema/cinema-inner7.jpg">
                          </div>
                          
                          <!--Eight Slide-->
                          <div class="swiper-slide">
                                <img alt='' src="images/cinema/cinema-inner8.jpg">
                          </div>
                          
                          <!--Nine Slide-->
                          <div class="swiper-slide"> 
                                <img alt='' src="images/cinema/cinema-inner9.jpg">
                          </div>
                       
                      </div>
                    </div>
                </div>
                
            </div>
        </section>
        <section class="container">         
  
            
            <!-- Progress bar -->
            
              
            <!-- Promo boxes -->
            <div class="content-wrapper" style="margin-top:5px;">               
                
                <div class="col-sm-3">
                  <div class="promo promo-field">                     
                      <div class="promo__content">
                          <ul>
                              <li class="store-variant"><a href="#"><img alt='' src="images/4k.jpg"></a></li>
                              <li class="store-variant"><a href="#"><img alt='' src="images/3d.jpg"></a></li>
                              <li class="store-variant"><a href="#"><img alt='' src="images/qube.jpg"></a></li>
							  <li class="store-variant"><a href="#"><img alt='' src="images/atmos.jpg"></a></li>
                          </ul>
                      </div>
                  </div>
                </div>
                <div class="col-sm-9">
                  <div class="promo promo--short">
                      <img class="promo__images" alt='' src="images/tickets.png">
                      <div class="promo__head">SANDHYA CINE HOUSE</div>
					  
					  <div class="promo__content">

<h3 style="text-align: left;">ABOUT US</h3>
<p style="text-align: left;line-height: 21px;">Founded by Dr. VK Bhaskaran, Sandhya Cine House is a major landmark at its locality in Calicut. The Theatre has more than 30 years of 
							experience in its field since its inception in 1984. With Jeevaraj VK and Yadunath VK at the helm providing the beacon of development, 
							we have evolved to a 2 screen Multiplex incorporating features like 4k Projection, 3D and Dolby Atmos sound system which is the first in Malabar.
</p>
<h3 style="text-align: left;">OUR VISION</h3>
<p style="text-align: left;line-height: 21px;">To bring the latest and greatest in cinema experience by employing the state of the art technology in visual and audio systems within the reach of every moviegoer.
</p>
<h3 style="text-align: left;">OTHER FACILITIES</h3>
<p style="text-align: left;line-height: 21px;">Snack bar serving delectable bites, Online booking, Ample parking space.
</p>
					  </div>
					  
                  </div>                 
                </div>
            </div>

            <div class="devider-wrapper">
                <div class="devider"></div>
            </div>

            <!-- Tabs -->
              <!-- testimonials -->
              <div class="content-wrapper" >
                 
				  <div class="col-sm-6 col-md-4">
                      <div class="testionaial testionaial--corner">
                          <div class="testionaial__images">
                            <img alt='' src="images/client-photo/bhaskaran.jpg">
                          </div>
                          <p class="testionaial__author">Founder</p>                       

                          <div class="testionaial__text"><b>Late. Dr. V K Bhaskaran </b></div>
                      </div>
                    
                  </div>
                  <div class="col-sm-6 col-md-4">
                      <div class="testionaial testionaial--corner">
                          <div class="testionaial__images">
                            <img alt='' src="images/client-photo/yadu.jpg">
                          </div>
                          <p class="testionaial__author">Managing Director</p>                        

                          <div class="testionaial__text"><b>Yadunath V K</b></div>
                      </div>
                    
                  </div>
                  <div class="col-sm-6 col-md-4">
                      <div class="testionaial testionaial--corner">
                          <div class="testionaial__images">
                            <img alt='' src="images/client-photo/jeevan.jpg">
                          </div>
                          <p class="testionaial__author">Managing Partner & Chief Sound Engineer</p> 					  

                          <div class="testionaial__text"><b>Jeevaraj V K</b></div>
                      </div>
                    
                  </div>

                  <!--<div class="col-sm-6 col-md-4">
                    <div class="testionaial testionaial--rect">
                        <div class="testimonial-inner">
                          <div class="testionaial__text">Quisque condimentum ante at est aliquam rutrum. In egestas eu arcu non tincidunt. Vivamus pellentesque orci vel libero rutrum feugiat. In hac habitasse platea dictumst. Etiam tincidunt fermentum nibh, posuere consectetur nisi semper in. </div>
                         
                        </div>
                    </div>
                    
                  </div>-->

              </div>

              <div class="devider-wrapper">
                  <div class="devider"></div>
              </div>
        </section>

        <div class="clearfix"></div>

       <footer class="footer-wrapper">
            <section class="container">
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="index.php" class="nav-link__item">HOME</a></li>
                        <li><a href="about.php" class="nav-link__item">ABOUT US</a></li>  
                        
                        <li><a href="movie-page-full.php" class="nav-link__item">NOW RUNNING</a></li>                   
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="#" class="nav-link__item">UPCOMING MOVIES</a></li>     
                        <li><a href="contact.php" class="nav-link__item">CONTACT US</a></li>               
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">                   
                         <li><a href="privacy.php" class="nav-link__item">Terms & Condition</a></li>  
                         <li><a href="privacy.php" class="nav-link__item">Privacy Policy</a></li>   
                          <li><a href="privacy.php#cancel_policy" class="nav-link__item">Refund and Cancellation Policy</a></li>                     
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">SANDHYA CINE HOUSE<br><span class="title-edition">in the social media</span></p>

                        <div class="social">
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-facebook"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-twitter"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-vk"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-instagram"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-tumblr"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-pinterest"></a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <p class="copy">&copy; Sandhya, 2016. All rights reserved. Powered by Snogol</p>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <div class="overlay overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close">Close</button>
                    <form id="login-form" class="login" method='get' novalidate=''>
                        <p class="login__title">Sandhya Cine House<br><span class="login-edition">Booking will be available in few days</span></p>
                        
                    </form>
                </div>

            </section>
        </div>

	<!-- JavaScript-->
        <!-- jQuery 1.9.1--> 
        <script src="../ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
        <!-- Migrate --> 
        <script src="js/external/jquery-migrate-1.2.1.min.js"></script>
        <!-- jQuery UI -->
        <script src="../code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <!-- Bootstrap 3--> 
        <script src="../netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>

        <!-- Mobile menu -->
        <script src="js/jquery.mobile.menu.js"></script>
         <!-- Select -->
        <script src="js/external/jquery.selectbox-0.2.min.js"></script>
        <!-- Swiper slider -->
        <script src="js/external/idangerous.swiper.min.js"></script>

        <!-- Share buttons -->
        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
        <script type="text/javascript" src="../s7.addthis.com/js/300/addthis_widget.js#pubid=ra-525fd5e9061e7ef0"></script>

        <!--*** Google map  ***-->
        <script src="https://maps.google.com/maps/api/js?sensor=true"></script> 
        <!--*** Google map infobox  ***-->
        <script src="js/external/infobox.js"></script>

        <!-- Form element -->
        <script src="js/external/form-element.js"></script>
        <!-- Form validation -->
        <script src="js/form.js"></script>

        <!-- Custom -->
        <script src="js/custom.js"></script> 
		
		<script type="text/javascript">
            $(document).ready(function() {
                init_Cinema();
            });
		</script>


</body>

</html>