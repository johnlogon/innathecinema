<!doctype html>
<html>
<head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>Sandhya</title>
        <meta name="description" content="Online Ticket Booking">
        <meta name="keywords" content="cinema,online,booking,ticket,shows">
        <meta name="author" content="InnatheCinema">
    
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
    
    <!-- Fonts -->
        <!-- Font awesome - icon font -->
        <link href="css/font-awesome.css" rel="stylesheet">
        <!-- Roboto -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700' rel='stylesheet' type='text/css'>
        <!-- Open Sans -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800italic' rel='stylesheet' type='text/css'>
    
    <!-- Stylesheets -->

        <!-- Mobile menu -->
        <link href="css/gozha-nav.css" rel="stylesheet" />
        <!-- Select -->
        <link href="css/external/jquery.selectbox.css" rel="stylesheet" />

        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    
        <!-- Custom -->
        <link href="css/style3860.css?v=1" rel="stylesheet" />


        <!-- Modernizr --> 
        <script src="js/external/modernizr.custom.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
</head>

<body>
    <div class="wrapper">
        <!-- Banner -->
        <div class="banner-top">
            <img alt='top banner' src="images/banners/bra.jpg">
        </div>

        <!-- Header section -->
        <header class="header-wrapper header-wrapper--home">
            <div class="container">
                <!-- Logo link-->
                <a href='index.php' class="logo">
                    <img alt='logo' src="images/logo.png">
                </a>
                
                <!-- Main website navigation-->
                <nav id="navigation-box">
                    <!-- Toggle for mobile menu mode -->
                    <a href="#" id="navigation-toggle">
                        <span class="menu-icon">
                            <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                              <span class="lines"></span>
                            </span>
                        </span>
                    </a>
                    
                    <!-- Link navigation -->
                    <ul id="navigation">
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="index.php">HOME</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="about.php">ABOUT</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="#">UPCOMING MOVIES</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="movie-page-full.php">NOW RUNNING</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="contact.php">CONTACT US</a>                            
                        </li>                        
                    </ul>
                </nav>
                
                <!-- Additional header buttons / Auth and direct link to booking-->
                <div class="control-panel">
                   
                     <a href="http://www.innathecinema.com/ticket/" class="btn btn-md btn--warning btn--book">Book a ticket</a>
                </div>

            </div>
        </header>

     <!--  
        <div class="search-wrapper">
            <div class="container container--add">
                <form id='search-form' method='get' class="search">
                    <input type="text" class="search__field" placeholder="Search">
                    <select name="sorting_item" id="search-sort" class="search__sort" tabindex="0">
                        <option value="1" selected='selected'>By title</option>
                        <option value="2">By year</option>
                        <option value="3">By producer</option>
                        <option value="4">By title</option>
                        <option value="5">By year</option>
                    </select>
                    <button type='submit' class="btn btn-md btn--danger search__button">search a movie</button>
                </form>
            </div>
        </div>-->
       <br><br> 
        <!-- Main content -->
        <section class="container">            

            <div class="typography-wrap">

           <!-- <h3>Login / Registration :</h3>

            <p>If you are a new customer you have to click on the Register option on the Homepage or during the booking flow and register yourself by filling the Personal Details. On registering you will receive confirmation message on the screen and you will also receive an instant email confirming your registration. You are now a registered user of TicketNew.After registration, you can go ahead book tickets to your favourite Movies, Plays, Sports and events anytime. Already registered user can directly login and book the movie tickets as required.</p>
-->
            <h3>Where can the customers look for information regarding the movies?</h3>

            <p>The customer can go to <a href="http://innathecinema.com" target="_blank">innathecinema.com</a> or <a href="http://sandhyacinehouse.com" target="_blank">sandhyacinehouse.com</a> for the movie release date, advance booking, movies shown around the clock and view the information. The customer can also book the tickets online securely using all type of debit and credit cards around the clock throughout the year. You can pick the ticket(s) in the theatre premises showing the confirmation sms / print out of online booking.</p>
            

            <!--<h3>Offers :</h3>

            <p>We do have lots of offers running from various banks and credit cards. Check the offer related pages or at the payment page to know more on the offers. Do not forget to check the terms and conditions of the offer before you complete the transaction.</p>
            <h3>Ticket Booking :</h3>-->

            <p>You can book your tickets by selecting the movie, show date, show timings, the class, the seat no, no of tickets to be booked. You can also book movie tickets for your family or friends. </p>
            <h3>Payment :</h3>
			<p>You can book your favourite movie tickets by using all type of Debit, Credit and Cash cards through <a target="_blank" href="https://payumoney.com">PayUMoney</a> payment gateway.</p>
			<!--<h3>Order History</h3>
			<p>After completing the ticket booking check the status of the order in this page here...... If you did not get the confirmation page or an SMS or Email confirming the booking, check the order status page to know the status of the order, in case you do not find the order details of your booking, We apologize for the inconvenience caused while booking the movie tickets. If you have completed the payment process and the order is not reflecting in the Order Status We will not charge for the transaction. In case the amount is charged, refund would happen in 3-10 business days for the full amount. We are sorry for the inconvenience. Please try booking again. For any support related to a failed transaction email us at support@ticketnew.com with email address, transaction date and theatre name. You can also reach us at 044 – 42454444 /45 for assistance.</p>-->
			<h3>Email/SMS</h3>
			<p>On booking your movie ticket successfully an sms or email confirmation will be sent automatically , the customer can always collect the ticket showing the confirmation sms / print out of online movie bookings at the counter. You are also required to carry the Credit/Debit/Cash card used for booking the tickets. In case if your booking movie tickets for your family or friends then the photocopy of the card used for booking the ticket has to be shown along with print out for collecting the ticket at the counter. This is to ensure that the person who has booked the ticket is the bonafide owner of the card.</p>
			<h3>How and when can I pickup my tickets once I've made a purchase on InnatheCinema?</h3>
			<p>We do not do home delivery of movie tickets, select events might have an option for home delivery, check the respective events for more details. You may pick up your tickets at the theatre, venue you've purchased tickets for only on the show date prior to the show timings. For collecting the tickets it is required that you produce the purchasing credit card, photo id along with the confirmation number provided.</p>
			<h3>Refund Process :</h3>
			<p>The right to cancellation of movie or postponing the movie will occur very rarely and rests with the theatre management. The customers will be informed in advance and the amount will be automatically refunded to their debit or credit or cash card bank account within 7 or 10 business working days. The theatre won’t be liable for travel or any other expense incurred by the customer. In case if the amount got debited from your credit or debit or cash card but tickets are booked /confirmed then the amount will be automatically refunded to your bank account within 7 to 10 business working days.</p>
			<!--<h3>What is the policy for obtaining a refund for cancelled/postponed events?</h3>
			<p>If an event is ever cancelled or postponed by a promoter, team, band or venue associated with TicketNew, we will not be able to inform you or refund or exchange procedures for that event. In order to receive a refund or an exchange that may be offered, you will have to comply with the promoter's, team's or venue's instructions or deadlines, which, along with the decision about whether or not to issue a refund or an exchange, may be at the promoter's, team's or venue's discretion. If TicketNew issues you a refund for a ticket, it will issue a refund of the ticket's face value paid only and not the facility fee and the convenience charge. In no event will delivery prices or any other fees or amounts be refund. If a refund is issued, it will be issued using the same method of payment that was used to purchase the tickets. If a credit card was used to make the refunded purchase, then only that actual credit card will receive the credit for the refund. TicketNew will not be liable for travel or any other expenses that you or anyone else incurs in connection with a cancelled or postponed event. If the event was moved or rescheduled, the venue or promoter may set refund limitations. If your event has not been cancelled or postponed refunds are not allowed. Policies set forth by our clients, including venues, teams and theatres prohibit TicketNew.com from issuing exchanges or refunds after a ticket has been purchased or for lost, stolen, damaged, or destroyed tickets.</p>-->
			<a name="cancel_policy"><h3>Ticket Cancellation/Change of show timings, date, seat :</h3>
			<p>As per Government law tickets once sold cannot be cancelled or replaced. Also change of show timing or date or seat is not possible once the ticket is booked. Policies set forth by our clients, including venues, event organisers and theatres prohibit innathecinema.com from issuing exchanges or refunds after a ticket has been purchased or for lost, stolen, damaged, or destroyed tickets.</p></a>
			<h3>Age limit for children requiring a ticket:</h3>
			<p>Children aged 3 years and above will require a separate ticket. For Adult movie below age group of 18 will not be allowed.</p>
			<!--<h3>How many tickets can I book in one transaction?</h3>
			<p>You are allowed to book maximum of 10 tickets per transaction. For Bulk booking you can reach us on 044 – 42454444/45.</p>-->
			<!--<h3>Why does TicketNew enforce a time limit when making purchases online?</h3>
			<p>When you are shopping for tickets, you are "holding" real inventory. No one else can purchase those tickets unless you release them by not completing your order. Due to high demand, we've implemented a time limit during the checkout process. Each checkout page is assigned a different time limit based on the type of information we need from you. If you exceed the posted time on the page, the tickets you are holding are released for others to purchase. You have about five minutes total to complete your purchase. This gives as many people as possible a chance to purchase tickets.</p>-->
			<h3>What if I received an error when trying to buy tickets?</h3>
			<p>Sorry for the inconvenience you have encountered with our online service. Before re-attempting your purchase, we recommend that you check your order status. If an order was placed, the order status will appear on the page just after you sign in. If no order appears in your Order History, we recommend that you then re-attempt your purchase. If you have completed the payment process and the order is not reflecting in the Order Status We will not charge for the transaction. In case the amount is charged, refund would happen in 3-10 business days for the full amount. We are sorry for the inconvenience. Please try booking again. For any support related to a failed transaction email us at service@innathecinema.com with email address, transaction date and show details.</p>
			<h3>How do I know that my transaction is secure and that my personal information is protected?</h3>
			<p>InnatheCinema ensures a safe and secure customer buying experience. Our service is equipped with the latest computer technology making sure of transaction security. All transactions conducted on our website is through a secure browser protected by the industry standard and the best software available today for secure online commerce business.</p>
			<h3>For Help or Assistance :</h3>
			<p>You can always reach us on : 0495-4044435 OR Email us at service@innathecinema.com</p>
            </div> 
        </section>

       <footer class="footer-wrapper">
            <section class="container">
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="index.php" class="nav-link__item">HOME</a></li>
                        <li><a href="about.php" class="nav-link__item">ABOUT US</a></li>  
                        
                        <li><a href="movie-page-full.php" class="nav-link__item">NOW RUNNING</a></li>                   
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="#" class="nav-link__item">UPCOMING MOVIES</a></li>     
                        <li><a href="contact.php" class="nav-link__item">CONTACT US</a></li>               
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">                   
                         <li><a href="privacy.php" class="nav-link__item">Terms & Condition</a></li>  
                         <li><a href="privacy.php" class="nav-link__item">Privacy Policy</a></li>   
                          <li><a href="privacy.php#cancel_policy" class="nav-link__item">Refund and Cancellation Policy</a></li>                     
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">SANDHYA CINE HOUSE<br><span class="title-edition">in the social media</span></p>

                        <div class="social">
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-facebook"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-twitter"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-vk"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-instagram"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-tumblr"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-pinterest"></a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <p class="copy">&copy; Sandhya, 2016. All rights reserved. Powered by <a href="http://innathecinema.com" target="_blank">InnatheCinema</a></p>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <div class="overlay overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close">Close</button>
                    <form id="login-form" class="login" method='get' novalidate=''>
                        <p class="login__title">Sandhya Cine House<br><span class="login-edition">Booking will be available in few days</span></p>
                        
                    </form>
                </div>

            </section>
        </div>

	<!-- JavaScript-->
        <!-- jQuery 1.9.1--> 
        <script src="js/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
        <!-- Migrate --> 
        <script src="js/external/jquery-migrate-1.2.1.min.js"></script>
        <!-- Bootstrap 3--> 
        <script src="js/bootstrap.min.js"></script>

        <!-- jQuery REVOLUTION Slider -->
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Mobile menu -->
        <script src="js/jquery.mobile.menu.js"></script>
         <!-- Select -->
        <script src="js/external/jquery.selectbox-0.2.min.js"></script>
        <!-- Stars rate -->
        <script src="js/external/jquery.raty.js"></script>
        
        <!-- Form element -->
        <script src="js/external/form-element.js"></script>
        <!-- Form validation -->
        <script src="js/form.js"></script>

        <!-- Twitter feed -->
        <script src="js/external/twitterfeed.js"></script>

        <!-- Custom -->
        <script src="js/custom.js"></script>
		
	      <script type="text/javascript">
              $(document).ready(function() {
                init_Home();
              });
		    </script>

</body>

</html>