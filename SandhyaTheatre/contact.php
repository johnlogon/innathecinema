<!doctype html>
<html>
<head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>Sandhya</title>
        <meta name="description" content="Online Ticket Booking">
        <meta name="keywords" content="cinema,online,booking,ticket,shows">
        <meta name="author" content="InnatheCinema">
    
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
    
    <!-- Fonts -->
        <!-- Font awesome - icon font -->
        <link href="css/font-awesome.css" rel="stylesheet">
        <!-- Roboto -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700' rel='stylesheet' type='text/css'>
        <!-- Open Sans -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800italic' rel='stylesheet' type='text/css'>
    
    <!-- Stylesheets -->

        <!-- Mobile menu -->
        <link href="css/gozha-nav.css" rel="stylesheet" />
        <!-- Select -->
        <link href="css/external/jquery.selectbox.css" rel="stylesheet" />

        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    
        <!-- Custom -->
        <link href="css/style3860.css?v=1" rel="stylesheet" />


        <!-- Modernizr --> 
        <script src="js/external/modernizr.custom.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
</head>

<body>
    <div class="wrapper">
        <!-- Banner -->
        <div class="banner-top">
            <img alt='top banner' src="images/banners/bra.jpg">
        </div>

        <!-- Header section -->
        <header class="header-wrapper header-wrapper--home">
            <div class="container">
                <!-- Logo link-->
                <a href='index.php' class="logo">
                    <img alt='logo' src="images/logo.png">
                </a>
                
                <!-- Main website navigation-->
                <nav id="navigation-box">
                    <!-- Toggle for mobile menu mode -->
                    <a href="#" id="navigation-toggle">
                        <span class="menu-icon">
                            <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                              <span class="lines"></span>
                            </span>
                        </span>
                    </a>
                    
                    <!-- Link navigation -->
                    <ul id="navigation">
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="index.php">HOME</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="about.php">ABOUT</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="#">UPCOMING MOVIES</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="movie-page-full.php">NOW RUNNING</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="contact.php">CONTACT US</a>                            
                        </li>                        
                    </ul>
                </nav>
                
                <!-- Additional header buttons / Auth and direct link to booking-->
                <div class="control-panel">
                   
                    <a href="http://www.innathecinema.com/ticket/" class="btn btn-md btn--warning btn--book">Book a ticket</a>
                </div>

            </div>
        </header>
        
       
        
        <!-- Main content -->
        <section class="container">
            <h2 class="page-heading heading--outcontainer">Contact</h2>
            <div class="contact">
                <p class="contact__title">You have any questions or need help, <br><span class="contact__describe">don’t be shy and contact us</span></br> 
				<span class="contact__mail" style="margin-top:0px;">service@innathecinema.com</span></br> </p>
               InnatheCinema.com<br>

1st floor,mulliyathu building<br>

Near EPF Quarters<br>

Sasthrinagar Colony Road<br>

East Nadakkav<br>

Eranjipalam Post-673006<br>

Calicut - India<br>

Ph: +91 9562583247<br>

Tel:+91-495-4044435 <br>(9 AM to 5.30 PM, MONDAY - FRIDAY)<br><br>


               
            </div>
        </section>        

        <section class="container">            
               <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d125133.37924520351!2d75.75979284283359!3d11.44971807096548!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x3ba6641ff40d5ca1%3A0xe4c2913421e6ed76!2ssandhya+theatre+balussery!3m2!1d11.4497264!2d75.8298334!5e0!3m2!1sen!2sin!4v1476509406949" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            
        </section>

       
        
        <div class="clearfix"></div>

         <footer class="footer-wrapper">
            <section class="container">
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="index.php" class="nav-link__item">HOME</a></li>
                        <li><a href="about.php" class="nav-link__item">ABOUT US</a></li>  
                        
                        <li><a href="movie-page-full.php" class="nav-link__item">NOW RUNNING</a></li>                   
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="#" class="nav-link__item">UPCOMING MOVIES</a></li>     
                        <li><a href="contact.php" class="nav-link__item">CONTACT US</a></li>               
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">                   
                         <li><a href="privacy.php" class="nav-link__item">Terms & Condition</a></li>  
                         <li><a href="privacy.php" class="nav-link__item">Privacy Policy</a></li>   
                          <li><a href="privacy.php#cancel_policy" class="nav-link__item">Refund and Cancellation Policy</a></li>                     
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">SANDHYA CINE HOUSE<br><span class="title-edition">in the social media</span></p>

                        <div class="social">
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-facebook"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-twitter"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-vk"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-instagram"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-tumblr"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-pinterest"></a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <p class="copy">&copy; Sandhya, 2016. All rights reserved. Powered by <a href="http://innathecinema.com" target="_blank">InnatheCinema</a></p>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <div class="overlay overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close">Close</button>
                    <form id="login-form" class="login" method='get' novalidate=''>
                        <p class="login__title">Sandhya Cine House<br><span class="login-edition">Booking will be available in few days</span></p>
                        
                    </form>
                </div>

            </section>
        </div>

	<!-- JavaScript-->
        <!-- jQuery 1.9.1--> 
        <script src="js/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
        <!-- Migrate --> 
        <script src="js/external/jquery-migrate-1.2.1.min.js"></script>
        <!-- Bootstrap 3--> 
        <script src="js/bootstrap.min.js"></script>

        <!-- jQuery REVOLUTION Slider -->
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Mobile menu -->
        <script src="js/jquery.mobile.menu.js"></script>
         <!-- Select -->
        <script src="js/external/jquery.selectbox-0.2.min.js"></script>
        <!-- Stars rate -->
        <script src="js/external/jquery.raty.js"></script>
        
        <!-- Form element -->
        <script src="js/external/form-element.js"></script>
        <!-- Form validation -->
        <script src="js/form.js"></script>

        <!-- Twitter feed -->
        <script src="js/external/twitterfeed.js"></script>

        <!-- Custom -->
        <script src="js/custom.js"></script>
		
	      <script type="text/javascript">
              $(document).ready(function() {
                init_Home();
              });
		    </script>

</body>

</html>

