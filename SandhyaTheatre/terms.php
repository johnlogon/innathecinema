<!doctype html>
<html>
<head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>Sandhya</title>
        <meta name="description" content="A Template by Gozha.net">
        <meta name="keywords" content="HTML, CSS, JavaScript">
        <meta name="author" content="Gozha.net">
    
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
    
    <!-- Fonts -->
        <!-- Font awesome - icon font -->
        <link href="css/font-awesome.css" rel="stylesheet">
        <!-- Roboto -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700' rel='stylesheet' type='text/css'>
        <!-- Open Sans -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800italic' rel='stylesheet' type='text/css'>
    
    <!-- Stylesheets -->

        <!-- Mobile menu -->
        <link href="css/gozha-nav.css" rel="stylesheet" />
        <!-- Select -->
        <link href="css/external/jquery.selectbox.css" rel="stylesheet" />

        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    
        <!-- Custom -->
        <link href="css/style3860.css?v=1" rel="stylesheet" />


        <!-- Modernizr --> 
        <script src="js/external/modernizr.custom.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
</head>

<body>
    <div class="wrapper">
        <!-- Banner -->
        <div class="banner-top">
            <img alt='top banner' src="images/banners/bra.jpg">
        </div>

        <!-- Header section -->
        <header class="header-wrapper header-wrapper--home">
            <div class="container">
                <!-- Logo link-->
                <a href='index.php' class="logo">
                    <img alt='logo' src="images/logo.png">
                </a>
                
                <!-- Main website navigation-->
                <nav id="navigation-box">
                    <!-- Toggle for mobile menu mode -->
                    <a href="#" id="navigation-toggle">
                        <span class="menu-icon">
                            <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                              <span class="lines"></span>
                            </span>
                        </span>
                    </a>
                    
                    <!-- Link navigation -->
                    <ul id="navigation">
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="index.php">HOME</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="about.php">ABOUT</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="#">UPCOMING MOVIES</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="movie-page-full.php">NOW RUNNING</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="contact.php">CONTACT US</a>                            
                        </li>                        
                    </ul>
                </nav>
                
                <!-- Additional header buttons / Auth and direct link to booking-->
                <div class="control-panel">
                   
                     <a href="http://www.innathecinema.com/ticket/" class="btn btn-md btn--warning btn--book">Book a ticket</a>
                </div>

            </div>
        </header>

        <!-- Search bar -->
        <div class="search-wrapper">
            <div class="container container--add">
                <form id='search-form' method='get' class="search">
                    <input type="text" class="search__field" placeholder="Search">
                    <select name="sorting_item" id="search-sort" class="search__sort" tabindex="0">
                        <option value="1" selected='selected'>By title</option>
                        <option value="2">By year</option>
                        <option value="3">By producer</option>
                        <option value="4">By title</option>
                        <option value="5">By year</option>
                    </select>
                    <button type='submit' class="btn btn-md btn--danger search__button">search a movie</button>
                </form>
            </div>
        </div>
        
        <!-- Main content -->
        <section class="container">            

            <div class="typography-wrap">

            <h1>Terms and Conditions</h1>

            <p>The use of any product, service or feature (the "Materials") available through the internet web sites accessible at www.innathecinema.com (collectively, the "Web Site") by any user of the Web Site ("You" or "Your" hereafter) shall be governed by the following terms of use:

</p><p>This Web Site is provided by SNOGOL SOFTWARE SOLUTIONS Calicut. India and shall be used for informational purposes only. By using the Web Site or downloading Materials from the Web Site, You hereby agree to abide by the terms and conditions set forth in this Terms of Use. In the event of You not agreeing to these terms and conditions, You are requested by SNOGOL SOFTWARE SOLUTIONS Calicut not to use the Web Site or download Materials from the Web Site.

</p><p>By mere use of the Website, You shall be contracting with SNOGOL SOFTWARE SOLUTIONS Calicut and these terms and conditions including the policies constitute your binding obligations, with SNOGOL SOFTWARE SOLUTIONS Calicut. For the purpose of these Terms of Use, wherever the context so requires "You" or "User" shall mean any natural or legal person who has agreed to become a buyer on the Website by providing Registration Data while registering on the Website as Registered User using the computer systems.  SNOGOL SOFTWARE SOLUTIONS allows the User to surf the Website or making purchases without registering on the Website. The term "We", "Us", "Our" shall mean SNOGOL SOFTWARE SOLUTIONS When You use any of the services provided by Us through the Website, including but not limited to, (e.g. Product Reviews, Seller Reviews), You will be subject to the rules, guidelines, policies, terms, and conditions applicable to such service, and they shall be deemed to be incorporated into this Terms of Use and shall be considered as part and parcel of this Terms of Use. We reserve the right, at our sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time without any prior written notice to you. It is your responsibility to review these Terms of Use periodically for updates / changes. Your continued use of the Website following the posting of changes will mean that you accept and agree to the revisions. As long as you comply with these Terms of Use, We grant you a personal, non-exclusive, non-transferable, limited privilege to enter and use the Website.

</p><p>In no case will be a cancellation of a ticket allowed and in no case will be a refund provided on cancellation basis.</p>
            </div> 
        </section>

      <footer class="footer-wrapper">
            <section class="container">
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="index.php" class="nav-link__item">HOME</a></li>
                        <li><a href="about.php" class="nav-link__item">ABOUT US</a></li>  
                        
                        <li><a href="movie-page-full.php" class="nav-link__item">NOW RUNNING</a></li>                   
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="#" class="nav-link__item">UPCOMING MOVIES</a></li>     
                        <li><a href="contact.php" class="nav-link__item">CONTACT US</a></li>               
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">                   
                         <li><a href="privacy.php" class="nav-link__item">Terms & Condition</a></li>  
                         <li><a href="privacy.php" class="nav-link__item">Privacy Policy</a></li>   
                          <li><a href="privacy.php#cancel_policy" class="nav-link__item">Refund and Cancellation Policy</a></li>                     
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">SANDHYA CINE HOUSE<br><span class="title-edition">in the social media</span></p>

                        <div class="social">
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-facebook"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-twitter"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-vk"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-instagram"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-tumblr"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-pinterest"></a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <p class="copy">&copy; Sandhya, 2016. All rights reserved. Powered by Snogol</p>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <div class="overlay overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close">Close</button>
                    <form id="login-form" class="login" method='get' novalidate=''>
                        <p class="login__title">Sandhya Cine House<br><span class="login-edition">Booking will be available in few days</span></p>
                        
                    </form>
                </div>

            </section>
        </div>

	<!-- JavaScript-->
        <!-- jQuery 1.9.1--> 
        <script src="js/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
        <!-- Migrate --> 
        <script src="js/external/jquery-migrate-1.2.1.min.js"></script>
        <!-- Bootstrap 3--> 
        <script src="js/bootstrap.min.js"></script>

        <!-- jQuery REVOLUTION Slider -->
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Mobile menu -->
        <script src="js/jquery.mobile.menu.js"></script>
         <!-- Select -->
        <script src="js/external/jquery.selectbox-0.2.min.js"></script>
        <!-- Stars rate -->
        <script src="js/external/jquery.raty.js"></script>
        
        <!-- Form element -->
        <script src="js/external/form-element.js"></script>
        <!-- Form validation -->
        <script src="js/form.js"></script>

        <!-- Twitter feed -->
        <script src="js/external/twitterfeed.js"></script>

        <!-- Custom -->
        <script src="js/custom.js"></script>
		
	      <script type="text/javascript">
              $(document).ready(function() {
                init_Home();
              });
		    </script>

</body>

</html>