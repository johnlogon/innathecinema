<!doctype html>

<html>

<head>

	<!-- Basic Page Needs -->

        <meta charset="utf-8">

        <title>Sandhya Cine House</title>

       <meta name="description" content="Online Ticket Booking">
        <meta name="keywords" content="cinema,online,booking,ticket,shows">
        <meta name="author" content="InnatheCinema">

    

    <!-- Mobile Specific Metas-->

    	<meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta content="telephone=no" name="format-detection">

		<link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />

    <!-- Fonts -->

        <!-- Font awesome - icon font -->

        <link href="css/font-awesome.css" rel="stylesheet">

        <!-- Roboto -->

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700' rel='stylesheet' type='text/css'>

        <!-- Open Sans -->

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800italic' rel='stylesheet' type='text/css'>

    

    <!-- Stylesheets -->



        <!-- Mobile menu -->

        <link href="css/gozha-nav.css" rel="stylesheet" />

        <!-- Select -->

        <link href="css/external/jquery.selectbox.css" rel="stylesheet" />



        <!-- REVOLUTION BANNER CSS SETTINGS -->

        <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

    

        <!-- Custom -->

        <link href="css/style3860.css?v=1" rel="stylesheet" />





        <!-- Modernizr --> 

        <script src="js/external/modernizr.custom.js"></script>

    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 

    <!--[if lt IE 9]> 

    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 

		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		

    <![endif]-->

</head>



<body>

    <div class="wrapper">

        <!-- Banner -->

        <div class="banner-top">

            <img alt='top banner' src="images/banners/bra.jpg">

        </div>



        <!-- Header section -->

        <header class="header-wrapper header-wrapper--home">

            <div class="container">

                <!-- Logo link-->

                <a href='index.php' class="logo">

                    <img alt='logo' src="images/logo.png">

                </a>

                

                <!-- Main website navigation-->

                <nav id="navigation-box">

                    <!-- Toggle for mobile menu mode -->

                    <a href="#" id="navigation-toggle">

                        <span class="menu-icon">

                            <span class="icon-toggle" role="button" aria-label="Toggle Navigation">

                              <span class="lines"></span>

                            </span>

                        </span>

                    </a>

                    

                    <!-- Link navigation -->

                    <ul id="navigation">

                        <li>

                            <span class="sub-nav-toggle plus"></span>

                            <a href="index.php">HOME</a>                            

                        </li>

                        <li>

                            <span class="sub-nav-toggle plus"></span>

                            <a href="about.php">ABOUT</a>                            

                        </li>

                        <li>

                            <span class="sub-nav-toggle plus"></span>

                            <a href="#">UPCOMING MOVIES</a>                            

                        </li>

                        <li>

                            <span class="sub-nav-toggle plus"></span>

                            <a href="movie-page-full.php">NOW RUNNING</a>                            

                        </li>

                        <li>

                            <span class="sub-nav-toggle plus"></span>

                            <a href="contact.php">CONTACT US</a>                            

                        </li>                        

                    </ul>

                </nav>

                

                <!-- Additional header buttons / Auth and direct link to booking-->

                <div class="control-panel">

                   

                    <a href="http://innathecinema.com/ticket" class="btn btn-md btn--warning btn--book ">Book a ticket</a>

                </div>



            </div>

        </header>



        <!-- Search bar -->        

        

        <!-- Main content -->

        <section class="container" style="margin-top:30px;">

         <?php

					$db = mysqli_connect("208.91.198.74", "snogop2y_innathe", "WX0lr&sVgAUx", "snogop2y_sandhya");

		$query = mysqli_query($db,"SELECT * FROM movies  where status='Active' ORDER BY movie_index");

		

		$num_rows = mysqli_num_rows($query );

                     while ($rows = mysqli_fetch_array($query,MYSQLI_ASSOC)):

					 $name=$rows['name'];

					   $image=$rows['icon_url'];
			
					   $description=$rows['description'];
			
					   $event_id=$rows['event_id'];

		   

		   ?>

		   <a name="<?php echo $rows['id']?>">

             <div class="col-sm-12">

                <div class="movie">

                    <h2 class="page-heading"><?php echo $name?></h2>

                    

                    <div class="movie__info">

                        <div class="col-sm-4 col-md-3 movie-mobile">

                            <div class="movie__images">                               

                                <img alt='' src="http://sandhyacinehouse.com/<?php echo $image?>">

                            </div>                            

                        </div>

                        <div class="col-sm-8 col-md-9">

                            <p class="movie__time" style="margin-bottom:0px;">SHOW TIME
                            	<?php
									date_default_timezone_set('Asia/Kolkata');
									$today = date("Y-m-d 00:00:00");
									$dateE = "2017-04-28 00:00:00";
									if($event_id==24 && $today<$dateE){
										echo "<b>(From 28th onwards)</b>";
									}
								?>
                            </p>

							<div class="time-select">

								<div class="time-select__group group--first">                            

									<ul class="col-sm-8 items-wrap" style="padding-top:0px;padding-bottom:0px;">

                                    <?php 
									
									$d = date('Y-m-d', time());
									
									$sql = "SELECT DISTINCT venue FROM show_times WHERE event_id=$event_id AND DATE='$d' ORDER BY TIME ASC";
									$result = mysqli_query($db, $sql);
									$venues = array();
									while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
										$venues[]=$row['venue'];
									}
									 if($event_id==13 && $today < $dateE){
										echo "<b>Screen 1</b>:&nbsp;&nbsp;";
										echo '<a href="http://innathecinema.com/ticket/#!/Events/from_date:08-02-2017/date:10-02-2017">
												<li class="time-select__item" >
													11:00 AM
                                                </li>
												<li class="time-select__item" >
													02:30 PM
                                                </li>
												<li class="time-select__item" >
													6:00 PM
                                                </li>
												<li class="time-select__item" >
													9:00 PM
                                                </li>
                                              </a>';
									}
									$num_result=0;
									foreach($venues as $venue){
									
										$timeQuery = mysqli_query($db,"SELECT * FROM show_times WHERE event_id=$event_id AND date='".$d."' AND venue='$venue'  ORDER BY TIME ASC");
										$prev_value ='';
										$times = 0;
											echo "<b>".$venue."</b>:&nbsp;&nbsp;";
										$num_result = mysqli_num_rows($timeQuery);
										if($num_result>0){
											while ($timeRows = mysqli_fetch_array($timeQuery,MYSQLI_ASSOC)):
											
												$time=$timeRows['time'];
			
												$date=new DateTime($time);
			
												?>
			
												<a href="http://innathecinema.com/ticket"><li class="time-select__item" ><?php echo $date->format('h:i a')?></li></a>
														
												<?php
											endwhile;
										}
											echo "<br>";
											//For Bahubali; Screen 2 show times
											/*if($event_id==24){
												echo "<a name='57'><b>Screen 2</b>:&nbsp;&nbsp;</a>";
												echo '<a name="57" href="#"><li class="time-select__item" >11:00 am</li></a>';
											}*/
									}
									if($num_result==0){
										$sql = "SELECT DISTINCT t.time FROM show_times t WHERE t.`event_id`=".$event_id." AND date=(SELECT MAX(date) FROM show_times WHERE event_id=".$event_id.") ORDER BY t.time ASC";
										$re = mysqli_query($db, $sql);
										if($event_id==12 || $event_id==13 || $event_id==14)
											echo "<b>Screen 2</b>:&nbsp;&nbsp;";
										if($event_id==17)
											echo "<b>Screen 1</b>:&nbsp;&nbsp;";
										while($row = mysqli_fetch_array($re,MYSQLI_ASSOC)){
											$date=new DateTime($row['time']);
											?>
                                            <?php
													if($event_id==24 && $today<=$dateE){
														 echo "<a href='http://www.innathecinema.com/ticket/index.php#!/Events/from_date:28-04-2017/date:28-04-2017'>";
													}
													else{
												?>
												<a href="http://innathecinema.com/ticket">
                                                <?php } ?>
												
                                                	<li class="time-select__item" >
														<?php echo $date->format('h:i a')?>
                                                    </li>
                                                </a>
											<?php
										}
									}

									?>

									</ul>

								</div>

							</div>

                            <p class="movie__option"><?php echo $description?></p>                                                       

                        </div>

                    </div>

                    

                    <div class="clearfix"></div> 

                </div>

            </div></a>

            <?php

                     endwhile;

			?>

        

           <!-- <div class="col-sm-12">

                <div class="movie">

                    <h2 class="page-heading">PULIMURUGAN</h2>

                    

                    <div class="movie__info">

                        <div class="col-sm-4 col-md-3 movie-mobile">

                            <div class="movie__images">                               

                                <img alt='' src="images/movie/screen1.jpg">

                            </div>                            

                        </div>

                        <div class="col-sm-8 col-md-9">

                            <p class="movie__time" style="margin-bottom:0px;">SHOW TIME</p>

							<div class="time-select">

								<div class="time-select__group group--first">                            

									<ul class="col-sm-8 items-wrap" style="padding-top:0px;padding-bottom:0px;">

										<li class="time-select__item" data-time='10:30 AM'>10:30 AM</li>

										<li class="time-select__item" data-time='1:30 PM'>&nbsp;&nbsp;1:30 PM</li>

										<li class="time-select__item " data-time='4:30 PM'>&nbsp;&nbsp;4:30 PM</li>

										<li class="time-select__item" data-time='7:30 PM'>&nbsp;&nbsp;7:30 PM</li>

										<li class="time-select__item" data-time='10:30 PM'>10:30 PM</li>

									</ul>

								</div>

							</div>

                            <p class="movie__option">Pulimurugan (English: Leopard Murugan) is a 2016 Indian Malayalam-language action adventure film directed by Vysakh, starring Mohanlal in the titular role. It is the first independent screenplay written by Udayakrishna of the Udayakrishna-Siby K. Thomas duo. The film is produced by Tomichan Mulakupadam through the production house, Mulakupadam Films. The score and soundtrack are composed by Gopi Sunder, while cinematography is handled by Shaji Kumar.



Peter Hein handles the fight choreography. The film, which commenced production in July 2015 in Hanoi, Vietnam. The film was released on 7 October 2016</p>

                            <p class="movie__option"><strong>Initial release: </strong>October 7, 2016</p>

                            <p class="movie__option"><strong>Director: </strong>Vysakh</p>

                            <p class="movie__option"><strong>Budget:  </strong>250 million INR</p>

                            <p class="movie__option"><strong>Music director: </strong>Gopi Sunder</p>

                            <p class="movie__option"><strong>Language: </strong>Malayalam</p>                                                       

                        </div>

                    </div>

                    

                    <div class="clearfix"></div> 

                </div>

            </div>

			<div class="col-sm-12">

                <div class="movie">

                    <h2 class="page-heading">AANANDAM</h2>

                    

                    <div class="movie__info">

                        <div class="col-sm-4 col-md-3 movie-mobile">

                            <div class="movie__images">                               

                                <img alt='' src="images/movie/anandam-show.jpg">

                            </div>                            

                        </div>

                        <div class="col-sm-8 col-md-9">

                            <p class="movie__time" style="margin-bottom:0px;">SHOW TIME</p>

							<div class="time-select">

								<div class="time-select__group group--first">                            

									<ul class="col-sm-8 items-wrap" style="padding-top:0px;padding-bottom:0px;">

										<li class="time-select__item" data-time='10:30 AM'>10:30 AM</li>

										<li class="time-select__item" data-time='1:30 AM'>&nbsp;&nbsp;1:30 PM</li>

										<li class="time-select__item " data-time='4:30 AM'>&nbsp;&nbsp;4:30 PM</li>

									</ul>

								</div>

							</div>

                            <p class="movie__option">Aanandam is a 2016 Indian Malayalam campus musical film written and directed by Ganesh Raj in his directorial debut. Vineeth Sreenivasan produces the film under the banner of Habit Of Life with Vinod Shornur under Cast N Crew.</p>

                            <p class="movie__option"><strong>Initial release: </strong>October 21, 2016</p>

                            <p class="movie__option"><strong>Director: </strong>Ganesh Raj</p>

                            <p class="movie__option"><strong>Produced by: </strong>Vineeth Sreenivasan </p>

                            <p class="movie__option"><strong>Language: </strong>Malayalam</p>                            

                        </div>

                    </div>

                    

                    <div class="clearfix"></div> 

                </div>

            </div>

			

	        <div class="col-sm-12">

                <div class="movie">

                    <h2 class="page-heading">THOPPIL JOPPAN</h2>

                    

                    <div class="movie__info">

                        <div class="col-sm-4 col-md-3 movie-mobile">

                            <div class="movie__images">                               

                                <img alt='' src="images/movie/screen2.jpg">

                            </div>                            

                        </div>

                        <div class="col-sm-8 col-md-9">

                            <p class="movie__time" style="margin-bottom:0px;">SHOW TIME</p>

							<div class="time-select">

								<div class="time-select__group group--first">                            

									<ul class="col-sm-8 items-wrap" style="padding-top:0px;padding-bottom:0px;">

										<li class="time-select__item" data-time='7:30 PM'>&nbsp;&nbsp;7:30 PM</li>

									</ul>

								</div>

							</div>

                            <p class="movie__option">Thoppil Joppan is a 2016 Indian Malayalam-language comedy film directed by Johny Antony, starring Mammootty in the titular role.[1]



Principal photography began in April 2016 in Ernakulam. With music and background score composed by Vidyasagar, the film released on 7 October 2016</p>

                            <p class="movie__option"><strong>Initial release: </strong>October 7, 2016</p>

                            <p class="movie__option"><strong>Director: </strong>Johny Antony</p>

                            <p class="movie__option"><strong>Editor: </strong>Ranjan Abraham </p>

                            <p class="movie__option"><strong>Music director: </strong>Vidhya Sagar</p>

                            <p class="movie__option"><strong>Language: </strong>Malayalam</p>                            

                        </div>

                    </div>

                    

                    <div class="clearfix"></div> 

                </div>

            </div>-->



        </section>

        

        <div class="clearfix"></div>



        <footer class="footer-wrapper" style="margin-top:20px;">

            <section class="container">

                <div class="col-xs-4 col-md-2 footer-nav">

                    <ul class="nav-link">

                        <li><a href="index.php" class="nav-link__item">HOME</a></li>

                        <li><a href="about.php" class="nav-link__item">ABOUT US</a></li>                                           

                    </ul>

                </div>

                <div class="col-xs-4 col-md-2 footer-nav">

                    <ul class="nav-link">

                        <li><a href="#" class="nav-link__item">Coming soon</a></li>

                        <li><a href="movie-page-full.php" class="nav-link__item">Cinemas</a></li>

                        <li><a href="#" class="nav-link__item">Trailers</a></li>                      

                    </ul>

                </div>

                <div class="col-xs-4 col-md-2 footer-nav">

                    <ul class="nav-link">                        

                        <li><a href="#" class="nav-link__item">Gallery</a></li>

                        <li><a href="contact.php" class="nav-link__item">Contacts</a></li>                     

                    </ul>

                </div>

                <div class="col-xs-12 col-md-6">

                    <div class="footer-info">

                        <p class="heading-special--small">SANDHYA CINE HOUSE<br><span class="title-edition">in the social media</span></p>



                        <div class="social">

                            <a href='#' class="social__variant fa fa-facebook"></a>

                            <a href='#' class="social__variant fa fa-twitter"></a>

                            <a href='#' class="social__variant fa fa-vk"></a>

                            <a href='#' class="social__variant fa fa-instagram"></a>

                            <a href='#' class="social__variant fa fa-tumblr"></a>

                            <a href='#' class="social__variant fa fa-pinterest"></a>

                        </div>

                        

                        <div class="clearfix"></div>

                        <p class="copy">&copy; Sandhya, 2016. All rights reserved. Powered by <a href="http://innathecinema.com" target="_blank">InnatheCinema</a></p>

                    </div>

                </div>

            </section>

        </footer>

    </div>



    <div class="overlay overlay-hugeinc">

            

            <section class="container">



                <div class="col-sm-4 col-sm-offset-4">

                    <button type="button" class="overlay-close">Close</button>

                    <form id="login-form" class="login" method='get' novalidate=''>

                        <p class="login__title">Sandhya Cine House<br><span class="login-edition">Booking will be available in few days</span></p>

                        

                    </form>

                </div>



            </section>

        </div>



	<!-- JavaScript-->

        <!-- jQuery 1.9.1--> 

        <script src="js/jquery.min.js"></script>

        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>

        <!-- Migrate --> 

        <script src="js/external/jquery-migrate-1.2.1.min.js"></script>

        <!-- Bootstrap 3--> 

        <script src="js/bootstrap.min.js"></script>



        <!-- jQuery REVOLUTION Slider -->

        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>

        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>



        <!-- Mobile menu -->

        <script src="js/jquery.mobile.menu.js"></script>

         <!-- Select -->

        <script src="js/external/jquery.selectbox-0.2.min.js"></script>

        <!-- Stars rate -->

        <script src="js/external/jquery.raty.js"></script>

        

        <!-- Form element -->

        <script src="js/external/form-element.js"></script>

        <!-- Form validation -->

        <script src="js/form.js"></script>



        <!-- Twitter feed -->

        <script src="js/external/twitterfeed.js"></script>



        <!-- Custom -->

        <script src="js/custom.js"></script>

		

	      <script type="text/javascript">

              $(document).ready(function() {

                init_Home();

              });

		    </script>



</body>



</html>



