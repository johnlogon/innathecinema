<strong><!doctype html>
<html>

<head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>Sandhya Cine House</title>
        <meta name="description" content="Online Ticket Booking">
        <meta name="keywords" content="cinema,online,booking,ticket,shows">
        <meta name="author" content="InnatheCinema">
    
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
   
	<link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />
   
    <!-- Fonts -->
        <!-- Font awesome - icon font -->
        <link href="css/font-awesome.css" rel="stylesheet">
        <!-- Roboto -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700' rel='stylesheet' type='text/css'>
        <!-- Open Sans -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800italic' rel='stylesheet' type='text/css'>
    
    <!-- Stylesheets -->

        <!-- Mobile menu -->
        <link href="css/gozha-nav.css" rel="stylesheet" />
        <!-- Select -->
        <link href="css/external/jquery.selectbox.css" rel="stylesheet" />

        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    
        <!-- Custom -->
        <link href="css/style3860.css?v=1" rel="stylesheet" />


        <!-- Modernizr --> 
        <script src="js/external/modernizr.custom.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
</head>

<body>
    <div class="wrapper">
        <!-- Banner -->
        <div class="banner-top">
            <img alt='top banner' src="images/banners/bra.jpg">
        </div>

        <!-- Header section -->
        <header class="header-wrapper header-wrapper--home">
            <div class="container">
                <!-- Logo link-->
                <a href='index.html' class="logo">
                    <img alt='logo' src="images/logo.png">
                </a>
                
                <!-- Main website navigation-->
                <nav id="navigation-box">
                    <!-- Toggle for mobile menu mode -->
                    <a href="#" id="navigation-toggle">
                        <span class="menu-icon">
                            <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                              <span class="lines"></span>
                            </span>
                        </span>
                    </a>
                    
                    <!-- Link navigation -->
                    <ul id="navigation">
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="index.php">HOME</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="about.php">ABOUT</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="#">UPCOMING MOVIES</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="movie-page-full.php">NOW RUNNING</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="contact.php">CONTACT US</a>                            
                        </li>                        
                    </ul>
                </nav>
                
                <!-- Additional header buttons / Auth and direct link to booking-->
                <div class="control-panel">
                   
                    <a href="http://innathecinema.com/ticket" class="btn btn-md btn--warning btn--book">Book a ticket</a>
                </div>

            </div>
        </header>

        <!-- Slider -->
                        <div class="bannercontainer">
						
                    <div class="banner">
					
                        <ul>

                            <li data-transition="fade" data-slotamount="7" class="slide" data-slide='Rush.'>
                                <img alt='' src="images/slides/first-slide.jpg">
                                <div class="caption slide__name margin-slider" 
                                     data-x="right" 
                                     data-y="80" 

                                     data-splitin="chars"
                                     data-elementdelay="0.1"

                                     data-speed="700" 
                                     data-start="1400" 
                                     data-easing="easeOutBack"

                                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"

                                    data-frames="{ typ :lines;
                                                 elementdelay :0.1;
                                                 start:1650;
                                                 speed:500;
                                                 ease:Power3.easeOut;
                                                 animation:x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;
                                                 },
                                                 { typ :lines;
                                                 elementdelay :0.1;
                                                 start:2150;
                                                 speed:500;
                                                 ease:Power3.easeOut;
                                                 animation:x:0;y:0;z:0;rotationX:00;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:1;transformPerspective:600;transformOrigin:50% 50%;
                                                 }
                                                 "


                                    data-splitout="lines"
                                    data-endelementdelay="0.1"
                                    data-customout="x:-230;y:0;z:0;rotationX:0;rotationY:0;rotationZ:90;scaleX:0.2;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%"

                                    data-endspeed="500"
                                    data-end="2400"
                                    data-endeasing="Back.easeIn"
                                     >                                    
                                </div>
                            </li>



                            

                        </ul>
                    </div>
					<div style="margin-top:0px;"><img alt='' src="images/feat2.jpg"></div>
                </div>
				
        <!--end slider -->
        
        
        <!-- Main content -->
        <section class="container">
            <div class="movie-best">
                 <div class="col-sm-10 col-sm-offset-1 movie-best__rating">Today Best choice</div>
                 
				 <div class="col-sm-12 change--col">
				 
                 <?php
				 	require_once('db_connect.php');
					date_default_timezone_set('Asia/Kolkata');
					$d = date('Y-m-d', time());
					//$sql = "SELECT * FROM movies WHERE event_id in(SELECT event_id FROM show_times WHERE date>='".$d."')";
					$sql = "SELECT * FROM movies WHERE status='Active' AND event_id NOT IN(23) ORDER BY movie_index";
					
					$result = mysqli_query($db, $sql);
					
					if($result){
					 $rowcount1234=mysqli_num_rows($result); 
					}
					
					if(!$result){
						echo mysqli_error($db);
						die();
					}
					while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
						$sql = "SELECT venue,MIN(id) FROM show_times WHERE event_id=".$row['event_id'];
						$result1 = mysqli_query($db, $sql);
						if(!$result1)
							echo mysqli_error($db);
						$row2=mysqli_fetch_array($result1, MYSQLI_ASSOC);
						?><div class="movie-beta__item col-xs-<?php echo 12/$rowcount1234; ?>" style="padding:0;">
                        	<a href="movie-page-full.php#<?php echo $row['id'] ?>" >
                             <div class=" " style="">
                                <img alt='' src="http://sandhyacinehouse.com/<?php echo $row['icon_url']; ?>">
                                 <span class="best-rate" ><?php echo $row2['venue']; /*if($row['id']==45) echo "Screen 2"; else echo "Screen 1"*/  ?></span>                        		
								 <?php if($row['id']==57){
									?>
										<!--<span class="best-rate" style="left:70%">Screen 2</span>-->
									<?php
								  } ?>  
                             </div>
                           </a> </div>
                        <?php
					}
				 ?>
				   <!--<a href="movie-page-full.html#PULIMURUGAN">
                     <div class="movie-beta__item ">
                        <img alt='' src="images/movie/movie-sample1.jpg">
                         <span class="best-rate">SCREEN 1</span>                        
                     </div>
				   </a> 
				   
				   <a href="movie-page-full.html#AANANDAM">
                     <div class="movie-beta__item">
                         <img alt='' src="images/movie/anandam-front.jpg">
                         <span class="best-rate">SCREEN 2</span>                        
                     </div> 
				   </a>-->
				   
                 </div>
				 
                <div class="col-sm-10 col-sm-offset-1 movie-best__check">check all movies now playing</div>
            </div>    
        </section>
		<section class="container">
            <div class="col-sm-12">               
                <div class="trailer-wrapper">
                    <!-- Films trailers -->
                    <div class="trailer-block row" style="padding-bottom:0px"> 
                    <?php
						$sql = "SELECT * FROM movies WHERE status='Active' AND event_id NOT IN(23) ORDER BY movie_index";
						$result = mysqli_query($db, $sql);
						$rowcount=mysqli_num_rows($result); 
						if($rowcount>0){
						while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
							?>
                            	<div class="col-sm-6 col-md-<?php echo 12/$rowcount?>">
                                     <h2 class="page-heading" style="margin-top:0px;margin-bottom:10px;font: 15px 'aleobold', sans-serif !important;">
                                     	 <?php 
											if($row['event_id']==12)
												echo "Munthiri....";
											else if($row['event_id']==17)
												echo "Oru Mexican..";
											else
												echo $row['name']
										 ?> Trailer
                                     </h2>
                                    <iframe width="100%" height="315" src="<?php echo $row['trailer_url']; ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                            <?php
						}}
					?>
                        <!--<div class="col-sm-6 col-md-4">
							 <h2 class="page-heading" style="margin-top:0px;margin-bottom:10px">Pulimurugan Trailer</h2>
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/blQUlD8g4Pk" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <div class="col-sm-6 col-md-4">
							 <h2 class="page-heading" style="margin-top:0px;margin-bottom:10px">Aanandam Trailer</h2>
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/9B1SDMwQRXk" frameborder="0" allowfullscreen></iframe>
                        </div>-->                      
                    </div>
                </div>
            </div>
        </section>
        
        <div class="clearfix"></div>
		
        <footer class="footer-wrapper">
            <section class="container">
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="index.php" class="nav-link__item">HOME</a></li>
                        <li><a href="about.php" class="nav-link__item">ABOUT US</a></li>                     
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="movie-page-full.php" class="nav-link__item">NOW RUNNING</a></li>
                        <li><a href="#" class="nav-link__item">UPCOMING MOVIES</a></li>                  
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="contact.php" class="nav-link__item">CONTACT US</a></li>                     
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">SANDHYA CINE HOUSE<br><span class="title-edition">in the social media</span></p>

                        <div class="social">
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-facebook"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-twitter"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-vk"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-instagram"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-tumblr"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-pinterest"></a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <p class="copy">&copy; Sandhya, 2016. All rights reserved. Powered by <a href="http://innathecinema.com" target="_blank">InnatheCinema</a></p>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <!-- open/close -->
        <div class="overlay overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close">Close</button>
                    <form id="login-form" class="login" method='get' novalidate=''>
                        <p class="login__title">Sandhya Cine House<br><span class="login-edition">Booking will be available in few hours</span></p>
                        
                    </form>
                </div>

            </section>
        </div>

	<!-- JavaScript-->
        <!-- jQuery 1.9.1--> 
        <script src="js/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
        <!-- Migrate --> 
        <script src="js/external/jquery-migrate-1.2.1.min.js"></script>
        <!-- Bootstrap 3--> 
        <script src="js/bootstrap.min.js"></script>

        <!-- jQuery REVOLUTION Slider -->
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

        <!-- Mobile menu -->
        <script src="js/jquery.mobile.menu.js"></script>
         <!-- Select -->
        <script src="js/external/jquery.selectbox-0.2.min.js"></script>
        <!-- Stars rate -->
        <script src="js/external/jquery.raty.js"></script>
        
        <!-- Form element -->
        <script src="js/external/form-element.js"></script>
        <!-- Form validation -->
        <script src="js/form.js"></script>

        <!-- Twitter feed -->
        <script src="js/external/twitterfeed.js"></script>

        <!-- Custom -->
        <script src="js/custom.js"></script>
		
	      <script type="text/javascript">
              $(document).ready(function() {
                init_Home();
              });
		    </script>

</body>

</html>
</strong>