<!doctype html>
<html>

<head>
	<!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title>Innathe Cinema</title>
        <meta name="description" content="Innathe Cinema">
        <meta name="keywords" content="Online booking, Online Film Booking, Ticket Booking, Film Ticket Booking, Sandhya Cine House, Innathe Cinema">
        <meta name="author" content="http://innathecinema.com">
    
    <!-- Mobile Specific Metas-->
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="telephone=no" name="format-detection">
    
    <!-- Fonts -->
        <!-- Font awesome - icon font -->
        <link href="../SandhyaTheatre/css/font-awesome.css" rel="stylesheet">
        <!-- Roboto -->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,700' rel='stylesheet' type='text/css'>
        <!-- Open Sans -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:800italic' rel='stylesheet' type='text/css'>
    
    <!-- Stylesheets -->

        <!-- Mobile menu -->
        <link href="../SandhyaTheatre/css/gozha-nav.css" rel="stylesheet" />
        <!-- Select -->
        <link href="../SandhyaTheatre/css/external/jquery.selectbox.css" rel="stylesheet" />

        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="../SandhyaTheatre/rs-plugin/css/settings.css" media="screen" />
    
        <!-- Custom -->
        <link href="../SandhyaTheatre/css/style3860.css?v=1" rel="stylesheet" />
        
		<style>
		#timeout-wrapper{
			position: absolute;
			top: 20px;
			right: 15px;
			width: 133px;
			height: 54px;
			background: antiquewhite;
			margin-bottom: 5px;
			font-size: 20px;
			padding: 2px;
			display:none;
		}
		#loader-ring{
			position: fixed;
			top: 50%;
			left: 50%;
			width: 350px;
    		height: 154px;
			margin-top: -85px;
			margin-left: -167px;
		}
		#no-loader{
			opacity:0.5;
			pointer-events:none;
		}
		.hide-loader{
			width:0px;
			height:0px;
			overflow:hidden;
		}
	</style>


        <!-- Modernizr --> 
        <script src="../SandhyaTheatre/js/external/modernizr.custom.js"></script>
        
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --> 
    <!--[if lt IE 9]> 
    	<script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script> 
		<script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>		
    <![endif]-->
</head>

<body>
<div id="no-loader">
    <div class="wrapper">
        <!-- Banner -->
        <div class="banner-top">
            <img alt='top banner' src="../SandhyaTheatre/images/banners/bra.jpg">
        </div>

        <!-- Header section -->
        <header class="header-wrapper header-wrapper--home">
            <div class="container">
                <!-- Logo link-->
                <a href='http://sandhyacinehouse.com/' class="logo">
                    <img alt='logo' src="../SandhyaTheatre/images/logo.png">
                </a>
                
                <!-- Main website navigation-->
                <nav id="navigation-box">
                    <!-- Toggle for mobile menu mode -->
                    <a href="#" id="navigation-toggle">
                        <span class="menu-icon">
                            <span class="icon-toggle" role="button" aria-label="Toggle Navigation">
                              <span class="lines"></span>
                            </span>
                        </span>
                    </a>
                    
                    <!-- Link navigation -->
                    <ul id="navigation">
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="http://sandhyacinehouse.com/">HOME</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="http://sandhyacinehouse.com/about.php">ABOUT</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="http://sandhyacinehouse.com/upcoming.php">UPCOMING MOVIES</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="http://sandhyacinehouse.com/movie-page-full.php">NOW RUNNING</a>                            
                        </li>
                        <li>
                            <span class="sub-nav-toggle plus"></span>
                            <a href="http://sandhyacinehouse.com/contact.php">CONTACT US</a>                            
                        </li>                        
                    </ul>
                </nav>
                
                <!-- Additional header buttons / Auth and direct link to booking-->
                <div class="control-panel">
                   
                    <a href="http://www.innathecinema.com/ticket/" class="btn btn-md btn--warning btn--book">Book a ticket</a>
                </div>

            </div>
        </header>

    <br>
    <br> 
        <!-- Main content -->
        <section class="container" style="min-height:400px">

<link href="http://innathecinema.com/sandhya/core/framework/libs/pj/css/pj.bootstrap.min.css" type="text/css" rel="stylesheet" />
<link href="http://innathecinema.com/sandhya/index.php?controller=pjFront&action=pjActionLoadCss" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="http://innathecinema.com/sandhya/index.php?controller=pjFront&action=pjActionLoad"></script>
<script src="jquery.js"></script>
<script type="text/javascript" src="date.js"></script>
<div id="timeout-wrapper">
    <label style="">Time left:</label><span style="margin-left:5px;" id="time_left"></span>
</div>
		
        </section>

        
        <div class="clearfix"></div>
		
       <footer class="footer-wrapper">
            <section class="container">
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="http://sandhyacinehouse.com/" class="nav-link__item">HOME</a></li>
                        <li><a href="http://sandhyacinehouse.com/about.php" class="nav-link__item">ABOUT US</a></li>  
                        
                        <li><a href="http://sandhyacinehouse.com//movie-page-full.php" class="nav-link__item">NOW RUNNING</a></li>                   
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">
                        <li><a href="http://sandhyacinehouse.com/upcoming.php" class="nav-link__item">UPCOMING MOVIES</a></li>     
                        <li><a href="http://sandhyacinehouse.com/contact.php" class="nav-link__item">CONTACT US</a></li>               
                    </ul>
                </div>
                <div class="col-xs-4 col-md-2 footer-nav">
                    <ul class="nav-link">                   
                         <li><a href="http://sandhyacinehouse.com/privacy.php" class="nav-link__item">Terms & Condition</a></li>  
                         <li><a href="http://sandhyacinehouse.com/privacy.php" class="nav-link__item">Privacy Policy</a></li>   
                          <li><a href="http://sandhyacinehouse.com/privacy.php#cancel_policy" class="nav-link__item">Refund and Cancellation Policy</a></li>                     
                    </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="footer-info">
                        <p class="heading-special--small">SANDHYA CINE HOUSE<br><span class="title-edition">in the social media</span></p>

                        <div class="social">
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-facebook"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-twitter"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-vk"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-instagram"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-tumblr"></a>
                            <a href='https://www.facebook.com/Sandhyacinehouse/' class="social__variant fa fa-pinterest"></a>
                        </div>
                        
                        <div class="clearfix"></div>
                        <p class="copy">&copy; Sandhya, 2016. All rights reserved. Powered by <a href="http://innathecinema.com" target="_blank">InnatheCinema.com</a></p>
                    </div>
                </div>
            </section>
        </footer>
    </div>

    <!-- open/close -->
        <div class="overlay overlay-hugeinc">
            
            <section class="container">

                <div class="col-sm-4 col-sm-offset-4">
                    <button type="button" class="overlay-close">Close</button>
                    <form id="login-form" class="login" method='get' novalidate=''>
                        <p class="login__title">Sandhya Cine House<br><span class="login-edition">Booking will be available in few days</span></p>
                        
                    </form>
                </div>

            </section>
        </div>

	<!-- JavaScript-->
        <!-- jQuery 1.9.1--> 
        <script src="../SandhyaTheatre/js/jquery.min.js"></script>
        <!--<script>
			$(document).ready(function(e) {
				console.log("ready");
				$('#loader-ring').hide();
				$('#no-loader').css('opacity', '1').css('pointer-events', 'auto');
			});
		</script>
-->
        
        <script>window.jQuery || document.write('<script src="js/external/jquery-1.10.1.min.js"><\/script>')</script>
        <!-- Migrate --> 
        <script src="../SandhyaTheatre/js/external/jquery-migrate-1.2.1.min.js"></script>
        <!-- Bootstrap 3--> 
        <script src="../SandhyaTheatre/js/bootstrap.min.js"></script>

        <!-- jQuery REVOLUTION Slider -->
        <script type="text/javascript" src="../SandhyaTheatre/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <!--<script type="text/javascript" src="../SandhyaTheatre/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>-->

        <!-- Mobile menu -->
        <script src="../SandhyaTheatre/js/jquery.mobile.menu.js"></script>
         <!-- Select -->
        <script src="../SandhyaTheatre/js/external/jquery.selectbox-0.2.min.js"></script>
        <!-- Stars rate -->
        <script src="../SandhyaTheatre/js/external/jquery.raty.js"></script>
        
        <!-- Form element -->
        <script src="../SandhyaTheatre/js/external/form-element.js"></script>
        <!-- Form validation -->
        <script src="../SandhyaTheatre/js/form.js"></script>

        <!-- Twitter feed -->
        <script src="../SandhyaTheatre/js/external/twitterfeed.js"></script>

        <!-- Custom -->
        <script src="../SandhyaTheatre/js/custom.js"></script>
		
	      <script type="text/javascript">
              $(document).ready(function() {
                init_Home();
              });
		    </script>
</div>
<div class="loader" id="loader-ring" style="transform: scale(0.5);">
	<img src="logo.png" style="
    width: 300px;
">
    <div style="
    text-align: center;
    font-size: 45px;
    font-family: cursive;
    font-weight: bold;
    color: rgba(0, 0, 0, 0.72);
">Loading..</div>
</div>
    
   <script>
	function showLoader(){
		document.getElementById('loader-ring').style.display="block";
		var no_loader = document.getElementById('no-loader');
		no_loader.style.pointerEvents="none";
		no_loader.style.opacity="0.2";
	}
	function hideLoader(){
		document.getElementById('loader-ring').style.display="none";
		var no_loader = document.getElementById('no-loader');
		no_loader.style.pointerEvents="auto";
		no_loader.style.opacity="1";
	}
</script>

</body>

</html>
