<?php
	require_once('data.php');
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="http://innathecinema.com/sandhya/app/web/css/admin.css">
<link rel="stylesheet" href="css/live.css" />
<link rel="stylesheet" href="css/ripple.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
	table {
		border-collapse: collapse;
	}
	
	td {
		padding-top: .5em;
		padding-bottom: .5em;
		font-size: 18px;
	}
	.font_bold{
		font-weight:bold;
	}
	.trans_success{
		color:green !important;
	}
	.trans_fail{
		color:red !important;
	}
</style>
<div id="no-loader">
	<div id="header">
        <div id="logo">
            <a href="http://innathecinema.com/sandhya" rel="nofollow">Sandhya Cine House</a>
            <span>v1.0</span>
        </div>
    </div>
    <div class='container'>
        <div class="row">
        	<div class="col-md-12" style="text-align:center">
            	<div class="col-md-6">
                    <lable style="font-size: 20px;">Transaction ID:</label>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" id="transaction_id" placeholder="Transaction ID without usercode goes here..">
                </div>
                <input type="button" class="btn btn-primary" id="go_btn" value="Go" style="margin-top:10px">
            </div>
        </div>
    </div>
    
    <div class="container map_container" style="display:block">
    	<div class="row">
        	<div class="col-sm-12" style="overflow-y: auto;">
            	 <div style="height: 750px;width:1000px;margin-left: 0px;margin:0 auto;position: relative;" class="map-holder">
                 	<table width="100%" cellspacing="10">
                    	<tr>
                        	<td>Name</td><td><span class="customer_name"></span></td>
                        </tr>
                        <tr>
                            <td>Transaction ID</td><td><span class="response_trans_id"></span></td>
                        </tr>
                        <tr>
                        	<td>Amount</td><td><span class="response_trans_amount"></span></td>
                        </tr>
                        <tr>
                        	<td>Transaction Date</td><td><span class="response_trans_date"></span></td>
                        </tr>
                        <tr>
                        	<td>Transaction Status</td><td><span class="response_trans_status"></td></td>
                        </tr>
                    </table>
                 </div>
            </div>
        </div>
    </div>
</div>
<div class='uil-squares-css' id="loader-ring" style='transform:scale(0.5);'>
	<div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div></div>
<script>
	$(document).on('keypress', '#transaction_id', function(e) {
		if(e.which == 13) {
			$('#go_btn').click();
		}
	});
	$('#go_btn').on('click', function(event){
		if($('#transaction_id').val()==""){
			$('#transaction_id').val().focus();
			return false;
		}
		var transaction_id = $('#transaction_id').val().trim();
		$.ajax({
			type: "POST",
			async: true,
			url: "http://innathecinema.com/payment-status/xhr.php",
			data:{'transaction_id':transaction_id},
			cache: false,
			beforeSend: function() {
				$('body').css('opacity', '0.3')
						 .css('pointer-events', 'none');
			},
			success: function(data) {
				setData(data);
			},
			error:function(){
				alert("Oops! Something is not right. Please try again");
			},
			complete: function(){
				$('body').css('opacity', '1')
						 .css('pointer-events', 'auto');
			}
		});
	});
	function setData(data){
		try{
			console.log(data);
			var obj = $.parseJSON(data);
			$('.customer_name').html(obj.c_name);
			var response = obj.gateway_response;
			$('.response_trans_id').html(response[0]);
			$('.response_trans_amount').html((response[1]/100));
			$('.response_trans_date').html(response[3]);
			var transaction_status = '';
			var transaction_css = "";
			switch(response[4]){
				case 'S':
					var transaction_status = 'SUCCESS';
					var transaction_css = "color:green;font-weight:bold";
				break;
				case 'A':
					var transaction_status = 'PENDING';
					var transaction_css = "color:orange;font-weight:bold";
				break;
				case 'P':
					var transaction_status = 'PENDING';
					var transaction_css = "color:orange;font-weight:bold";
				break;
				case 'F':
					var transaction_status = 'FAILED';
					var transaction_css = "color:red;font-weight:bold";
				break;
				case 'R':
					var transaction_status = 'REFUNDED';
					var transaction_css = "color:brown;font-weight:bold";
				break;
				default:
					var transaction_status = 'CANNOT READ TRANSACTION STATUS';
					var transaction_css = "color:red;font-weight:bold";
				break;
			}
			$('.response_trans_status').html(transaction_status);
			$('.response_trans_status').attr('style', transaction_css);
		
		}catch(err){
			alert('Oops! Something is no right');
		}
		
	}
</script>