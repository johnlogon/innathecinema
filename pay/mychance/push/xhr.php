<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	
	if(!isset($_POST['action'])){
		exit;
	}
	
	require_once('db_connect.php');
	
	switch($_POST['action']){
		case 'wall_like':
			require_once('wall_like.php');	
		break;
		case 'wall_comment':
			require_once('wall_comment.php');	
		break;
		case 'friend_request':
			require_once('friend_request.php');	
		break;
		case 'group_invitation':
			require_once('group_invitation.php');	
		break;
	}
?>