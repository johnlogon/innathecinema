<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	
	require_once("db_connect.php");
	
	if(!isset($_POST['guid']) || empty($_POST['guid']))
		exit;
		
	$guid = $_POST['guid'];
	
	$table = 'ossn_user';
	
	$sql = "SELECT device_id FROM `ossn_deviceids` WHERE userid=".$guid;
	
	$result = mysqli_query($db, $sql);
	if(!$result){
		echo mysqli_error($db);
		exit;
	}
	
	$regIds = array();
	while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
		array_push( $regIds, $row['device_id']);
	}
	
	if(empty($regIds))
		exit;
		
	$msg = array
	(
		'message' 	=>'You have new message',
		'vibrate'	=> 1,
		'sound'		=> "default",
	);
	
	require_once('push.php')
?>