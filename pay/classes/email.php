<?php

class SnogoEmail extends pjFront{
	
	public  function connectPj(){
		$dbPj = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
		return $dbPj;
	}
	
	/* FUNCTION TO SEND EMAIL */
	
	public function sendMail($invoice_params){
		
		$message = "<tr><td><b><h3>Your booking has been received. </b></h3></td></tr>
					<tr><td>Personal details:</td></tr>
					<tr><td>Name:</td><td>".$invoice_params['c_name']."</td></tr>
					<tr><td>E-Mail:</td><td>".$invoice_params['c_email']."</td></tr>
					<tr><td>Phone:</td><td>".$invoice_params['c_phone']."</td></tr>
					<tr><td><b><h3>Booking details:</b></h3></td></tr>
					<tr><td>Movie:</td><td>".$invoice_params['movie']."</td></tr>
					<tr><td>Venue:</td><td>".$invoice_params['venue']."</td></tr>
					<tr><td>Showtime:</td><td>".$invoice_params['show_time']."</td></tr>
					<tr><td>Booking ID:</td><td>".$invoice_params['booking_id']."</td></tr>
					<tr><td>Booking Seats:</td><td>".$invoice_params['seats']."</td></tr>
					<tr><td>Please reach before 15 minutes. Thank you</td></tr>";
	
	
	
		$to      = $invoice_params['c_email'];
		$subject = 'Ticket Confirmation';
		$headers = 'From: Sandhya Cine House <admin@sandhyacinehouse.com>' . PHP_EOL .
			'Reply-To: Sandhya Cine House <admin@sandhyacinehouse.com>' . PHP_EOL .
			'Content-type:text/html;charset=UTF-8'. PHP_EOL .
			'X-Mailer: PHP/' . phpversion();
	
		$mail = mail($to, $subject, $message, $headers);
		$invoice_params['mail'] = "success mail";
		$invoice_params['mail_message'] = $message;
		$this->logNotification($invoice_params);
	}
	
	/* FUNCTION TO SEND SMS */
	
	public function sendSms($invoice_params){
		
		$message = "Name: ".$invoice_params['c_name'].", Movie: ".$invoice_params['movie'].", Venue: ".$invoice_params['venue'].", Show: ".$invoice_params['show_time'].", ID: ".$invoice_params['booking_id'].", Seats: ".$invoice_params['seats'].". Pls reach before 15 min";
		
		$http = new pjHttp();
		
		$url = "http://alerts.ebensms.com/api/v3/";
		$params = http_build_query(array(
			'method' =>'sms',
			'message' => $message,
			'sender' => 'INCNMA',
			'to' =>$invoice_params['c_phone'],
			'api_key' => 'A35e447822b5a27a172224ae820e51572' 
		));
		$invoice_params['sms']="success msg";
		$invoice_params['sms_message'] = $message;
		$this->logNotification($invoice_params);
		$http->request($url ."?". $params)->getResponse();
	}
	
	public function sendErrorSMS($invoice_params){
		$message = "Your transaction has failed. For refund, please contact: 0495-4044435 (9 AM to 5.30 PM, MONDAY-FRIDAY) .Transaction ID:".$invoice_params['booking_id'];
		
		$http = new pjHttp();
		$url = "http://alerts.ebensms.com/api/v3/";
		$params = http_build_query(array(
			'method' =>'sms',
			'message' => $message,
			'sender' => 'INCNMA',
			'to' =>$invoice_params['c_phone'],
			'api_key' => 'A35e447822b5a27a172224ae820e51572' 
		));
		
		$http->request($url ."?". $params)->getResponse();
		$invoice_params['sms'] = "error message";
		$invoice_params['sms_message'] = $message;
		$this->logNotification($invoice_params);
	}
	public function sendErrorMail($invoice_params){
		
		$message = "Your transaction has failed. For refund, please contact: 0495-4044435 (9 AM to 5.30 PM, MONDAY-FRIDAY). Transaction ID:".$invoice_params['booking_id'];
		$to      = $invoice_params['c_email'];
		$subject = 'Booking failed';
		$headers = 'From: Sandhya Cine House <admin@sandhyacinehouse.com>' . PHP_EOL .
			'Reply-To: Sandhya Cine House <admin@sandhyacinehouse.com>' . PHP_EOL .
			'Bcc: suhail@snogol.net,shamsheer@snogol.net'. PHP_EOL.
			'Content-type:text/html;charset=UTF-8'. PHP_EOL .
			'X-Mailer: PHP/' . phpversion();
	
		$mail = mail($to, $subject, $message, $headers);
		$invoice_params['mail'] = "error mail";
		$invoice_params['mail_message'] = $message;
		$this->logNotification($invoice_params);
	}
	
	public function logNotification($params){
		
		$myfile = fopen($_SERVER['DOCUMENT_ROOT']."/pay/logs/smsEmailLog.txt", "a");
		
		date_default_timezone_set("Asia/Kolkata");
		$date = new DateTime();
		$result = $date->format('d-M-Y h:i:s A');
		$params['created_time'] = $result;
		
		$txt = json_encode($params);
		fwrite($myfile, $txt."\n\n\n");
	}
	
}

?>