<?php
	class Encryption {
		private function generateKey(){
			$keyString = 'innsng$#555';
			$key = md5($keyString, true);
			return $key;
		}
		public function encrypt($id){
			$key = $this->generateKey();
			$id = base_convert($id, 10, 36);
			$data = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
			$data = bin2hex($data);
			return $data;
		}
		
		public function decrypt($encrypted_id){
			$key = $this->generateKey();
			$data = pack('H*', $encrypted_id);
			$data = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
			$data = base_convert($data, 36, 10);
			return $data;
		}
	}
?>