<?php
	class OfflineApi{
		private function connect(){
			$conn = new mysqli(PJ_HOST,PJ_USER, PJ_PASS, PJ_DB);
			if ($conn->connect_errno) {
				$this->logger($conn->connect_error);
				return false;
			}
			return $conn;
		}
		public function logger($log){
			$file = $_SERVER['DOCUMENT_ROOT']."/fedpay/offline_check_log.txt";
			$logFile = fopen($file, 'a');
			$time = date('d-m-Y h:i:s A', time());
			$time= $this->getTimeStamp();
			$log.="       time:".$time."\n";
			fwrite($logFile, $log);
			fclose($logFile);
		}
		private function setTimeZone(){
			date_default_timezone_set('Asia/Kolkata');
		}
		private function getTimeStamp(){
			$this->setTimeZone();
			$time = time();
			return $time;
		}
		public function check(){
			$pending_records = $this->getPendingRecords();
			foreach($pending_records as $record){
				$payment_status = $this->requestStatus($record);
				if(end($payment_status)=='S'){
					$this->assignTickets($record);
				}
				else if($record['times_checked']>=2){
					$record['times_checked'] = $record['times_checked'] + 1;
					$this->setTimesChecked($record, true);
				}
				else if($record['times_checked']<2){
					$record['times_checked'] = $record['times_checked'] + 1;
					$this->setTimesChecked($record);
				}
			}
		}
		public function getPendingRecords(){
			$mysqli = $this->connect();
			$sql = "SELECT 
					  t.id,
					  t.uuid,
					  t.status ,
					  t.`times_checked`
					FROM
					  thcbs_bookings t 
					WHERE (? - t.`pg_time`) >= 600  
					  AND t.`status` = 'pending' 
					  AND t.`is_msg_sent` = 0 
					  AND t.`times_checked` < 2 ";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $this->getTimeStamp());			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				return false;
			}
			$stmt->bind_result($id, $uuid, $status, $times_checked);
			$data = array();
			$i=0;
			while($stmt->fetch()){
				$data[$i]['id'] = $id;
				$data[$i]['uuid'] = $uuid;
				$data[$i]['status'] = $status;
				$data[$i++]['times_checked'] = $times_checked;
			}
		}
		public function requestStatus($params){
			$http = new pjHttp();
			$url = "https://epay.federalbank.co.in/easypaytranstatus/GetEasyPayTranStatus.htm";
			$params = http_build_query(array(
				'user_code' =>'INNCINEMALIV',
				'pass_code' => 'INNCINEMA678$',
				'tran_id' => 'INNCINEMALIV'.$params['uuid']
			));
			$response = $http->request($url ."?". $params)->getResponse();
			$response=str_replace("ns1:","",$response);
			$xml = simplexml_load_string($response);
			$pipedString =  $xml->SP->V_STATUS;
			$data = explode("|", $pipedString);
			return $data;
		}
		public function assignTickets($params){
			
		}
		private function setTimesChecked($params, $isCancelled = false){
			$mysqli = $this->connect();
			$sql = "UPDATE 
					  thcbs_bookings t 
					SET
					  t.`times_checked` = ?";
			if($isCancelled)
				$sql.=",t.`status` = 'cancelled'";
			$sql.="	WHERE t.`id` =";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $params['times_checked']);
			if($stmt->execute()){
				return true;
			}
			$this->logger($mysqli->error);
			return false;
		}
	}
?>