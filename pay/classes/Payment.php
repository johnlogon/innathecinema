<?php
	class Payment{
		
		private function connect(){
			$conn = new mysqli(PJ_HOST,PJ_USER, PJ_PASS, PJ_DB);
			if ($conn->connect_errno) {
				$this->logger($conn->connect_error);
				return false;
			}
			return $conn;
		}
		public function logger($log){
			$file = $_SERVER['DOCUMENT_ROOT']."/fedpay/log.txt";
			$logFile = fopen($file, 'a');
			$time = date('d-m-Y h:i:s A', time());
			$time= $this->getTimeStamp();
			$log.="       time:".$time."\n";
			fwrite($logFile, $log);
			fclose($logFile);
		}
		private function setTimeZone(){
			date_default_timezone_set('Asia/Kolkata');
		}
		private function getTimeStamp(){
			$this->setTimeZone();
			$time = time();
			return $time;
		}
		private function getGatewayCreds(){
			$params = array();
			$params['user_code'] = 'INNCINEMALIV';
			$params['hash_key'] = 'ASXVSPOHF7696678';
			return $params;
		}
		public function isAlreadyConfirmed($bkid){
			$mysqli = $this->connect();
			$sql = "SELECT status FROM thcbs_bookings WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				throw new Exception('mysql error');
			}
			$stmt->bind_result($status);
			$stmt->fetch();
			if($status=='confirmed')
				return true;
			return false;
		}
		public function getPaymentInfoById($bkid){
			$mysqli = $this->connect();
			$sql = "SELECT  t1.id, t1.uuid, t1.event_id, t1.date_time, 
							t1.total, t1.status, t1.txn_id, t1.created, 
							t1.c_name, t1.c_phone, t1.c_email 
					   FROM thcbs_bookings t1 WHERE t1.id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				return false;
			}
			$stmt->bind_result($id, $uuid, $event_id, $date_time, $total, $status, $txn_id, $created, $c_name,  $c_phone, $c_email );
			$data = array();
			$stmt->fetch();
			$data['id'] = $id;
			$data['uuid'] = $uuid;
			$data['event_id'] = $event_id;
			$data['date_time'] = $date_time;
			$data['total'] = $total;
			$data['status'] = $status;
			$data['txn_id'] = $txn_id;
			$data['created'] = $created;
			$data['c_name'] = $c_name;
			$data['c_phone'] = $c_phone;
			$data['c_email'] = $c_email;
			return $data;
			
		}
		public function getPaymentInfoByUUID($uuid){
			$mysqli = $this->connect();
			$sql = "SELECT  t1.id, t1.uuid, t1.event_id, t1.date_time, 
							t1.total, t1.status, t1.txn_id, t1.created, 
							t1.c_name, t1.c_phone, t1.c_email 
					   FROM thcbs_bookings t1 WHERE t1.uuid=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('s', $uuid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				echo $mysqli->error;
				return false;
			}
			$stmt->bind_result($id, $uuid, $event_id, $date_time, $total, $status, $txn_id, $created, $c_name,  $c_phone, $c_email );
			$data = array();
			$stmt->fetch();
			$data['id'] = $id;
			$data['uuid'] = $uuid;
			$data['event_id'] = $event_id;
			$data['date_time'] = $date_time;
			$data['total'] = $total;
			$data['status'] = $status;
			$data['txn_id'] = $txn_id;
			$data['created'] = $created;
			$data['c_name'] = $c_name;
			$data['c_phone'] = $c_phone;
			$data['c_email'] = $c_email;
			return $data;
			
		}
		public function verifyHash($post){
			$params = $this->getGatewayCreds();
			$arr['user_code'] = $params['user_code'];
			$arr['tran_id'] = $post['tran_id'];
			$arr['amount'] = $post['amount'];
			$arr['status'] = $post['Tran_status'];
			$arr['hash_key'] = $params['hash_key'];
			
			$hash_v = implode("|", $arr);
			$original_hash = base64_encode(sha1($hash_v , true));
			if(strcmp($original_hash, $post['hash_value'])==0)
				return true;
			return false;
		}
		public function getTransactionID($mixedValue){
			$params = $this->getGatewayCreds();
			$replace = $params['user_code'];
			$trans = str_replace($replace, "", $mixedValue);
			return $trans;
		}
		public function savePaymentGatewayReponse($params){
			$mysqli = $this->connect();
			$sql = "INSERT INTO thcbs_payment_gateway_response 
					(trans_id, amount, trans_status, hash_value, uuid) 
					VALUES (?,?,?,?,?)";
			$stmt = $mysqli->prepare($sql);
			$uuid = $this->getTransactionID($params['tran_id']);
			$stmt->bind_param('sisss', $params['tran_id'], $params['amount'], $params['Tran_status'], $params['hash_value'], $uuid);
			if($stmt->execute())
				return true;
			return false;
		}
		public function performPostPaymentSuccessOps($id){
			//$paymentInfo = $this->getPaymentInfoByUuid($id);	
		}
		public function hasDuplication($bkid){
			$mysqli = $this->connect();
			$sql = "SELECT 
					  COUNT(*) AS s 
					FROM
					  thcbs_bookings_shows 
					WHERE show_id = 
					  (SELECT DISTINCT 
						show_id 
					  FROM
						thcbs_bookings_shows 
					  WHERE booking_id = ?) 
					  AND seat_id IN 
					  (SELECT 
						seat_id 
					  FROM
						thcbs_bookings_shows 
					  WHERE booking_id = ?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('ii', $bkid, $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				throw new Exception('mysql error');
			}
			$stmt->bind_result($total_count);
			$user__booked_seat_count = $this->getSeatCountByBookingId($bkid);
			if($total_count > $user__booked_seat_count)
				return true;
			return false;
		}
		private function getSeatCountByBookingId($bkid){
			$mysqli = $this->connect();
			$sql = "SELECT COUNT(*) AS s FROM thcbs_bookings_shows WHERE booking_id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				throw new Exception('mysql error');
			}
			$stmt->bind_result($count);
			return $count;
		}
		private function updateBookingStatus($status, $bkid){
			$mysqli = $this->connect();
			$sql = "UPDATE thcbs_bookings SET status=? WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('si', $status, $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				throw new Exception('mysql error');
			}
		}
		private function removeFromBookingPayments($bkid){
			$mysqli = $this->connect();
			$sql = "DELETE FROM thcbs_bookings_payments WHERE booking_id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				throw new Exception('mysql error');
			}
		}
		private function removeFromBookingShows($bkid){
			$mysqli = $this->connect();
			$sql = "DELETE FROM thcbs_bookings_shows WHERE booking_id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				throw new Exception('mysql error');
			}
		}
		private function removeFromBookingTickets($bkid){
			$mysqli = $this->connect();
			$sql = "DELETE FROM thcbs_bookings_tickets WHERE booking_id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $bkid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				throw new Exception('mysql error');
			}
		}
		private function updateInvoiceStatus($status, $uuid){
			$mysqli = $this->connect();
			$sql = "UPDATE thcbs_plugin_invoice SET STATUS=? WHERE order_id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('si', $status, $uuid);			
			if(!$stmt->execute()){
				$this->logger($mysqli->error);
				throw new Exception('mysql error');
			}
		}
	}
?>