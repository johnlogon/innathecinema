<?php
		/*
			CHECK FOR DUPLICATION OF SEATS
		*/
		$sql = "SELECT COUNT(*) AS s FROM thcbs_bookings_shows WHERE show_id=(SELECT DISTINCT show_id FROM thcbs_bookings_shows WHERE booking_id=".$bkid.") 
				AND seat_id IN (SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=".$bkid.")";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$count_total = $row['s'];
		
		$sql = "SELECT COUNT(*) AS s FROM thcbs_bookings_shows WHERE booking_id=".$bkid;
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$count_this_booking_seats = $row['s'];
		
		if($count_total>$count_this_booking_seats){ //duplication occured
			//fetch duplicated seats.. only for reference
			$sql = "SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=".$bkid;
			$result = mysqli_query($db, $sql);
			$dupSeats = "";
			while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
				$dupSeats.=$row['seat_id']." ";
			}
			$dupFile = fopen($_SERVER['DOCUMENT_ROOT'].'/pay/logs/dup_log.txt', 'a');
			date_default_timezone_set('Asia/Kolkata');
			fwrite($dupFile, 'Duplicated Seats found>>Booking id:'.$bkid.', seats: '.$dupSeats.',  time:'.date('d-m-Y h:i:s A')."\n");
			echo "<div style='margin:10%; border:solid 1px;text-align:center;color:red;'>";
			echo "<h3>Your transaction has been failed. For refund, please contact: 0495-4044435 (9 AM to 5.30 PM, MONDAY-FRIDAY)</h3>"; 
			echo "<h4>Your Transaction ID is " . $transaction_id . ".</h4>";
			echo "<a href='http://innathecinema.com/ticket' >Book Again</a><br><br>";
			echo "</div>";
			$sqls = array("UPDATE thcbs_bookings SET status='cancelled' WHERE id=$bkid", "DELETE FROM thcbs_bookings_payments WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_shows WHERE booking_id=$bkid", "DELETE FROM thcbs_bookings_tickets WHERE booking_id=$bkid", "UPDATE thcbs_plugin_invoice SET STATUS='cancelled' WHERE order_id='".$booking_info['uuid']."'");
	
			foreach($sqls as $sql){
				mysqli_query($db, $sql);
			}
			
			/*mail and message*/
			$invoice_params['booking_id'] = $transaction_id;
			$invoice_params['c_email'] = $booking_info['c_email'];
			$invoice_params['c_phone'] = $booking_info['c_phone'];
			$mail->sendErrorSMS($invoice_params);
			$mail->sendErrorMail($invoice_params);
			die();
		}
?>