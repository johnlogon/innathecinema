<?php
	$db = mysqli_connect(PJ_HOST, PJ_USER, PJ_PASS, PJ_DB);
	$mail = new SnogoEmail();
	require_once('duplication_check.php');
	
	/*
		IF NO DUPLICATION SEND SUCCESS SMS AND MAIL
	*/
	
	$sqls = array("UPDATE thcbs_bookings SET status='confirmed' WHERE id=".$bkid, "UPDATE thcbs_bookings_payments SET status='paid', amount=".$booking_info['total']." WHERE booking_id=".$bkid, "UPDATE thcbs_plugin_invoice SET STATUS='paid' WHERE order_id='". $booking_info['uuid']."'");
		foreach($sqls as $sql){
			if(!mysqli_query($db, $sql)){
				
			}				
		}
		
		$invoice_params = array();
		$invoice_params['c_name']  = $booking_info['c_name'];
		$invoice_params['c_email'] = $booking_info['c_email'];
		$invoice_params['c_phone'] = $booking_info['c_phone'];
		
		/**** MOVIE TITLE ***/
		$sql = "SELECT content FROM thcbs_multi_lang WHERE foreign_id=(SELECT event_id FROM thcbs_bookings WHERE id=$bkid) AND model='pjEvent' AND FIELD='title'";
		$row =  mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC);
		$invoice_params['movie'] = $row['content'];
		
		/***** FETCHING SHOW TIME AND VENUE ****/
		$sql = "SELECT date_time, venue_id FROM thcbs_shows WHERE id=(SELECT show_id FROM thcbs_bookings_shows WHERE booking_id=$bkid LIMIT 1 OFFSET 0);";
		$row = mysqli_fetch_array(mysqli_query($db, $sql), MYSQLI_ASSOC); 
		$showTime = $row['date_time'];
		$showTime = date('d-M-Y h:i A', strtotime($showTime));
		$invoice_params['show_time'] = $showTime;
		
		$sql = "SELECT * FROM thcbs_multi_lang WHERE foreign_id=".$row['venue_id']." AND model='pjVenue' AND FIELD='name'";
		$result = mysqli_query($db, $sql);
		$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
		$invoice_params['venue'] = $row['content'];
		
		
		/***** FETCHING SHOW SEATS ****/
		
		$sql = "SELECT name FROM thcbs_seats WHERE id IN(SELECT seat_id FROM thcbs_bookings_shows WHERE booking_id=$bkid);";
		
		$result = mysqli_query($db, $sql);
		$sendFlag=true;
		if(!mysqli_num_rows($result)>0){
			$sendFlag=false;
		}
		$seats = "";
		while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
			$seats = $seats. $row['name'].",";
		}
		$seats= rtrim($seats, ",");
		$invoice_params['seats'] = $seats;
		
		/***** FETCHING CUSTOMER NUMBER ****/
		$invoice_params['booking_id'] = $booking_info["uuid"];
		
		if($sendFlag){
			echo "<div style='margin:10%; border:solid 1px;text-align:center;color:green;font-family:arial;'>";
			echo "<h3>Your booking is successful.</h3>";
			echo "<h4>Transaction ID is " . $booking_info['uuid'] . ".</h4>";
			echo "<h4>Name: ".$invoice_params['c_name'].".</h4>";
			echo "<h4>We have received a payment of Rs. " . $booking_info['total'] . "</h4>";
			echo "<span style='color: blue;font-size: 18px;'><h4>Movie:".$invoice_params['movie'].", Seats: ".$seats.", Show: ".$invoice_params['show_time'].",".$invoice_params['venue']."</h4></span>";
			echo "<h4>Your ticket will be recieved in SMS / mail</h4>";
			echo "<h4>Show the mail / screenshot / SMS at the counter.</h4>";
			echo "<h4 style='color:black;'>For any queries: +91 9562583247 (9 AM to 9 PM)</h4>";
			echo "<h3>Enjoy the film. Thanks!!</h3>";
			echo "<a href='http://innathecinema.com/ticket' >Book Another Ticket</a><br><br>";
			echo "</div>";			
			$mail->sendMail($invoice_params);
			$send = $mail->sendSms($invoice_params);
		}else{
			echo "<div style='margin:10%; border:solid 1px;text-align:center;color:red;'>";
			echo "<h3>Your transaction has been failed. For refund, please contact: 0495-4044435 (9 AM to 5.30 PM, MONDAY-FRIDAY) </h3>";
			echo "<h4>Your Transaction ID is " . $transaction_id . ".</h4>";
						echo "<a href='http://innathecinema.com/ticket' >Book Again</a><br><br>";
						echo "</div>";
			$mail->sendErrorSMS($invoice_params);
			$mail->sendErrorMail($invoice_params);
		}
		require_once('booking_limit_check.php');
?>
