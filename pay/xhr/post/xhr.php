<?php
	/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
	header('Access-Control-Allow-Origin: *');
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		require_once($_SERVER['DOCUMENT_ROOT']."/pay/auto_load.php");
		if(!isset($_POST['action']) || empty($_POST['action'])){
			$response['code'] = 400;
			$response['msg'] = "Bad Request";
			echo json_encode($response);
			exit;
		}
		switch($_POST['action']){
			case "id-encrypt":
				if(isset($_POST['bkid']) && !empty($_POST['bkid'])){
					if(!ctype_digit($_POST['bkid'])){
						$response['code'] = 400;
						$response['msg'] = "Bad Request";
						echo json_encode($response);
						exit;
					}
					$encryption = new Encryption;
					$response['code'] = 200;
					$response['data'] = $encryption->encrypt($_POST['bkid']);
					echo json_encode($response);
				}
				else{
					$response['code'] = 400;
					$response['msg'] = "Bad Request";
					echo json_encode($response);
					exit;
				}
			break;
			default:
				$response['code'] = 400;
				$response['msg'] = "Bad Request";
				echo json_encode($response);
				exit;
		}
		
	}
	else{
		$response = array();
		$response['code'] = 400;
		$response['msg'] = "Not authorized";
		echo json_encode($response);
	}
?>