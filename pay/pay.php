<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

require_once('auto_load.php');
require_once('data.php');
?>

<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width,initial-scale=1">

        <title>InnatheCinema - Payment</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>

    <body>

        <div style="width:100%; text-align: center;font-size: 25px;margin-top: 50px;">
        	<div>Please wait..</div>
            <div>We are redirecting you to the payment gateway..</div>
            <div><img src="img/squares.gif"></div>
        </div>

        <form action="https://epay.federalbank.co.in/FedPaymentsV1/Payments.ashx" method="post" name="merchantForm">

            <input type="hidden" value="<?php echo $response['user_code']; ?>" name="user_code">

            <input type="hidden" value="<?php echo $response['pass_code']; ?>" name="pass_code">

            <input type="hidden" value="<?php echo $response['tran_id']; ?>" name="tran_id">

            <input type="hidden" value="<?php echo $response['amount']; ?>" name="amount">

            <input type="hidden" value="<?php echo $response['charge_code']; ?>" name="charge_code">

            <input type="hidden" value="<?php echo $response['hash_value']; ?>" name="hash_value">
            
            <input type="hidden" value="<?php echo $response['reserve1'] ?>" name="reserve1"> 

            <input type="submit" id="submitBtn" style="display:none" value="" />

        </form>

    </body>
    <script>
    	$(document).ready(function(e) {
            $('#submitBtn').click();
        });
    </script>

</html>