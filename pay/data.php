<?php

	if($_SERVER['REQUEST_METHOD'] !== 'GET')
		exit;
	if(!isset($_GET['secret']))
		exit;
	if(empty($_GET['secret']))
		exit;
	//decrypting secret to get numerical id
	$encryption = new Encryption;
	$bkid = $encryption->decrypt($_GET['secret']);
	if(!ctype_digit($bkid))
		exit;
	$payment = new Payment;
	if($payment->isAlreadyConfirmed($bkid)){
		echo "Already Confirmed";
		exit;
	}
	$http = new pjHttp();
	$http->setMethod('POST');
	$data = array("token" => md5('snginn$#', true));
	$http->setData($data);
	$url = 'http://innathecinema.com/pay/getCredes.php';
	$res = $http->curlRequest($url)->getResponse();
	$response = json_decode($res, true);
	if($response['code']!=200){
		echo "Oops! Something is no right. Please try again";
		exit;
	}
	
	$paymentInfo = $payment->getPaymentInfoById($bkid);
	if($paymentInfo==false){
		exit;
	}
	$response['tran_id'] = $response['user_code']. $paymentInfo['uuid'];
	$response['amount'] = floatval($paymentInfo['total']) * 100;
	
	$string = implode('|', array(

            $response['user_code'] ,

            $response['pass_code'] ,

            $response['tran_id'] ,

            $response['amount'] ,

            $response['charge_code'] ,

            $response['hash_key']

        ));

    $response['hash_value'] = base64_encode(sha1( $string , true ) );
?>