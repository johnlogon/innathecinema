<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	require_once('auto_load.php');
	if($_SERVER['REQUEST_METHOD'] === 'POST'){
		$payment = new Payment;
		$payment->savePaymentGatewayReponse($_POST);
		$transaction_id = $payment->getTransactionID($_POST['tran_id']);
		if(!$payment->verifyHash($_POST)){
			include_once("data/payment_error.php");
			exit;
		}
		switch($_POST['Tran_status']){
			case "S":
				include_once("data/payment_success.php");
			break;
			
			case "F":
				include_once("data/payment_error.php");
				exit;
			break;
			
			case "P":
				include_once("data/payment_error.php");
				exit;
			break;
			
			default:
				echo "Something is not right!";
		}
	}
	else{
		header('HTTP/1.0 403 Forbidden');
   		die('You are not allowed to access this file.');   
	}
?>